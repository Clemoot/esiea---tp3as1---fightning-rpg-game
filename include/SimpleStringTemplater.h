/**
	\author Garnier Cl�ment
	\brief Declarations of library elements

	The file contains the declaration of StringTemplate and SimpleStringTemplater.
	The class StringTemplate replaces variables in a template.
	The class SimpleStringTemplater wraps StringTemplate classes and sort them according to its id and its language code
*/

#ifndef __SIMPLE_STRING_TEMPLATER_INCUDED__
#define __SIMPLE_STRING_TEMPLATER_INCUDED__

#ifdef SIMPLE_STRING_TEMPLATER_LIBRARY_EXPORTS
#define SIMPLE_STRING_TEMPLATER_LIBRARY_API __declspec(dllexport)
#else
#define SIMPLE_STRING_TEMPLATER_LIBRARY_API __declspec(dllimport)
#endif

#include <fstream>
#include <iostream>
#include <vector>
#include <map>


#define STRING_TEMPLATE_DEFAULT_ID_SEPARATOR '\"'
#define STRING_TEMPLATE_DEFAULT_LANG_SEPARATOR '>'
#define STRING_TEMPLATE_DEFAULT_VAR_SEPARATOR ':'

/**
\def STRING_TEMPLATE_VAR_CHARACTER_TEST
\brief Function checking if the identifiers are alphanumerical
*/
#define STRING_TEMPLATE_VAR_CHARACTER_TEST [](unsigned char c) { return (std::isalnum(c) || c == '_'); }

/**	\class StringTemplate
	\brief String containing variables to replace with values
*/
class SIMPLE_STRING_TEMPLATER_LIBRARY_API StringTemplate {
public:
	/** 
	\brief StringTemplate constructor
	\param templateString string containing variables
	\param varSeparator Character determining variables placeholder
	*/
	StringTemplate(std::string templateString = "", unsigned char varSeparator = STRING_TEMPLATE_DEFAULT_VAR_SEPARATOR);

	/**
	\brief StringTemplate copy constructor
	*/
	StringTemplate(const StringTemplate& other);
	~StringTemplate();

	/// Checks if all variables of the tempalte have been set
	bool IsReady() const;

	/**
	\brief Replaces variables with the same name by a value
	\param varName Variable name
	\param value Value
	\return Template with replaced variables
	*/
	StringTemplate Replace(std::string varName, std::string value);

	/**
	\brief Replaces all remaining variables with a value
	\param value Global value
	\return	String without variables
	*/
	std::string Terminate(std::string value = "?");

	/**
	\brief Retrieve the current string with variables placeholder
	*/
	std::string Str() const;

	StringTemplate& operator=(const StringTemplate& other);

private:
	unsigned char _varSeparator;			///< Character determining variable range
	std::vector<std::string>* _variables;	///< Variables in the template
	std::string* _template;					///< Template string

	bool __LocalizeVariable(std::string varName, uint32_t& pos, uint32_t& size, uint32_t offset = 0);
	static std::string::iterator __FindPattern(std::string& src, std::string pattern);
};

/**	\class SimpleStringTemplater
	\brief Template manager
	This class allows to load template files/strings and store them.
	It can be retrieved with its id and its language mentionned in the file/string.
*/
class SIMPLE_STRING_TEMPLATER_LIBRARY_API SimpleStringTemplater {
public:
	/**
	\brief SimpleStringTemplater constructor
	\param path Path to the file containing templates
	\param idSeparator Character determning id location
	\param langSeparator Character seperating the language code from its value
	\param varSeparator Character determning variables placeholder in templates
	*/
	SimpleStringTemplater(std::string path = "", unsigned char idSeperator = STRING_TEMPLATE_DEFAULT_ID_SEPARATOR, unsigned char langSeparator = STRING_TEMPLATE_DEFAULT_LANG_SEPARATOR, unsigned char varSeparator = STRING_TEMPLATE_DEFAULT_VAR_SEPARATOR);
	~SimpleStringTemplater();

	/// Load templates from a file
	void LoadFile(std::string path);

	/// Load templates from a string
	void LoadString(std::string content);

	/**
	\brief Retrieve a template with its id and its language code
	\param templateId Id of the template specified
	\param lang Language code
	*/
	StringTemplate Find(std::string templateId, std::string lang = "");

	/**
	\brief Alias for Find member
	*/
	StringTemplate operator()(const std::string templateId, const std::string lang = "");


	/**
	\brief Set a default language
	\param languageCode Default language code
	*/
	void SetDefaultLanguage(const std::string languageCode);

private:
	unsigned char _idSeparator, _langSeperator, _varSeparator;	///< Separators
	std::map<std::string, StringTemplate>* _templates;	///< Stores templates
	std::string* _defaultLang;		///< Default language code
};

#endif