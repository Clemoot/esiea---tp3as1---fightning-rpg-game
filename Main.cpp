#include <iostream>

#include "Utils/WeaponLoader.h"
#include "Utils/CharacterLoader.h"

#include "Characters/Characters.h"
#include "Weapons/Weapons.h"
#include "Instances/FightInstance.h"

TurnAction* OnTurnChanged(CharacterInstance& c, TurnOptions options) {
	std::cout << std::endl << "Turn: " << c.InstantiatedCharacter().Name() << " (" << c.Resources().Life() << " / " << c.Stats().Life() << " HP)" << std::endl;
	
	size_t i, option_index, target_index;
	do {
		do {
			std::cout << "Skills: " << std::endl;
			for (i = 0; i < options.size(); i++)
				std::cout << (i + 1) << "- " << options[i].skill.Name() << std::endl;
			std::cout << "Your choice:  ";
			std::cin >> option_index;
			option_index--;
		} while (option_index < 0 || option_index >= options.size());

		std::vector<CharacterInstance*> targets = options[option_index].targets;
		target_index = 0;
		do {
			std::cout << std::endl << "Target(s) for " << options[option_index].skill.Name() << ":" << std::endl;
			for (i = 0; i < targets.size(); i++)
				std::cout << (i + 1) << "- " << targets[i]->InstantiatedCharacter().Name() << std::endl;
			std::cout << (targets.size() + 1) << "- Back to skills" << std::endl;
			std::cout << "Your choice:  ";
			std::cin >> target_index;
			target_index--;
		} while (target_index < 0 || target_index >= targets.size() + 1);

		if (target_index == targets.size())
			continue;
		break;
	} while (true);

	TurnAction* action = new TurnAction{ *options[option_index].targets[target_index],  options[option_index].skill };

	return action;
}

int main(int argc, char** argv) {
	WeaponDatabase wdb;
	WLoader.SetDefaultDatabase(&wdb);
	WLoader.LoadFile("config/weapons.jdc");

	CharacterDatabase cdb;
	CLoader.SetDefaultDatabase(&cdb);
	CLoader.LoadFile("config/characters.jdc");

	std::string character_names[TEAMS_PER_FIGHT][CHARACTERS_PER_TEAM] = {
		{ "Gandalf", "Leoric", "Garona" },
		{ "Sylvanas Coursevent", "Jaina Portvaillant", "Oliver Queen" }
	};

	std::string weapon_names[TEAMS_PER_FIGHT][CHARACTERS_PER_TEAM] = {
		{ "Baguette de Sureau", "Excalibur", "Perce-bourse" },
		{ "Thori'dal", "Atiesh", "Gandiva" }
	};

	Team t[TEAMS_PER_FIGHT] = { 0 };

	for(int i = 0; i < TEAMS_PER_FIGHT; i++)
		for (int j = 0; j < CHARACTERS_PER_TEAM; j++) {
			Weapon* w = wdb.FindByName(weapon_names[i][j])->Copy();
			Character* c = cdb.FindByName(character_names[i][j])->Copy();

			c->Equip(w);

			t[i][j] = c;
		}


	FightInstance fightInstance(t, OnTurnChanged);
	
	while (!fightInstance.Finished()) {
		fightInstance.NextTurn();
	}

	for (int i = 0; i < TEAMS_PER_FIGHT; i++)
		for (int j = 0; j < CHARACTERS_PER_TEAM; j++) {
			delete t[i][j]->EquipedWeapon();
			delete t[i][j];
		}

	return true;
}