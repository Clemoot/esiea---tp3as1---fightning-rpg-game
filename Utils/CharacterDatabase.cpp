/**
	\file CharacterDatabase.cpp
	\brief CharacterDatabase declaration
	\author Garnier Cl�ment
*/

#include "CharacterDatabase.h"

CharacterDatabase::CharacterDatabase() :
	_characters(new std::vector<Character*>())
{}

CharacterDatabase::~CharacterDatabase()
{
	if (!_characters)
		return;
	delete _characters;
	_characters = nullptr;
}

bool CharacterDatabase::AddCharacter(Character& character)
{
	if (!_characters)	return false;
	if (__AlreadyInDatabase(character))	return false;

	_characters->push_back(character.Copy());

	return true;
}

Character* CharacterDatabase::FindByName(std::string name)
{
	auto pos = std::find_if(_characters->begin(), _characters->end(), [&](Character* c) {
		if (!c)	return false;
		return name == c->Name();
	});
	if (pos != _characters->end())
		return *pos;
	return nullptr;
}

bool CharacterDatabase::__AlreadyInDatabase(Character& character) const
{
	auto pos = std::find_if(_characters->begin(), _characters->end(), [&](Character* c) {
		if (!c)	return false;
		return character == *c;
	});
	return pos != _characters->end();
}
