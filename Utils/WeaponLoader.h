/**
	\file WeaponLoader.h
	\brief WeaponLoader declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "WeaponDatabase.h"
#include <map>

/**
	\class WeaponLoader
	\brief Loads Weapon configuration files
*/
class JEU_DE_COMBAT_API WeaponLoader {
public:
	/**
		\brief WeaponLoader constructor
		\param defaultDatabase Default dattabase to store weapons
	*/
	WeaponLoader(WeaponDatabase* defaultDatabase = nullptr);
	/// WeaponLoader destructor
	~WeaponLoader();

	/**
		\brief Loads configuration file
		\param path Path of the file to load
		\param db Database storing the loaded content
	*/
	void LoadFile(std::string path, WeaponDatabase* db = nullptr);
	/**
		\brief Loads configuration strings
		\param content Content to load
		\param db Database storing the loaded content
	*/
	void LoadString(std::string content, WeaponDatabase* db = nullptr);

	/// Default database setter
	void SetDefaultDatabase(WeaponDatabase* db);

private:
	WeaponDatabase* _db;	///< Default database

	/**
		\brief Creates a weapon from configuration values
		\param config Map containing configuration values
		\return Weapon
	*/
	static Weapon* __ConfigToWeapon(std::map<std::string, std::string>& config);
	/**
		\brief Converts a string into a float value
		\param value String containing the float value
		\return Float value
	*/
	static float __StringToFloat(std::string value);
	/**
		\brief Converts a string into a uint32 value
		\param value String containing the uint32 value
		\return uint32 value
	*/
	static uint32_t __StringToUInt32(std::string value);
};

extern WeaponLoader WLoader;		///< Service
