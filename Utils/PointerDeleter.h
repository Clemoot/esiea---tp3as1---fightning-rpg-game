/**
	\file PointerDeleter.h
	\brief PointerDeleter definition
	\author Garnier Cl�ment
*/

#pragma once

#include <functional>
#include "LibExport.h"

/**
	\class PointerDeleter
	\brief Predicate which delete pointers in STD containers
*/
template<class T>
class JEU_DE_COMBAT_API PointerDeleter{
public:
	/**
		\brief PointerDeleter constructor
		\param testFunction Function determining if the pointer should be deleted
	*/
	PointerDeleter(std::function<bool(T*&)> testFunction): 
		_testFunction(testFunction)
	{}
	/// PointerDeleter destructor
	~PointerDeleter() {}

	/**
		\brief Called on each container pointer
		\param pointer Pointer in the container
		\return true if the pointer should be removed from the container, false otherwise
	*/
	bool operator()(T*& pointer) {
		bool res = _testFunction(pointer);
		if (res && pointer) {
			delete pointer;
			pointer = nullptr;
		}
		return res;
	}

private:
	std::function<bool(T*&)> _testFunction;	///< Test function
};