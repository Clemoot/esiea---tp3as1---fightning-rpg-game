/**
	\file Exceptions.h
	\brief All Exception declarations
	\author Garnier Cl�ment
*/

#pragma once

#ifndef EXCEPTIONS_INCLUDED
/// Exception.h included
#define EXCEPTIONS_INCLUDED

#include "../Utils/LibExport.h"

#include <iostream>
#include "SST.h"

/// Default language
#define DEFAULT_LANGUAGE "en"

/**
	\class Exception
	\brief Basic exception
*/
class JEU_DE_COMBAT_API Exception
{
public:
	/// Exception constructor
	Exception(std::string msg);
	/// Exception desctructor
	~Exception();

	std::string Message() const;	///< Message getter


	/**
		\brief Prints an Exception in a stream
		\param s Stream output
		\param e Exception to print
	*/
	friend std::ostream& operator<<(std::ostream& s, const Exception& e);

private:
	std::string _msg;	///< Message
};

std::ostream& operator<<(std::ostream& s, const Exception& e);

/**
	\class CannotCastException
	\brief Throwed if the character cannot cast the skill
*/
class JEU_DE_COMBAT_API CannotCastException: public Exception {
public:
	/**
		\brief CannotCastException constructor
		\param cName Caster name
		\param sName Skill name
	*/
	CannotCastException(std::string cName, std::string sName);
};

/**
	\class CasterNoWeaponEquipedException
	\brief Throwed if the caster do not have an equiped weapon
*/
class JEU_DE_COMBAT_API CasterNoWeaponEquipedException: public Exception {
public:
	/**
		\brief CasterNoWeaponEquipedException constructor
		\param cName Caster name
	*/
	CasterNoWeaponEquipedException(std::string cName);
};

/**
	\class TargetNoWeaponEquipedException
	\brief Throwed if target do not have an equiped weapon
*/
class JEU_DE_COMBAT_API TargetNoWeaponEquipedException: public Exception {
public:
	/**
		\brief TargetNoWeaponEquipedException constructor
		\param cName Caster name
	*/
	TargetNoWeaponEquipedException(std::string cName);
};

/**
	\class NotAMeleeWeaponException
	\brief Throwed if the equiped weapon is not a melee weapon
*/
class JEU_DE_COMBAT_API NotAMeleeWeaponException: public Exception {
public:
	/**
		\brief NotAMeleeWeaponException constructor
		\param wName Weapon name
	*/
	NotAMeleeWeaponException(std::string wName);
};

/**
	\class BrokenWeaponException
	\brief Throwed if the equiped weapon is broken
*/
class JEU_DE_COMBAT_API BrokenWeaponException: public Exception {
public:
	/**
		\brief BrokenWeaponException constructor
		\param wName Weapon name
	*/
	BrokenWeaponException(std::string wName);
};

/**
	\class NotABowException
	\brief Throwed if the equiped weapon is not a bow
*/
class JEU_DE_COMBAT_API NotABowException: public Exception {
public:
	/**
		\brief NotABowException constructor
		\param wName Weapon name
	*/
	NotABowException(std::string wName);
};

/**
	\class NotAStaffException
	\brief Throwed if the equiped weapon is not a staff
*/
class JEU_DE_COMBAT_API NotAStaffException: public Exception {
public:
	/**
		\brief NotAStaffException constructor
		\param wName Weapon name
	*/
	NotAStaffException(std::string wName);
};

/**
	\class NotAnAllyTargetException
	\brief Throwed if the target is not an ally
*/
class JEU_DE_COMBAT_API NotAnAllyTargetException: public Exception {
public:
	/**
		\brief NotAnAllyTargetException constructor
		\param cName Caster name
	*/
	NotAnAllyTargetException(std::string cName);
};

/**
	\class NotAnOpponentTargetException
	\brief Throwed if the target is not an opponent
*/
class JEU_DE_COMBAT_API NotAnOpponentTargetException: public Exception {
public:
	/**
		\brief NotAnOpponentTargetException constructor
		\param cName Caster name
	*/
	NotAnOpponentTargetException(std::string cName);
};

/**
	\class NotHimSelfTargetException
	\brief Throwed if the target is himself
*/
class JEU_DE_COMBAT_API NotHimSelfTargetException: public Exception {
public:
	/**
		\brief NotHimSelfTargetException constructor
	*/
	NotHimSelfTargetException();
};

/**
	\class DeadCasterException
	\brief Throwed if the caster is dead
*/
class JEU_DE_COMBAT_API DeadCasterException: public Exception {
public:
	/**
		\brief DeadCasterException constructor
		\param cName Caster name
	*/
	DeadCasterException(std::string cName);
};

/**
	\class DeadTargetException
	\brief Throwed if the target is dead
*/
class JEU_DE_COMBAT_API DeadTargetException: public Exception {
public:
	/**
		\brief DeadTargetException constructor
		\param cName Caster name
	*/
	DeadTargetException(std::string cName);
};

/**
	\class TargetNotPoisonedException
	\brief Throwed if the target is not poisoned
*/
class JEU_DE_COMBAT_API TargetNotPoisonedException: public Exception {
public:
	/**
		\brief TargetNotPoisonedException constructor
		\param cName Caster name
	*/
	TargetNotPoisonedException(std::string cName);
};

/**
	\class NotEnoughResourcesException
	\brief Throwed if the caster do not have anough resources to csat the skill
*/
class JEU_DE_COMBAT_API NotEnoughResourcesException: public Exception {
public:
	/**
		\brief NotEnoughResourcesException constructor
		\param cName Caster name
		\param sName Skill name
	*/
	NotEnoughResourcesException(std::string cName, std::string sName);
};

/**
	\class NoMoreUsageException
	\brief Throwed if the skill cannot be used anymore
*/
class JEU_DE_COMBAT_API NoMoreUsageException: public Exception {
public:
	/**
		\brief NoMoreUsageException constructor
		\param cName Caster name
		\param sName Skill name
	*/
	NoMoreUsageException(std::string cName, std::string sName);
};

/**
	\class StillInCooldownException
	\brief Throwed if the skill still is in cooldown
*/
class JEU_DE_COMBAT_API StillInCooldownException: public Exception {
public:
	/**
		\brief StillInCooldownException constructor
		\param sName Skill name
	*/
	StillInCooldownException(std::string sName);
};

/**
	\class FunctionWithWeaponNotDefinedException
	\brief Throwed if the damage function for hands is not defined
*/
class JEU_DE_COMBAT_API FunctionWithWeaponNotDefinedException: public Exception {
public:
	/**
		\brief FunctionWithWeaponNotDefinedException constructor
	*/
	FunctionWithWeaponNotDefinedException();
};

/**
	\class FunctionWithoutWeaponNotDefinedException
	\brief Throwed if the damage function for weapon is not defined
*/
class JEU_DE_COMBAT_API FunctionWithoutWeaponNotDefinedException: public Exception {
public:
	/**
		\brief FunctionWithWeaponNotDefinedException constructor
	*/
	FunctionWithoutWeaponNotDefinedException();
};

/**
	\class WeaponCannotBeUsedException
	\brief Throwed if the weapon cannot be used
*/
class JEU_DE_COMBAT_API WeaponCannotBeUsedException: public Exception {
public:
	/**
		\brief WeaponCannotBeUsedException constructor
		\param wName Weapon name
	*/
	WeaponCannotBeUsedException(std::string wName);
};

#endif