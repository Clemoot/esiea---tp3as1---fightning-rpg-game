/**
	\file CharacterLoader.cpp
	\brief CharacterLoader declaration
	\author Garnier Cl�ment
*/

#include "CharacterLoader.h"
#include <algorithm>
#include <fstream>
#include <sstream>

#include "../Characters/Characters.h"

CharacterLoader CLoader;

CharacterLoader::CharacterLoader(CharacterDatabase* defaultDatabase) : _db(defaultDatabase)
{}

CharacterLoader::~CharacterLoader() {}

void CharacterLoader::LoadFile(std::string path, CharacterDatabase* db)
{
	if (path.empty())	return;

	std::ifstream stream(path);
	if (!stream.is_open())	return;

	std::string content;
	content.assign(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());

	stream.close();

	LoadString(content, db);
}

void CharacterLoader::LoadString(std::string content, CharacterDatabase* db)
{
	CharacterDatabase* targetDb = db;
	if (content.empty())	return;
	if (!db) {
		if (_db)	targetDb = _db;
		else		return;
	}

	std::stringstream ss(content);
	std::string line;
	std::map<std::string, std::string> tmpValues;

	bool readingCharacter = false;

	while (std::getline(ss, line)) {
		if (line.empty())	continue;

		if (line.find("EndCharacter") != std::string::npos) {
			readingCharacter = false;
			Character* tmpCharacter = __ConfigToCharacter(tmpValues);
			if (tmpCharacter) {
				targetDb->AddCharacter(*tmpCharacter);
				delete tmpCharacter;
			}
			tmpCharacter = nullptr;
			continue;
		}
		else if (line.find("Character") != std::string::npos) {
			readingCharacter = true;
			tmpValues.clear();
			continue;
		}

		if (!readingCharacter)	continue;

		auto keyStartPos = std::find_if_not(line.begin(), line.end(), std::isspace);
		auto keyEndPos = std::find_if(keyStartPos, line.end(), std::isspace);

		auto valueStartPos = std::find_if_not(keyEndPos, line.end(), std::isspace);

		if (keyStartPos == line.end() || keyEndPos == line.end() || valueStartPos == line.end())	continue;

		std::string key = std::string(keyStartPos, keyEndPos);
		std::string value = std::string(valueStartPos, line.end());

		tmpValues[key] = value;
	}

	ss.str("");
}

void CharacterLoader::SetDefaultDatabase(CharacterDatabase* db)
{
	_db = db;
}

Character* CharacterLoader::__ConfigToCharacter(std::map<std::string, std::string>& config)
{
	Character* res = nullptr;

	try {
		std::string sType = config["Classe"];
		if (sType == "Mage")			res = new Mage();
		else if (sType == "Guerrier")	res = new Warrior();
		else if (sType == "Archer")		res = new Ranger();
		else if (sType == "Voleur")		res = new Rogue();
	}
	catch (const std::out_of_range&) {
		return nullptr;
	}

	if (!res)	return nullptr;

	try {
		std::string sName = config["Nom"];
		res->Name(sName);
	}
	catch (const std::out_of_range&) {}

	{
		BaseStats tmp = res->Stats();
		try {
			std::string sHP = config["HP"];
			tmp.Life(__StringToFloat(sHP));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sAttack = config["Attaque"];
			tmp.Attack(__StringToFloat(sAttack));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sDefense = config["Defense"];
			tmp.Defense(__StringToFloat(sDefense));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sDexterity = config["Agilite"];
			tmp.Dexterity(__StringToFloat(sDexterity));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sIntelligence = config["Intelligence"];
			tmp.Intelligence(__StringToFloat(sIntelligence));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sSpeed = config["Vitesse"];
			tmp.Speed(__StringToFloat(sSpeed));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sDodge = config["Dodge"];
			tmp.Dodge(__StringToFloat(sDodge));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sSpecial = config["Special"];
			tmp.Parry(__StringToFloat(sSpecial));
			tmp.PoisonRate(__StringToFloat(sSpecial));
			tmp.BonusMana(__StringToFloat(sSpecial));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sParry = config["Parry"];
			tmp.Parry(__StringToFloat(sParry));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sPoisonRate = config["PoisonRate"];
			tmp.PoisonRate(__StringToFloat(sPoisonRate));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sMana = config["Mana"];
			tmp.BonusMana(__StringToFloat(sMana));
		}
		catch (const std::out_of_range&) {}

		res->Stats(tmp);
	}

	return res;
}

float CharacterLoader::__StringToFloat(std::string value)
{
	float res = .0f;
	std::stringstream(value) >> res;
	return res;
}

uint32_t CharacterLoader::__StringToUInt32(std::string value)
{
	uint32_t res = 0;
	std::stringstream(value) >> res;
	return res;
}

