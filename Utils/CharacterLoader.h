/**
	\file CharacterLoader.h
	\brief CharacterLoader declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "CharacterDatabase.h"
#include <map>

/**
	\class CharacterLoader
	\brief Loads Character configuration files
*/
class JEU_DE_COMBAT_API CharacterLoader {
public:
	/**
		\brief CharacterLoader constructor
		\param defaultDatabase Default dattabase to store characters
	*/
	CharacterLoader(CharacterDatabase* defaultDatabase = nullptr);
	/// CharacterLoader destructor
	~CharacterLoader();

	/**
		\brief Loads configuration file 
		\param path Path of the file to load
		\param db Database storing the loaded content
	*/
	void LoadFile(std::string path, CharacterDatabase* db = nullptr);
	/**
		\brief Loads configuration strings
		\param content Content to load
		\param db Database storing the loaded content
	*/
	void LoadString(std::string content, CharacterDatabase* db = nullptr);

	/// Default database setter
	void SetDefaultDatabase(CharacterDatabase* db);

private:
	CharacterDatabase* _db;	///< Default database

	/**
		\brief Creates a character from configuration values
		\param config Map containing configuration values
		\return Character
	*/
	static Character* __ConfigToCharacter(std::map<std::string, std::string>& config);
	/**
		\brief Converts a string into a float value
		\param value String containing the float value
		\return Float value
	*/
	static float __StringToFloat(std::string value);
	/**
		\brief Converts a string into a uint32 value
		\param value String containing the uint32 value
		\return uint32 value
	*/
	static uint32_t __StringToUInt32(std::string value);
};

extern CharacterLoader CLoader;		///< Service
