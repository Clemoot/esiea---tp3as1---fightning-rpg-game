/**
	\file Exceptions.cpp
	\brief all Exceptions declarations
	\author Garnier Cl�ment
*/

#include "Exceptions.h"

Exception::Exception(std::string msg): _msg(msg) {}

Exception::~Exception()	 {}

std::string Exception::Message() const
{
	return _msg;
}

std::ostream& operator<<(std::ostream& s, const Exception& e)
{
	return s << e._msg;
}

CannotCastException::CannotCastException(std::string cName, std::string sName):
	Exception(SST.Find("cannot_cast", DEFAULT_LANGUAGE).Replace("character", cName).Replace("skill", sName).Str()) {}

CasterNoWeaponEquipedException::CasterNoWeaponEquipedException(std::string cName):
	Exception(SST.Find("character_no_weapon", DEFAULT_LANGUAGE).Replace("character", cName).Str()) {}

TargetNoWeaponEquipedException::TargetNoWeaponEquipedException(std::string cName):
	Exception(SST.Find("character_no_weapon", DEFAULT_LANGUAGE).Replace("character", cName).Str()) {}

NotAnAllyTargetException::NotAnAllyTargetException(std::string cName):
	Exception(SST.Find("not_an_ally", DEFAULT_LANGUAGE).Replace("character", cName).Str()) {}

NotAnOpponentTargetException::NotAnOpponentTargetException(std::string cName):
	Exception(SST.Find("not_an_opponent", DEFAULT_LANGUAGE).Replace("character", cName).Str()) {}

NotHimSelfTargetException::NotHimSelfTargetException():
	Exception(SST.Find("not_himself", DEFAULT_LANGUAGE).Str()) {}

DeadCasterException::DeadCasterException(std::string cName):
	Exception(SST.Find("character_dead", DEFAULT_LANGUAGE).Replace("character", cName).Str()) {}

DeadTargetException::DeadTargetException(std::string cName):
	Exception(SST.Find("character_dead", DEFAULT_LANGUAGE).Replace("character", cName).Str()) {}

TargetNotPoisonedException::TargetNotPoisonedException(std::string cName):
	Exception(SST.Find("not_poisoned", DEFAULT_LANGUAGE).Replace("character", cName).Str()) {}

NotAMeleeWeaponException::NotAMeleeWeaponException(std::string wName):
	Exception(SST.Find("not_melee_weapon", DEFAULT_LANGUAGE).Replace("weapon", wName).Str()) {}

BrokenWeaponException::BrokenWeaponException(std::string wName):
	Exception(SST.Find("broken_weapon", DEFAULT_LANGUAGE).Replace("weapon", wName).Str()) {}

NotEnoughResourcesException::NotEnoughResourcesException(std::string cName, std::string sName):
	Exception(SST.Find("not_enough_resources", DEFAULT_LANGUAGE).Replace("character", cName).Replace("skill", sName).Str()) {}

NotABowException::NotABowException(std::string wName):
	Exception(SST.Find("not_bow", DEFAULT_LANGUAGE).Replace("weapon", wName).Str()) {}

NotAStaffException::NotAStaffException(std::string wName):
	Exception(SST.Find("not_staff", DEFAULT_LANGUAGE).Replace("weapon", wName).Str()) {}

NoMoreUsageException::NoMoreUsageException(std::string cName, std::string sName):
	Exception(SST.Find("no_more_usage", DEFAULT_LANGUAGE).Replace("character", cName).Replace("skill", sName).Str()) {}

StillInCooldownException::StillInCooldownException(std::string sName):
	Exception(SST.Find("still_cooldown", DEFAULT_LANGUAGE).Replace("skill", sName).Str()) {}

FunctionWithWeaponNotDefinedException::FunctionWithWeaponNotDefinedException():
	Exception(SST.Find("function_with_weapon", DEFAULT_LANGUAGE).Str()) {}

FunctionWithoutWeaponNotDefinedException::FunctionWithoutWeaponNotDefinedException():
	Exception(SST.Find("function_without_weapon", DEFAULT_LANGUAGE).Str()) {}

WeaponCannotBeUsedException::WeaponCannotBeUsedException(std::string wName):
	Exception(SST.Find("weapon_not_usable", DEFAULT_LANGUAGE).Replace("weapon", wName).Str()) {}
