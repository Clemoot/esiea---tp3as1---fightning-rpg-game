/**
	\file WeaponDatabase.cpp
	\brief WeaponDatabase declaration
	\author Garnier Cl�ment
*/

#include "WeaponDatabase.h"

WeaponDatabase::WeaponDatabase():
	_weapons(new std::vector<Weapon*>())
{}

WeaponDatabase::~WeaponDatabase()
{
	if (!_weapons)
		return;
	delete _weapons;
	_weapons = nullptr;
}

bool WeaponDatabase::AddWeapon(Weapon& weapon)
{
	if(!_weapons)	return false;
	if (__AlreadyInDatabase(weapon))	return false;

	_weapons->push_back(weapon.Copy());

	return true;
}

Weapon* WeaponDatabase::FindByName(std::string name)
{
	auto pos = std::find_if(_weapons->begin(), _weapons->end(), [&](Weapon* c) {
		if (!c)	return false;
		return name == c->Name();
		});
	if (pos != _weapons->end())
		return *pos;
	return nullptr;
}

bool WeaponDatabase::__AlreadyInDatabase(Weapon& weapon) const
{
	auto pos = std::find_if(_weapons->begin(), _weapons->end(), [&](Weapon* w) {
		if (!w)	return false;
		return weapon == *w;
	});
	return pos != _weapons->end();
}
