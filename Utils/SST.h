/**
	\file SST.h
	\brief Create SimpleStringTemplater service
	\author Garnier Cl�ment
*/

#pragma once

#include <SimpleStringTemplater.h>

/// Path to the template files
#define TEMPLATE_FILE_PATH "config/exceptions.template"

extern SimpleStringTemplater SST;	///< SimpleStringTemplater service
