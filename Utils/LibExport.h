/**
	\file LibExport.h
	\brief Pre-processor variables for DLL export/import
	\author Garnier Cl�ment
*/

#pragma once

//#define JEU_DE_COMBAT_LIB_EXPORTS
//#define JEU_DE_COMBAT_LIB_IMPORTS

#ifdef JEU_DE_COMBAT_LIB_EXPORTS
	#define JEU_DE_COMBAT_API __declspec(dllexport)
#else
	#ifdef JEU_DE_COMBAT_LIB_IMPORTS
		#define JEU_DE_COMBAT_API __declspec(dllimport)
	#else
		#define JEU_DE_COMBAT_API
	#endif
#endif