/**
	\file WeaponLoader.cpp
	\brief WeaponLoader declaration
	\author Garnier Cl�ment
*/

#include "WeaponLoader.h"
#include <algorithm>
#include <fstream>
#include <sstream>

#include "../Weapons/Weapons.h"

WeaponLoader WLoader;

WeaponLoader::WeaponLoader(WeaponDatabase* defaultDatabase): _db(defaultDatabase)
{}

WeaponLoader::~WeaponLoader() {}

void WeaponLoader::LoadFile(std::string path, WeaponDatabase* db)
{
	if (path.empty())	return;

	std::ifstream stream(path);
	if (!stream.is_open())	return;

	std::string content;
	content.assign(std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>());

	stream.close();

	LoadString(content, db);
}

void WeaponLoader::LoadString(std::string content, WeaponDatabase* db)
{
	WeaponDatabase* targetDb = db;
	if (content.empty())	return;
	if (!db) {
		if (_db)	targetDb = _db;
		else		return;
	}

	std::stringstream ss(content);
	std::string line;
	std::map<std::string, std::string> tmpValues;

	bool readingWeapon = false;

	while (std::getline(ss, line)) {
		if (line.empty())	continue;
		
		if (line.find("EndWeapon") != std::string::npos) {
			readingWeapon = false;
			Weapon* tmpWeapon = __ConfigToWeapon(tmpValues);
			if (tmpWeapon) {
				targetDb->AddWeapon(*tmpWeapon);
				delete tmpWeapon;
			}
			tmpWeapon = nullptr;
			continue;
		}
		else if (line.find("Weapon") != std::string::npos) {
			readingWeapon = true;
			tmpValues.clear();
			continue;
		}

		if (!readingWeapon)	continue;

		auto keyStartPos = std::find_if_not(line.begin(), line.end(), std::isspace);
		auto keyEndPos = std::find_if(keyStartPos, line.end(), std::isspace);

		auto valueStartPos = std::find_if_not(keyEndPos, line.end(), std::isspace);

		if (keyStartPos == line.end() || keyEndPos == line.end() || valueStartPos == line.end())	continue;

		std::string key = std::string(keyStartPos, keyEndPos);
		std::string value = std::string(valueStartPos, line.end());

		tmpValues[key] = value;
	}

	ss.str("");
}

void WeaponLoader::SetDefaultDatabase(WeaponDatabase * db)
{
	_db = db;
}

Weapon* WeaponLoader::__ConfigToWeapon(std::map<std::string, std::string>& config)
{
	Weapon* res = nullptr;

	try {
		std::string sType = config["Type"];
		if (sType == "Epee")		res = new Sword();
		else if (sType == "Dague")	res = new Dagger();
		else if (sType == "Arc")	res = new Bow();
		else if (sType == "Baton")	res = new Staff();
	} catch(const std::out_of_range&) {
		return nullptr;
	}

	if (!res)	return nullptr;

	try {
		std::string sName = config["Nom"];
		res->Name(sName);
	} catch (const std::out_of_range&) {}

	WeaponStats stats = res->OriginalStats();

	try {
		std::string sDamage = config["Degat"];
		stats.Damage(__StringToFloat(sDamage));
	} catch (const std::out_of_range&) {}

	try {
		std::string sCriticalRate = config["Critique"];
		stats.CriticalRate(__StringToFloat(sCriticalRate));
	} catch(const std::out_of_range&) {}

	{
		BaseStats tmp = res->OriginalStats().BonusStats();
		try {
			std::string sHP = config["HP"];
			tmp.Life(__StringToFloat(sHP));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sAttack = config["Attack"];
			tmp.Attack(__StringToFloat(sAttack));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sDefense = config["Defense"];
			tmp.Defense(__StringToFloat(sDefense));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sDexterity = config["Agilite"];
			tmp.Dexterity(__StringToFloat(sDexterity));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sIntelligence = config["Intelligence"];
			tmp.Intelligence(__StringToFloat(sIntelligence));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sSpeed = config["Vitesse"];
			tmp.Speed(__StringToFloat(sSpeed));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sDodge = config["Dodge"];
			tmp.Dodge(__StringToFloat(sDodge));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sSpecial = config["Special"];
			tmp.Parry(__StringToFloat(sSpecial));
			tmp.PoisonRate(__StringToFloat(sSpecial));
			tmp.BonusMana(__StringToFloat(sSpecial));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sParry = config["Parry"];
			tmp.Parry(__StringToFloat(sParry));
		} catch(const std::out_of_range&) {}

		try {
			std::string sPoisonRate = config["PoisonRate"];
			tmp.PoisonRate(__StringToFloat(sPoisonRate));
		}
		catch (const std::out_of_range&) {}

		try {
			std::string sMana = config["Mana"];
			tmp.BonusMana(__StringToFloat(sMana));
		}
		catch (const std::out_of_range&) {}

		stats.BonusStats(tmp);
	}

	res->Stats(stats);

	if (Weapon::Is<Melee>(*res)) {
		try {
			std::string sDurability = config["Durabilite"];
			Melee& melee = dynamic_cast<Melee&>(*res);
			melee.Durability(__StringToUInt32(sDurability));
		}
		catch (const std::out_of_range&) {}
		catch (const std::bad_cast&) {}
	}
	else if (Weapon::Is<Bow>(*res)) {
		try {
			std::string sNbArrows = config["Fleche"];
			Bow& bow = dynamic_cast<Bow&>(*res);
			bow.NbArrows(__StringToUInt32(sNbArrows));
		}
		catch (const std::out_of_range&) {}
		catch (const std::bad_cast&) {}
	}
	else if (Weapon::Is<Staff>(*res)) {
		try {
			std::string sCost = config["Cout"];
			Staff& staff = dynamic_cast<Staff&>(*res);
			staff.ManaCost(__StringToFloat(sCost));
		}
		catch (const std::out_of_range&) {}
		catch (const std::bad_cast&) {}
	}

	return res;
}

float WeaponLoader::__StringToFloat(std::string value)
{
	float res = .0f;
	std::stringstream(value) >> res;
	return res;
}

uint32_t WeaponLoader::__StringToUInt32(std::string value)
{
	uint32_t res = 0;
	std::stringstream(value) >> res;
	return res;
}

