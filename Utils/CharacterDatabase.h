/**
	\file CharacterDatabase.h
	\brief CharacterDatabase declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "LibExport.h"
#include "../Characters/Character.h"

/**
	\class CharacterDatabase
	\brief Object containing many characters
*/
class JEU_DE_COMBAT_API CharacterDatabase {
public:
	/// CharacterDatabse default constructor
	CharacterDatabase();
	/// CharacterDatabase destructor
	~CharacterDatabase();

	/**
		\brief Adds a character to the database
		\param character Character to add
		\return true if correctly added, false otherwise
	*/
	bool AddCharacter(Character& character);

	/**
		\brief Retrieves a character with its name
		\param name Name of the character to find
		\return null_ptr if not found, the matching character otherwise
	*/
	Character* FindByName(std::string name);

private:
	std::vector<Character*>* _characters;	///< Characters

	/// Checks if a character is already in database
	bool __AlreadyInDatabase(Character& character) const;
};