/**
	\file WeaponDatabase.h
	\brief WeaponDatabase definition
	\author Garnier Cl�ment
*/

#pragma once

#include "LibExport.h"
#include "../Weapons/Weapon.h"

/**
	\class WeaponDatabase
	\brief Object containing many weapons
*/
class JEU_DE_COMBAT_API WeaponDatabase {
public:
	/// WeaponDatabase default constructor
	WeaponDatabase();
	/// CharacterDatabase destructor
	~WeaponDatabase();

	/**
		\brief Adds a weapon to the database
		\param weapon Weapon to add
		\return true if correctly added, false otherwise
	*/
	bool AddWeapon(Weapon& weapon);

	/**
		\brief Retrieves a weapon with its name
		\param name Name of the weapon to find
		\return null_ptr if not found, the matching weapon otherwise
	*/
	Weapon* FindByName(std::string name);

private:
	std::vector<Weapon*>* _weapons;	///< Weapons

	/// Checks if a weapon is already in database
	bool __AlreadyInDatabase(Weapon& weapon) const;
};