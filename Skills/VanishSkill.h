/**
	\file VanishSkill.h
	\brief VanishSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef VANISH_SKILL_INCLUDED
/// VanishSkill.h included
#define VANISH_SKILL_INCLUDED

#include "Skill.h"

/// VanishSkill default name
#define VANISH_SKILL_NAME "Vanish"
/// VanishSkill default cooldown
#define VANISH_SKILL_COOLDOWN 4
/// VanishSkill default target tags
#define VANISH_SKILL_TARGETS { new SelfTarget() }

/**
	\class VanishSkill
	\brief The caster starts aiming
*/
class JEU_DE_COMBAT_API VanishSkill: public Skill {
public:
	/// VanishSkill default constructor
	VanishSkill();
	/**
		\brief VanishSkill copy constructor
		\param other VanishSkill to copy
	*/
	VanishSkill(const VanishSkill& other);
	/// VanishSkill destructor
	~VanishSkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);
};

#endif