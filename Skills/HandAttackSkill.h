/**
	\file HandAttackSkill.h
	\brief HandAttackSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef HAND_ATTACK_SKILL_INCLUDED
/// VanishSkill.h included
#define HAND_ATTACK_SKILL_INCLUDED

#include "Skill.h"
#include "DodgeableSkill.h"
#include "ParriableSkill.h"

/// VanishSkill default name
#define HAND_ATTACK_SKILL_NAME "Attack with Hands"
/// VanishSkill default cooldown
#define HAND_ATTACK_SKILL_COOLDOWN 0
/// VanishSkill default target tags
#define HAND_ATTACK_SKILL_TARGETS { new OpponentTarget() }

/**
	\class HandAttackSkill
	\brief The caster starts aiming
*/
class JEU_DE_COMBAT_API HandAttackSkill: public DodgeableSkill, public ParriableSkill {
public:
	/// HandAttackSkill default constructor
	HandAttackSkill();
	/**
		\brief HandAttackSkill copy constructor
		\param other HandAttackSkill to copy
	*/
	HandAttackSkill(const HandAttackSkill& other);
	/// HandAttackSkill destructor
	~HandAttackSkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);
};

#endif