/**
	\file BattleCrySkill.cpp
	\brief BattleCrySkill declaration
	\author Garnier Cl�ment
*/

#include "BattleCrySkill.h"
#include <algorithm>
#include "../Characters/Warrior.h"
#include "Targets/SelfTarget.h"
#include "../Auras/TauntedAura.h"
#include "../Utils/Exceptions.h"

BattleCrySkill::BattleCrySkill():
	Skill(BATTLE_CRY_SKILL_NAME, BATTLE_CRY_SKILL_COOLDOWN, BATTLE_CRY_SKILL_TARGETS)
{}

BattleCrySkill::BattleCrySkill(const BattleCrySkill & other): Skill(other)
{}

BattleCrySkill::~BattleCrySkill()
{
}

bool BattleCrySkill::Cast(CharacterInstance& caster, CharacterInstance& target)
{
	if (!CastConditions(caster, target))
		return false;

	std::cout << caster.InstantiatedCharacter().Name() << " casts " << _name << std::endl;

	auto opponents = caster.Opponents();
	std::for_each(opponents.begin(), opponents.end(), [&](CharacterInstance* opponent) {
		if (!opponent)	return;
		CharacterAura* newAura = TauntedAura::Instantiate(opponent, &caster);
		if(newAura)
			if (!opponent->AddAura(newAura)) 
				std::cerr << "Couldn't apply Battle Cry CharacterAura (TauntedAura) on opponent" << std::endl;
	});

	return true;
}

Skill* BattleCrySkill::Copy()
{
	return new BattleCrySkill(*this);
}

bool BattleCrySkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (&caster != &target)
		throw NotHimSelfTargetException();
	if (!caster.Is<Warrior>())
		throw CannotCastException(caster.InstantiatedCharacter().Name(), _name);
	return true;
}
