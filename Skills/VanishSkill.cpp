/**
	\file VanishSkill.cpp
	\brief VanishSkill declaration
	\author Garnier Cl�ment
*/

#include "VanishSkill.h"
#include "../Auras/InvisibleAura.h"
#include "../Characters/Rogue.h"
#include "Targets/SelfTarget.h"
#include "../Utils/Exceptions.h"

VanishSkill::VanishSkill():
	Skill(VANISH_SKILL_NAME, VANISH_SKILL_COOLDOWN, VANISH_SKILL_TARGETS)
{}

VanishSkill::VanishSkill(const VanishSkill & other): Skill(other)
{}

VanishSkill::~VanishSkill()
{
}

bool VanishSkill::Cast(CharacterInstance& caster, CharacterInstance& target)
{
	if (!CastConditions(caster, target))
		return false;
	
	std::cout << caster.InstantiatedCharacter().Name() << " casts " << _name << std::endl;
	CharacterAura* newAura = InvisibleAura::Instantiate(&caster);
	if(newAura)
		caster.AddAura(newAura);

	return true;
}

Skill* VanishSkill::Copy()
{
	return new VanishSkill(*this);
}

bool VanishSkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (&caster != &target)
		throw NotHimSelfTargetException();
	if (!caster.Is<Rogue>())
		throw CannotCastException(caster.InstantiatedCharacter().Name(), _name);
	return true;
}