/**
	\file CurePoisonSkill.h
	\brief CurePoisonSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef CURE_POISON_SKILL_INCLUDED
/// VanishSkill.h included
#define CURE_POISON_SKILL_INCLUDED

#include "Skill.h"

/// VanishSkill default name
#define CURE_POION_SKILL_NAME "Cure Poison"
/// VanishSkill default cooldown
#define CURE_POION_SKILL_COOLDOWN 0
/// VanishSkill default target tags
#define CURE_POION_SKILL_TARGETS { new AllyTarget() }

/**
	\class CurePoisonSkill
	\brief The caster starts aiming
*/
class JEU_DE_COMBAT_API CurePoisonSkill: public Skill {
public:
	/// CurePoisonSkill default constructor
	CurePoisonSkill(uint8_t nbUsage);
	/**
		\brief CurePoisonSkill copy constructor
		\param other CurePoisonSkill to copy
	*/
	CurePoisonSkill(const CurePoisonSkill& other);
	/// CurePoisonSkill destructor
	~CurePoisonSkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);

private:
	CharacterAura* __FindPoisonedAura(std::vector<CharacterAura*> auras) const;

	uint8_t _nbUsage;
};

#endif