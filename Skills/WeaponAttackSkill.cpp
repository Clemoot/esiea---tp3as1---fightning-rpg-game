/**
	\file WeaponAttackSkill.cpp
	\brief WeaponAttackSkill declaration
	\author Garnier Cl�ment
*/

#include "WeaponAttackSkill.h"
#include "Targets/OpponentTarget.h"
#include "../Utils/Exceptions.h"
#include "../Weapons/Melee.h"

WeaponAttackSkill::WeaponAttackSkill() :
	Skill(WEAPON_ATTACK_SKILL_NAME, WEAPON_ATTACK_SKILL_COOLDOWN, WEAPON_ATTACK_SKILL_TARGETS),
	DodgeableSkill(WEAPON_ATTACK_SKILL_NAME),
	ParriableSkill(WEAPON_ATTACK_SKILL_NAME)
{}

WeaponAttackSkill::WeaponAttackSkill(const WeaponAttackSkill & other): 
	Skill(other),
	DodgeableSkill(WEAPON_ATTACK_SKILL_NAME),
	ParriableSkill(WEAPON_ATTACK_SKILL_NAME)
{}

WeaponAttackSkill::~WeaponAttackSkill()
{}

bool WeaponAttackSkill::Cast(CharacterInstance& caster, CharacterInstance& target)
{
	if (!CastConditions(caster, target))
		return false;

	std::cout << caster.InstantiatedCharacter().Name() << " casts " << _name << std::endl;

	try {
		caster.EquipedWeapon()->Use(caster);
		float dmg = caster.InstantiatedCharacter().WeaponDamageFunction()(*caster.EquipedWeapon(), caster.Stats(), target.Stats());
		target.Hit(dmg);

		std::cout << target.InstantiatedCharacter().Name() << " loses " << dmg << " HP !" << std::endl;

		caster.EquipedWeapon()->Used();
	}
	catch (const Exception & e) {
		std::cerr << "Error: " << e << std::endl;
	}

	return true;
}

Skill* WeaponAttackSkill::Copy()
{
	return new WeaponAttackSkill(*this);
}

bool WeaponAttackSkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (target.Dead())
		throw DeadTargetException(target.InstantiatedCharacter().Name());
	if (!caster.IsOpponent(&target))
		throw NotAnOpponentTargetException(target.InstantiatedCharacter().Name());
	Weapon* weapon = caster.EquipedWeapon();
	if (!weapon)
		throw CasterNoWeaponEquipedException(caster.InstantiatedCharacter().Name());
	if (!weapon->Usable(caster))
		throw WeaponCannotBeUsedException(weapon->Name());
	return true;
}
