/**
	\file RepairSkill.cpp
	\brief RepairSkill declaration
	\author Garnier Cl�ment
*/

#include "RepairSkill.h"
#include "../Weapons/Melee.h"
#include "../Characters/Warrior.h"
#include "Targets/AllyTarget.h"
#include "Targets/SelfTarget.h"
#include "../Utils/Exceptions.h"

RepairSkill::RepairSkill(): 
	Skill(REPAIR_SKILL_NAME, REPAIR_SKILL_COOLDOWN, REPAIR_SKILL_TARGETS)
{}

RepairSkill::RepairSkill(const RepairSkill & other): Skill(other)
{}

RepairSkill::~RepairSkill()
{
}

bool RepairSkill::Cast(CharacterInstance& caster, CharacterInstance& target)
{
	if (!CastConditions(caster, target))
		return false;

	std::cout << caster.InstantiatedCharacter().Name() << " casts " << _name << std::endl;

	Weapon& weapon = *target.EquipedWeapon();
	try {
		Melee& m = dynamic_cast<Melee&>(weapon);	// Arme = Melee ?

		if (m.Broken())		// Si l'arme est cass�e, +1 sinon al�atoire
			m.Repair(REPAIR_IF_BROKEN);
		else {
			int repair = (rand() % 13) + 3;		// nombre al�atoire entre 3 et 15
			m.Repair(repair);
		}

		return true;
	}
	catch (const std::bad_cast&) {
		throw NotAMeleeWeaponException(weapon.Name());
	}
	return false;
}

Skill* RepairSkill::Copy()
{
	return new RepairSkill(*this);
}

bool RepairSkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (target.Dead())
		throw DeadTargetException(target.InstantiatedCharacter().Name());
	if (!caster.IsAlly(&target) && &caster != &target)
		throw NotAnAllyTargetException(target.InstantiatedCharacter().Name());
	if (!caster.Is<Warrior>())
		throw CannotCastException(caster.InstantiatedCharacter().Name(), _name);
	Weapon* weapon = target.EquipedWeapon();
	if (!weapon)
		throw TargetNoWeaponEquipedException(target.InstantiatedCharacter().Name());
	try {
		dynamic_cast<Melee&>(*weapon);
		return true;
	}
	catch (const std::bad_cast&) {
		throw NotAMeleeWeaponException(weapon->Name());
	}
	return true;
}

