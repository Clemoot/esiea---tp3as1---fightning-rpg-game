/**
	\file SelfTarget.h
	\brief SelfTarget declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef SELF_TARGET_INCLUDED
#define SELF_TARGET_INCLUDED

#include "Target.h"

/**
	\class SelfTarget
	\brief Targets the instance himself
*/
class JEU_DE_COMBAT_API SelfTarget: public Target {
public:
	/// SelfTarget default constructor
	SelfTarget();
	/// SelfTarget destructor
	~SelfTarget();

	/// Retrieves targets from an instance
	std::vector<CharacterInstance*> Targets(CharacterInstance& character);

	/// Tag description
	std::string Description();

	/// Copy the target tag
	Target* Copy() const;
};

#endif