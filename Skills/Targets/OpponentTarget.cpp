/**
	\file OpponentTarget.cpp
	\brief OpponentTarget declaration
	\author Garnier Cl�ment
*/

#include "OpponentTarget.h"

OpponentTarget::OpponentTarget()
{
}

OpponentTarget::~OpponentTarget()
{
}

std::vector<CharacterInstance*> OpponentTarget::Targets(CharacterInstance& character)
{
	return character.Opponents();
}

std::string OpponentTarget::Description()
{
	return "OpponentTarget";
}

Target* OpponentTarget::Copy() const
{
	return new OpponentTarget(*this);
}
