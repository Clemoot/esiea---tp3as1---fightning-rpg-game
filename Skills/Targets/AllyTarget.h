/**
	\file AllyTarget.h
	\brief AllyTarget declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef ALLY_TARGET_INCLUDED
#define ALLY_TARGET_INCLUDED

#include "Target.h"

/**
	\class AllyTarget
	\brief Targets the allies
*/
class JEU_DE_COMBAT_API AllyTarget: public Target {
public:
	/// AllyTarget default constructor
	AllyTarget();
	/// AllyTarget destructor
	~AllyTarget();

	/// Retrieves targets from an instance
	std::vector<CharacterInstance*> Targets(CharacterInstance& character);

	/// Tag description
	std::string Description();

	/// Copy the target tag
	Target* Copy() const;
};

#endif