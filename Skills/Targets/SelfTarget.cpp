/**
	\file SelfTarget.cpp
	\brief SelfTarget declaration
	\author Garnier Cl�ment
*/

#include "SelfTarget.h"

SelfTarget::SelfTarget()
{
}

SelfTarget::~SelfTarget()
{
}

std::vector<CharacterInstance*> SelfTarget::Targets(CharacterInstance& character)
{
	return { character.Self() };
}

std::string SelfTarget::Description()
{
	return "SelfTarget";
}

Target* SelfTarget::Copy() const
{
	return new SelfTarget(*this);
}
