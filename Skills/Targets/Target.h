/**
	\file Target.h
	\brief Target declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef TARGET_INCLUDED
#define TARGET_INCLUDED

#include <iostream>
#include <vector>

#include "../../Instances/CharacterInstance.h"

/**
	\class Target
	\brief Basic Target tag
*/
class JEU_DE_COMBAT_API Target{
public:
	/// Target default constructor
	Target();
	/// Target destructor
	~Target();

	/// Retrieves targets from an instance
	virtual std::vector<CharacterInstance*> Targets(CharacterInstance& character) = 0;

	/// Descrtiption of the tag
	virtual std::string Description() = 0;

	/// Copy the tag
	virtual Target* Copy() const = 0;

	/// Checks if the target tag is castable in the child Target tag
	template<class T>
	static bool Is(Target& target) {
		try {
			dynamic_cast<T&>(target);
			return true;
		}
		catch (const std::bad_cast&) {
			return false;
		}
	}
};

#endif