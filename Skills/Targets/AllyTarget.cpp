/**
	\file AllyTarget.cpp
	\brief AllyTarget declaration
	\author Garnier Cl�ment
*/

#include "AllyTarget.h"

AllyTarget::AllyTarget()
{
}

AllyTarget::~AllyTarget()
{
}

std::vector<CharacterInstance*> AllyTarget::Targets(CharacterInstance& character)
{
	std::vector<CharacterInstance*> targets = character.Allies();
	return targets;
}

std::string AllyTarget::Description()
{
	return "AllyTarget";
}

Target* AllyTarget::Copy() const
{
	return new AllyTarget(*this);
}
