/**
	\file OpponentTarget.h
	\brief OpponentTarget declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef OPPONENT_TARGET_INCLUDED
#define OPPONENT_TARGET_INCLUDED

#include "Target.h"

/**
	\class OpponentTarget
	\brief Targets the opponents
*/
class JEU_DE_COMBAT_API OpponentTarget: public Target {
public:
	/// OpponentTarget default constructor
	OpponentTarget();
	/// OpponentTarget destructor
	~OpponentTarget();

	/// Retrieves targets from an instance
	std::vector<CharacterInstance*> Targets(CharacterInstance& character);

	/// Tag description
	std::string Description();

	/// Copy the target tag
	Target* Copy() const;
};

#endif