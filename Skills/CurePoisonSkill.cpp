/**
	\file CurePoisonSkill.cpp
	\brief CurePoisonSkill declaration
	\author Garnier Cl�ment
*/

#include "CurePoisonSkill.h"
#include "../Characters/Ranger.h"
#include "Targets/AllyTarget.h"
#include "../Auras/PoisonedAura.h"
#include "../Utils/Exceptions.h"

CurePoisonSkill::CurePoisonSkill(uint8_t nbUsage) :
	Skill(CURE_POION_SKILL_NAME, CURE_POION_SKILL_COOLDOWN, CURE_POION_SKILL_TARGETS),
	_nbUsage(nbUsage)
{}

CurePoisonSkill::CurePoisonSkill(const CurePoisonSkill& other): Skill(other), _nbUsage(other._nbUsage)
{}

CurePoisonSkill::~CurePoisonSkill()
{}

bool CurePoisonSkill::Cast(CharacterInstance& caster, CharacterInstance& target)
{
	if (!CastConditions(caster, target))
		return false;

	target.RemoveAura(__FindPoisonedAura(target.Auras()));
	_nbUsage--;

	return true;
}

Skill* CurePoisonSkill::Copy()
{
	return new CurePoisonSkill(*this);
}

bool CurePoisonSkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (!caster.IsAlly(&target) && &caster == &target)
		throw NotAnAllyTargetException(target.InstantiatedCharacter().Name());
	if (_nbUsage <= 0)
		throw NoMoreUsageException(caster.InstantiatedCharacter().Name(), _name);
	if (!caster.Is<Ranger>())
		throw CannotCastException(caster.InstantiatedCharacter().Name(), _name);
	if(!__FindPoisonedAura(target.Auras()))
		throw TargetNotPoisonedException(caster.InstantiatedCharacter().Name());
	return true;
}

CharacterAura* CurePoisonSkill::__FindPoisonedAura(std::vector<CharacterAura*> auras) const
{
	auto aura = std::find_if(auras.begin(), auras.end(), [](CharacterAura* a) {
		if (!a)	return false;
		return CharacterAura::Is<PoisonedAura>(*a);
		});
	if (aura == auras.end())
		return nullptr;
	return *aura;
}
