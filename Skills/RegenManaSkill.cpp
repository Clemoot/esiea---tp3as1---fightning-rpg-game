/**
	\file RegenManaSkill.cpp
	\brief RegenManaSkill declaration
	\author Garnier Cl�ment
*/

#include "RegenManaSkill.h"
#include "../Characters/Mage.h"
#include "Targets/SelfTarget.h"
#include "../Utils/Exceptions.h"

RegenManaSkill::RegenManaSkill():
	Skill(REGEN_MANA_SKILL_NAME, REGEN_MANA_SKILL_COOLDOWN, REGEN_MANA_SKILL_TARGETS)
{}

RegenManaSkill::RegenManaSkill(const RegenManaSkill & other): Skill(other)
{}

RegenManaSkill::~RegenManaSkill()
{
}

bool RegenManaSkill::Cast(CharacterInstance& caster, CharacterInstance& target)
{
	if (!CastConditions(caster, target))
		return false;

	std::cout << caster.InstantiatedCharacter().Name() << " casts " << _name << std::endl;

	float amount = rand() / RAND_MAX * 5.f + 2.f;
	caster.Regenerate(amount);

	return true;
}

Skill* RegenManaSkill::Copy()
{
	return new RegenManaSkill(*this);
}

bool RegenManaSkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (&caster != &target)
		throw NotHimSelfTargetException();
	if (!caster.Is<Mage>())
		throw CannotCastException(caster.InstantiatedCharacter().Name(), _name);
	return true;
}