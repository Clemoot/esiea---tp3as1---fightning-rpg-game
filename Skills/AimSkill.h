/**
	\file AimSkill.h
	\brief AimSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef AIM_SKILL_INCLUDED
/// AimSkill.h included
#define AIM_SKILL_INCLUDED


#include "Skill.h"

/// AimSkill default name
constexpr auto AIM_SKILL_NAME = "Aim";
/// AimSkill default cooldown
#define AIM_SKILL_COOLDOWN 0
/// AimSkill default target tags
#define AIM_SKILL_TARGETS { new SelfTarget() }

/**
	\class AimSkill
	\brief The caster starts aiming
*/
class JEU_DE_COMBAT_API AimSkill: public Skill {
public:
	/// AimSkill default constructor
	AimSkill();
	/**
		\brief AimSkill copy constructor
		\param other AimSkill to copy
	*/
	AimSkill(const AimSkill& other);
	/// AimSkill destructor
	~AimSkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);
};

#endif