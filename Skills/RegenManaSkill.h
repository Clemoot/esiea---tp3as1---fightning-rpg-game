/**
	\file RegenManaSkill.h
	\brief RegenManaSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef REGEN_MANA_SKILL_INCLUDED
/// VanishSkill.h included
#define REGEN_MANA_SKILL_INCLUDED

#include "Skill.h"

/// VanishSkill default name
#define REGEN_MANA_SKILL_NAME "Regenerate Mana"
/// VanishSkill default cooldown
#define REGEN_MANA_SKILL_COOLDOWN 0
/// VanishSkill default target tags
#define REGEN_MANA_SKILL_TARGETS { new SelfTarget() }

/**
	\class RegenManaSkill
	\brief The caster starts aiming
*/
class JEU_DE_COMBAT_API RegenManaSkill: public Skill {
public:
	/// RegenManaSkill default constructor
	RegenManaSkill();
	/**
		\brief RegenManaSkill copy constructor
		\param other RegenManaSkill to copy
	*/
	RegenManaSkill(const RegenManaSkill& other);
	/// RegenManaSkill destructor
	~RegenManaSkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);
};

#endif