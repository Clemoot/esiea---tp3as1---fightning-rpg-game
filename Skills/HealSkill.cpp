/**
	\file HealSkill.cpp
	\brief HealSkill declaration
	\author Garnier Cl�ment
*/

#include "HealSkill.h"
#include "../Characters/Mage.h"
#include "Targets/AllyTarget.h"
#include "Targets/SelfTarget.h"
#include "../Weapons/Staff.h"
#include "../Utils/Exceptions.h"

HealSkill::HealSkill():
	Skill(HEAL_SKILL_NAME, HEAL_SKILL_COOLDOWN, HEAL_SKILL_TARGETS)
{}

HealSkill::HealSkill(const HealSkill & other): Skill(other)
{}

HealSkill::~HealSkill()
{
}

bool HealSkill::Cast(CharacterInstance& caster, CharacterInstance& target)
{
	if (!CastConditions(caster, target))
		return false;

	std::cout << caster.InstantiatedCharacter().Name() << " casts " << _name << std::endl;

	caster.Consume(HEAL_RESOURCE_COST);

	float heal = rand() / RAND_MAX * 10.f + 10.f;
	target.Heal(heal);

	return true;
}

Skill* HealSkill::Copy()
{
	return new HealSkill(*this);
}

bool HealSkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (target.Dead())
		throw DeadTargetException(caster.InstantiatedCharacter().Name());
	if (&caster != &target && !caster.IsAlly(&target))
		throw NotAnAllyTargetException(target.InstantiatedCharacter().Name());
	if (!caster.Is<Mage>())
		throw CannotCastException(caster.InstantiatedCharacter().Name(), _name);
	Weapon* weapon = caster.EquipedWeapon();
	if (!weapon)
		throw CasterNoWeaponEquipedException(caster.InstantiatedCharacter().Name());
	if (!Weapon::Is<Staff>(*weapon))
		throw NotAStaffException(weapon->Name());
	if (caster.Resources().Mana() < HEAL_RESOURCE_COST)
		throw NotEnoughResourcesException(caster.InstantiatedCharacter().Name(), _name);
	return true;
}