/**
	\file Skill.h
	\brief Skill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef SKILL_INCLUDED
#define SKILL_INCLUDED

#include <iostream>
#include <vector>

#include "../Instances/CharacterInstance.h"

class Target;	// Forward declaration to avoid circular dependencies

/**
	\class Skill
	\brief Basic Skill declaration 
*/
class Skill
{
public:
	/**
		\brief Skill constructor
		\param name Name of the skill
		\param cooldown Cooldown of the skill
		\param targets Target tags of the skill
	*/
	Skill(std::string name = "", uint8_t cooldown = 0, std::vector<Target*> targets = std::vector<Target*>());
	/**
		\brief Skill copy constructor
		\param other Skill to copy
	*/
	Skill(const Skill& other);
	/// Skill destructor
	virtual ~Skill();

	std::string Name() const;	///< Name getter
	uint8_t Cooldown() const;	///< Cooldown getter

	/// Reduces the cooldown of the aura (generally called at the beginning of the turn)
	void ReduceCooldown();

	/**
		\brief Checks if the skill is castable by an instance on a target (without throwing errors)
		\return true if castable, false otherwise
	*/
	bool Castable(CharacterInstance& caster, CharacterInstance& target);
	/**
		\brief Casts the skill on a target (throws errors)
		\return true if casted, false otherwise
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target) = 0;
	
	/// Retrieve targets of the skill from an instance
	std::vector<CharacterInstance*> FindTargetsFrom(CharacterInstance& caster);

	/// Check if skill has a specific target tag
	template<class T>
	bool Targets() {
		auto pos = std::find_if(_targets.begin(), _targets.end(), [](Target* t) {
			return Target::Is<T>(*t);
		});
		if (pos != _targets.end())
			return true;
		return false;
	}
	/// Checks if the skill has the OffensiveTarget tag
	bool IsOffensive();

	/// Copy the skill
	virtual Skill* Copy() = 0;

	/// Checks if the skill is castable in a child skill
	template<class T>
	static bool Is(Skill& s) {
		try {
			dynamic_cast<T&>(s);
			return true;
		}
		catch (const std::bad_cast&) {
			return false;
		}
	}

protected:
	std::string _name;		///< Name of the skill
	uint8_t _cooldown;		///< Cooldown of the skill
	uint8_t _curCooldown;	///< Current cooldown of the skill
	std::vector<Target*> _targets;	///< Target tags

	/// Check if the skill is castable (throws errors)
	virtual bool CastConditions(CharacterInstance& caster, CharacterInstance& target) = 0;
};

#endif
