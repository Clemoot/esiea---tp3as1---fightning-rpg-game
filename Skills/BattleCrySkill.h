/**
	\file BattleCrySkill.h
	\brief BattleCrySkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef BATTLECRY_SKILL_INCLUDED
/// VanishSkill.h included
#define BATTLECRY_SKILL_INCLUDED

#include "Skill.h"

/// VanishSkill default name
#define BATTLE_CRY_SKILL_NAME "Battle Cry"
/// VanishSkill default cooldown
#define BATTLE_CRY_SKILL_COOLDOWN 4
/// VanishSkill default target tags
#define BATTLE_CRY_SKILL_TARGETS { new SelfTarget() }

/**
	\class BattleCrySkill
	\brief The caster taunts all the opponents
*/
class JEU_DE_COMBAT_API BattleCrySkill: public Skill {
public:
	/// BattleCrySkill default constructor
	BattleCrySkill();
	/**
		\brief BattleCrySkill copy constructor
		\param other BattleCrySkill to copy
	*/
	BattleCrySkill(const BattleCrySkill& other);
	/// BattleCrySkill destructor
	~BattleCrySkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);
};

#endif