/**
	\file HealSkill.h
	\brief HealSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef HEAL_SKILL_INCLUDED
/// VanishSkill.h included
#define HEAL_SKILL_INCLUDED

#include "Skill.h"

/// VanishSkill default name
#define HEAL_SKILL_NAME "Heal"
/// VanishSkill default cooldown
#define HEAL_SKILL_COOLDOWN 4
/// VanishSkill default target tags
#define HEAL_SKILL_TARGETS { new AllyTarget(), new SelfTarget() }
/// VanishSkill default resource cost
#define HEAL_RESOURCE_COST 5.f

/**
	\class HealSkill
	\brief The caster starts aiming
*/
class JEU_DE_COMBAT_API HealSkill: public Skill {
public:
	/// HealSkill default constructor
	HealSkill();
	/**
		\brief HealSkill copy constructor
		\param other HealSkill to copy
	*/
	HealSkill(const HealSkill& other);
	/// HealSkill destructor
	~HealSkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);
};

#endif