/**
	\file AimSkill.cpp
	\brief AimSkill declaration
	\author Garnier Cl�ment
*/

#include "AimSkill.h"
#include "../Auras/AimAura.h"
#include "../Characters/Ranger.h"
#include "../Weapons/Bow.h"
#include "Targets/SelfTarget.h"
#include "../Utils/Exceptions.h"

AimSkill::AimSkill(): Skill(AIM_SKILL_NAME, AIM_SKILL_COOLDOWN, AIM_SKILL_TARGETS)
{}

AimSkill::AimSkill(const AimSkill & other): Skill(other)
{}

AimSkill::~AimSkill()
{}

bool AimSkill::Cast(CharacterInstance& caster, CharacterInstance& target)
{
	if (!CastConditions(caster, target))
		return false;

	std::cout << caster.InstantiatedCharacter().Name() << " casts " << _name << std::endl;
	CharacterAura* newAura = AimAura::Instantiate(&caster);
	if (newAura)
		caster.AddAura(newAura);

	return true;
}

Skill* AimSkill::Copy()
{
	return new AimSkill(*this);
}

bool AimSkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (&caster != &target)
		throw NotHimSelfTargetException();
	if (!caster.Is<Ranger>())
		throw CannotCastException(caster.InstantiatedCharacter().Name(), _name);
	Weapon* weapon = caster.EquipedWeapon();
	if (!weapon)
		throw CasterNoWeaponEquipedException(caster.InstantiatedCharacter().Name());
	if(!Weapon::Is<Bow>(*weapon))
		throw NotABowException(weapon->Name());
	return true;
}
