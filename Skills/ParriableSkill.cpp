/**
	\file ParriableSkill.cpp
	\brief ParriableSkill declaration
	\author Garnier Cl�ment
*/

#include "ParriableSkill.h"

#include "../Characters/Warrior.h"
#include "../Weapons/Sword.h"

ParriableSkill::ParriableSkill(std::string name):
	Skill(name)
{}

ParriableSkill::~ParriableSkill()
{}

bool ParriableSkill::IsParried(CharacterInstance & target, float& roll)
{
	bool isParried = false;
	if (roll < target.Stats().Parry()) 
		if(target.EquipedWeapon() && target.Is<Warrior>() && Weapon::Is<Sword>(*target.EquipedWeapon()))
			isParried = true;
	roll -= target.Stats().Parry();
	return isParried;
}