/**
	\file RepairSkill.h
	\brief RepairSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef REPAIR_SKILL_INCLUDED
/// VanishSkill.h included
#define REPAIR_SKILL_INCLUDED

#include "Skill.h"

/// VanishSkill default name
#define REPAIR_SKILL_NAME "Repair"
/// VanishSkill default cooldown
#define REPAIR_SKILL_COOLDOWN 0
/// VanishSkill default target tags
#define REPAIR_SKILL_TARGETS { new AllyTarget(), new SelfTarget() }
/// Amount repaired if weapon is borken
#define REPAIR_IF_BROKEN 1

/**
	\class RepairSkill
	\brief The caster starts aiming
*/
class JEU_DE_COMBAT_API RepairSkill : public Skill {
public:
	/// RepairSkill default constructor
	RepairSkill();
	/**
		\brief RepairSkill copy constructor
		\param other RepairSkill to copy
	*/
	RepairSkill(const RepairSkill& other);
	/// RepairSkill destructor
	~RepairSkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);
};

#endif