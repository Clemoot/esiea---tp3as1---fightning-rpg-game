#include "Skill.h"
#include <algorithm>
#include "Targets/OpponentTarget.h"
#include "../Utils/Exceptions.h"
#include "../Utils/PointerDeleter.h"

Skill::Skill(std::string name, uint8_t cooldown, std::vector<Target*> targets): 
	_name(name),
	_cooldown(cooldown),
	_targets(targets),
	_curCooldown(0)
{}

Skill::Skill(const Skill & other): _name(other._name), _cooldown(other._cooldown), _curCooldown(0)
{
	std::for_each(other._targets.begin(), other._targets.end(), [&](Target* t) {
		if (!t)	return;
		_targets.push_back(t->Copy());
	});
}

Skill::~Skill()
{
	std::for_each(_targets.begin(), _targets.end(), PointerDeleter<Target>([&](Target*& t) {  return true;  }));
}

std::string Skill::Name() const
{
	return _name;
}

uint8_t Skill::Cooldown() const
{
	return _cooldown;
}

void Skill::ReduceCooldown()
{
	if(_curCooldown)
		_curCooldown--;
}

bool Skill::Castable(CharacterInstance& caster, CharacterInstance& target)
{
	try {
		if (CastConditions(caster, target))
			return true;
	}
	catch (const Exception&) {
		return false;
	}
	return false;
}

std::vector<CharacterInstance*> Skill::FindTargetsFrom(CharacterInstance& caster)
{

	std::vector<CharacterInstance*> targets;
	std::vector<CharacterInstance*> tmpTargets;
	for (size_t i = 0; i < _targets.size(); i++) {
		if (!_targets[i])
			continue;
		tmpTargets = _targets[i]->Targets(caster);
		std::for_each(tmpTargets.begin(), tmpTargets.end(), [&](CharacterInstance* target) {
			if (Castable(caster, *target))
				targets.push_back(target);
		});
	}
	return targets;
}

bool Skill::IsOffensive()
{
	return Targets<OpponentTarget>();
}