/**
	\file EnchantSkill.h
	\brief EnchantSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef ENCHANT_SKILL_INCLUDED
/// VanishSkill.h included
#define ENCHANT_SKILL_INCLUDED

#include "Skill.h"

/// VanishSkill default name
#define ENCHANT_SKILL_NAME "Enchant Weapon"
/// VanishSkill default cooldown
#define ENCHANT_SKILL_COOLDOWN 3
/// VanishSkill default resource cost
#define ENCHANT_SKILL_RESOURCE_COST 7
/// VanishSkill default target tags
#define ENCHANT_SKILL_TARGETS { new AllyTarget() }

/**
	\class EnchantSkill
	\brief The caster starts aiming
*/
class JEU_DE_COMBAT_API EnchantSkill: public Skill {
	/// EnchantSkill default constructor
public:
	EnchantSkill();
	/**
		\brief EnchantSkill copy constructor
		\param other EnchantSkill to copy
	*/
	EnchantSkill(const EnchantSkill& other);
	/// EnchantSkill destructor
	~EnchantSkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);
};

#endif