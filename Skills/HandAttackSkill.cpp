/**
	\file HandAttackSkill.cpp
	\brief HandAttackSkill declaration
	\author Garnier Cl�ment
*/

#include "HandAttackSkill.h"
#include "Targets/OpponentTarget.h"
#include "../Utils/Exceptions.h"

HandAttackSkill::HandAttackSkill():
	Skill(HAND_ATTACK_SKILL_NAME, HAND_ATTACK_SKILL_COOLDOWN, HAND_ATTACK_SKILL_TARGETS),
	DodgeableSkill(HAND_ATTACK_SKILL_NAME),
	ParriableSkill(HAND_ATTACK_SKILL_NAME)
{}

HandAttackSkill::HandAttackSkill(const HandAttackSkill & other): 
	Skill(other),
	DodgeableSkill(other),
	ParriableSkill(other)
{}

HandAttackSkill::~HandAttackSkill()
{}

bool HandAttackSkill::Cast(CharacterInstance & caster, CharacterInstance & target)
{
	if (!CastConditions(caster, target))
		return false;

	std::cout << caster.InstantiatedCharacter().Name() << " casts " << _name << std::endl;

	try {
		float dmg = caster.InstantiatedCharacter().HandDamageFunction()(caster.Stats(), target.Stats());
		target.Hit(dmg);

		std::cout << target.InstantiatedCharacter().Name() << " loses " << dmg << " HP !" << std::endl;
	} catch (const Exception & e) {
		std::cerr << "Error: " << e << std::endl;
	}

	return true;
}

Skill* HandAttackSkill::Copy()
{
	return new HandAttackSkill(*this);
}

bool HandAttackSkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (target.Dead())
		throw DeadTargetException(target.InstantiatedCharacter().Name());
	if (!caster.IsOpponent(&target))
		throw NotAnOpponentTargetException(target.InstantiatedCharacter().Name());
	return true;
}