/**
	\file Skills.h
	\brief Includes all Skill-related headers
	\author Garnier Cl�ment
*/

#pragma once

#ifndef SKILLS_INCLUDED
#define SKILLS_INCLUDED

#include "AimSkill.h"
#include "BattleCrySkill.h"
#include "CurePoisonSkill.h"
#include "EnchantSkill.h"
#include "HealSkill.h"
#include "RegenManaSkill.h"
#include "RepairSkill.h"
#include "VanishSkill.h"

#endif