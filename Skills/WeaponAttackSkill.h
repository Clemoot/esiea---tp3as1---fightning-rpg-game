/**
	\file WeaponAttackSkill.h
	\brief WeaponAttackSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef WEAPON_ATTACK_SKILL_INCLUDED
/// WeaponAttackSkill.h included
#define WEAPON_ATTACK_SKILL_INCLUDED

#include "Skill.h"
#include "DodgeableSkill.h"
#include "ParriableSkill.h"

/// WeaponAttackSkill default name
#define WEAPON_ATTACK_SKILL_NAME "Attack with Weapon"
/// WeaponAttackSkill default cooldown
#define WEAPON_ATTACK_SKILL_COOLDOWN 0
/// WeaponAttackSkill default target tags
#define WEAPON_ATTACK_SKILL_TARGETS { new OpponentTarget() }

/**
	\class WeaponAttackSkill
	\brief The caster starts aiming
*/
class JEU_DE_COMBAT_API WeaponAttackSkill: public DodgeableSkill, public ParriableSkill {
public:
	/// WeaponAttackSkill default constructor
	WeaponAttackSkill();
	/**
		\brief WeaponAttackSkill copy constructor
		\param other WeaponAttackSkill to copy
	*/
	WeaponAttackSkill(const WeaponAttackSkill& other);
	/// WeaponAttackSkill destructor
	~WeaponAttackSkill();

	/**
		\brief Casts the skill
		\param caster Instance casting the skill
		\param target Target of the skill
	*/
	virtual bool Cast(CharacterInstance& caster, CharacterInstance& target);
	/// Copy the skill
	Skill* Copy();

protected:
	/// Checks if the skill is castable by an instance
	bool CastConditions(CharacterInstance& caster, CharacterInstance& target);
};

#endif