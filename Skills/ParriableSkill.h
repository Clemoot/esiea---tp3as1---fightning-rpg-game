/**
	\file ParriableSkill.h
	\brief ParriableSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "Skill.h"

/**
	\class ParriableSkill
	\brief Skill tag

	If a skill is inherited from ParriableSkill, it can be parried
*/
class JEU_DE_COMBAT_API ParriableSkill: virtual public Skill {
public:
	/**
		\brief ParriableSkill constructor
		\param name Name of the skill
	*/
	ParriableSkill(std::string name = "");
	/// ParriableSkill destructor
	~ParriableSkill();

	/**
		\brief Parry test
		\param target target of the skill
		\param roll Random value (0 < roll < 1)
		\return true is the skill is parried, false otherwise
	*/
	bool IsParried(CharacterInstance& target, float& roll);
};