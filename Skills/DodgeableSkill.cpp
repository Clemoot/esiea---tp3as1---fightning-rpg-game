/**
	\file DodgeableSkill.cpp
	\brief DodgeableSkill declaration
	\author Garnier Cl�ment
*/

#include "DodgeableSkill.h"

DodgeableSkill::DodgeableSkill(std::string name):
	Skill(name)
{}

DodgeableSkill::~DodgeableSkill() {}

bool DodgeableSkill::IsDodged(CharacterInstance& target, float& roll)
{
	bool isDodged = roll < target.Stats().Dodge();
	roll -= target.Stats().Dodge();
	return isDodged;
}
