/**
	\file EnchantSkill.cpp
	\brief EnchantSkill declaration
	\author Garnier Cl�ment
*/

#include "EnchantSkill.h"
#include "../Characters/Mage.h"
#include "../Auras/EnchantedAura.h"
#include "Targets/AllyTarget.h"
#include "../Utils/Exceptions.h"

EnchantSkill::EnchantSkill():
	Skill(ENCHANT_SKILL_NAME, ENCHANT_SKILL_COOLDOWN, ENCHANT_SKILL_TARGETS)
{}

EnchantSkill::EnchantSkill(const EnchantSkill & other): Skill(other)
{}

EnchantSkill::~EnchantSkill()
{
}

bool EnchantSkill::Cast(CharacterInstance& caster, CharacterInstance& target)
{
	if (!CastConditions(caster, target))
		return false;

	std::cout << caster.InstantiatedCharacter().Name() << " casts " << _name << std::endl;

	WeaponAura* newAura = EnchantedAura::Instantiate(caster.EquipedWeapon());
	if (newAura) {
		target.EquipedWeapon()->AddAura(newAura);
		caster.Consume(ENCHANT_SKILL_RESOURCE_COST);
	}

	return true;
}

Skill* EnchantSkill::Copy()
{
	return new EnchantSkill(*this);
}

bool EnchantSkill::CastConditions(CharacterInstance& caster, CharacterInstance& target)
{
	if (_curCooldown > 0)
		throw StillInCooldownException(_name);
	if (caster.Dead())
		throw DeadCasterException(caster.InstantiatedCharacter().Name());
	if (target.Dead())
		throw DeadTargetException(target.InstantiatedCharacter().Name());
	if (!caster.IsAlly(&target))
		throw NotAnAllyTargetException(target.InstantiatedCharacter().Name());
	if (!caster.Is<Mage>())
		throw CannotCastException(caster.InstantiatedCharacter().Name(), _name);
	if (!target.EquipedWeapon())
		throw TargetNoWeaponEquipedException(target.InstantiatedCharacter().Name());
	if (caster.Resources().Mana() < ENCHANT_SKILL_RESOURCE_COST)
		throw NotEnoughResourcesException(caster.InstantiatedCharacter().Name(), _name);
	return true;
}