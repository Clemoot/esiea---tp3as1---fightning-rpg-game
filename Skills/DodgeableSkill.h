/**
	\file DodgeableSkill.h
	\brief DodgeableSkill declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "Skill.h"

/**
	\class DodgeableSkill
	\brief Skill tag

	If a skill is inherited from DodgeableSkill, it can be dodged
*/
class JEU_DE_COMBAT_API DodgeableSkill: virtual public Skill {
public:
	/**
		\brief DodgeableSkill constructor
		\param name Name of the skill
	*/
	DodgeableSkill(std::string name);
	/// DodgeableSkill destructor
	~DodgeableSkill();

	/**
		\brief Dodge test
		\param target target of the skill
		\param roll Random value (0 < roll < 1)
		\return true is the skill is dodged, false otherwise
	*/
	bool IsDodged(CharacterInstance& target, float& roll);
};