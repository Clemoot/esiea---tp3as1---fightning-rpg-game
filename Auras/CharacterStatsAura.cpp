/**
	\file CharacterStatsAura.cpp
	\brief CharacterStatsAura definition
	\author Garnier Cl�ment
*/

#include "CharacterStatsAura.h"

CharacterStatsAura::CharacterStatsAura(std::string name, uint8_t duration):
	Aura(name, duration),
	CharacterAura(name, duration),
	_flatStats(BaseStats::NULL_VALUE),
	_multiplier(BaseStats::IDENTITY)
{}

CharacterStatsAura::CharacterStatsAura(BaseStats multiplier, std::string name, uint8_t duration) :
	Aura(name, duration),
	CharacterAura(name, duration),
	_flatStats(BaseStats::NULL_VALUE),
	_multiplier(multiplier)
{}

CharacterStatsAura::CharacterStatsAura(float globalMultiplier, std::string name, uint8_t duration):
	Aura(name, duration),
	CharacterAura(name, duration),
	_flatStats(BaseStats::NULL_VALUE),
	_multiplier(globalMultiplier)
{}

CharacterStatsAura::CharacterStatsAura(BaseStats multiplier, BaseStats flat, std::string name, uint8_t duration):
	Aura(name, duration),
	CharacterAura(name, duration),
	_flatStats(flat),
	_multiplier(multiplier)
{}

CharacterStatsAura::CharacterStatsAura(const CharacterStatsAura& other):
	Aura(other),
	CharacterAura(other),
	_flatStats(other._flatStats),
	_multiplier(other._multiplier)
{}


CharacterStatsAura::~CharacterStatsAura()
{}

BaseStats CharacterStatsAura::FlatStats() const
{
	return _flatStats;
}

BaseStats CharacterStatsAura::Multiplier() const
{
	return _multiplier;
}

CharacterAura* CharacterStatsAura::Instantiate(CharacterInstance* owner)
{
	CharacterStatsAura* a = new CharacterStatsAura();
	if (!a->__Instantiate(owner))
		return nullptr;
	return a;
}

CharacterAura* CharacterStatsAura::Copy()
{
	return new CharacterStatsAura(*this);
}