/**
	\file CharacterStatsAura.h
	\brief CharacterStatsAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "CharacterAura.h"
#include "../Stats/BaseStats.h"

/// CharactersStatsAura default name
#define CHARACTER_STATS_AURA_NAME "Stats Modification"
/// CharactersStatsAura default duration
#define CHARACTER_STATS_AURA_DURATION INFINITE_DURATION

/**
	\class CharacterStatsAura
	\brief Aura which affects the owner stats
*/
class JEU_DE_COMBAT_API CharacterStatsAura : virtual public CharacterAura {
public:
	/**
		\brief CharacterStatsAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	CharacterStatsAura(std::string name = CHARACTER_STATS_AURA_NAME, uint8_t duration = CHARACTER_STATS_AURA_DURATION);
	/**
		\brief CharacterStatsAura constructor
		\param multiplier Stats multiplier
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	CharacterStatsAura(BaseStats multiplier, std::string name = CHARACTER_STATS_AURA_NAME, uint8_t duration = CHARACTER_STATS_AURA_DURATION);
	/**
		\brief CharacterStatsAura constructor
		\param globalMultiplier Scalar multipling all stats 
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	CharacterStatsAura(float globalMultiplier, std::string name = CHARACTER_STATS_AURA_NAME, uint8_t duration = CHARACTER_STATS_AURA_DURATION);
	/**
		\brief CharacterStatsAura constructor
		\param multiplier Stats multiplier
		\param flat Stats to add/subtract
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	CharacterStatsAura(BaseStats multiplier, BaseStats flat, std::string name = CHARACTER_STATS_AURA_NAME, uint8_t duration = CHARACTER_STATS_AURA_DURATION);
	/**
		\brief CharacterStatsAura copy constructor
		\param other CharacterStatsAura to copy
	*/
	CharacterStatsAura(const CharacterStatsAura& other);
	/// CharacterStatsAura destructor
	~CharacterStatsAura();

	/// Flat stats getter
	BaseStats FlatStats() const;
	/// Stats multiplier getter
	BaseStats Multiplier() const;

	/// Instantiates a new CharacterStatsAura on a character
	static CharacterAura* Instantiate(CharacterInstance* owner);

	/// Copy the aura
	CharacterAura* Copy();

private:
	BaseStats _flatStats;	///< Flat stats to add/substract
	BaseStats _multiplier;	///< Stats multipler
};