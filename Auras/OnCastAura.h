/**
	\file OnCastAura.h
	\brief OnCastAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "CharacterAura.h"

/**
	\class OnCastAura
	\brief Aura tag

	If an aura is inherited from OnCastAura, the aura is triggered each time the owner casts a spell
*/
class JEU_DE_COMBAT_API OnCastAura: virtual public CharacterAura {
public:
	/**
		\brief OnCastAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	OnCastAura(std::string name = "", uint8_t duration = 0);
	/**
		\brief OnCastAura copy constructor
		\param other OnCastAura to copy
	*/
	OnCastAura(const OnCastAura& other);
	/// OnCast destructor
	~OnCastAura();

	/// Triggers when skill is casted by the owner
	virtual void OnCast(CharacterInstance& target, Skill& skill) = 0;
};