/**
	\file AimAura.h
	\brief AimAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "CharacterStatsAura.h"
#include "FadeOnAttackAura.h"

/// CharactersStatsAura default name
#define AIM_AURA_NAME "Aim"

/**
	\class AimAura
	\brief The owner aims and receives a dexterity buff
*/
class JEU_DE_COMBAT_API AimAura: public CharacterStatsAura, public FadeOnAttackAura {
public:
	/**
		\brief AimAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	AimAura(std::string name = AIM_AURA_NAME, uint8_t duration = INFINITE_DURATION);
	/// AimAura destructor
	~AimAura();

	/// Instantiates a new AimAura on a character
	static CharacterAura* Instantiate(CharacterInstance* owner);

	/// Copy the aura
	CharacterAura* Copy();

protected:
	/// Checks if the owner can aim
	bool __OnInstantiate();
};