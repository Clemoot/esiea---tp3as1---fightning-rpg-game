/**
	\file OnTurnBeginAura.h
	\brief OnTurnBeginAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "CharacterAura.h"

/**
	\class OnTurnBeginAura
	\brief Aura tag

	If an aura is inherited from OnTurnBeginAura, the aura is triggered at the beginning of the owner turn
*/
class JEU_DE_COMBAT_API OnTurnBeginAura: virtual public CharacterAura {
public:
	/**
		\brief OnTurnBeginAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	OnTurnBeginAura(std::string name = "", uint8_t duration = 0);
	/**
		\brief OnTurnBeginAura copy constructor
		\param other OnTurnBeginAura to copy
	*/
	OnTurnBeginAura(const OnTurnBeginAura& other);
	/// OnTurnBeginAura destructor
	~OnTurnBeginAura();

	/// Triggers the aura
	virtual void Proc() = 0;
};