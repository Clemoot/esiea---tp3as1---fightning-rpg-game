/**
	\file PoisonOnHitAura.h
	\brief PoisonOnHitAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "OnCastAura.h"

/// PoisonOnHitAura default name
#define POISON_ON_HIT_AURA_NAME "Poison on Hit"
/// PoisonOnHitAura default duration
#define POISON_ON_HIT_AURA_DURATION INFINITE_DURATION

/**
	\class PoisonOnHitAura
	\brief Aura which allows a character to poison an ennemy
*/
class JEU_DE_COMBAT_API PoisonOnHitAura : public OnCastAura {
public:
	/**
		\brief PoisonOnHit constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	PoisonOnHitAura(std::string name = POISON_ON_HIT_AURA_NAME, uint8_t duration = POISON_ON_HIT_AURA_DURATION);
	/**
		\brief PoisonOnHit copy constructor
		\param other PoisonOnHit to copy
	*/
	PoisonOnHitAura(const PoisonOnHitAura& other);
	/// PoisonOnHit destructor
	~PoisonOnHitAura();

	/// Trigger on skill casted
	void OnCast(CharacterInstance& target, Skill& skill);

	/// Instantiates a new PoisonOnHitAura on a character
	static CharacterAura* Instantiate(CharacterInstance* owner);

	/// Copy tha aura
	CharacterAura* Copy();
};