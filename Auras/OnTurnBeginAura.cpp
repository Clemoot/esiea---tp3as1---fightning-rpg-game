/**
	\file OnTurnBeginAura.cpp
	\brief OnTurnBeginAura definition
	\author Garnier Cl�ment
*/

#include "OnTurnBeginAura.h"

OnTurnBeginAura::OnTurnBeginAura(std::string name, uint8_t duration): CharacterAura(name, duration)
{}

OnTurnBeginAura::OnTurnBeginAura(const OnTurnBeginAura & other): CharacterAura(other)
{}

OnTurnBeginAura::~OnTurnBeginAura()
{}
