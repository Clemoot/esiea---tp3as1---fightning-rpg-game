/**
	\file CharacterAura.cpp
	\brief CharacterAura definition
	\author Garnier Cl�ment
*/

#include "CharacterAura.h"

CharacterAura::CharacterAura(std::string name, uint8_t duration): Aura(name, duration), _owner(nullptr)
{
}

CharacterAura::CharacterAura(const CharacterAura & other): Aura(other), _owner(other._owner)
{}

CharacterAura::~CharacterAura()
{
}
bool CharacterAura::Instantiated() const
{
	return _owner != nullptr;
}

bool CharacterAura::__Instantiate(CharacterInstance* owner)
{
	_owner = owner;
	if (!__OnInstantiate()) {
		_owner = nullptr;
		return false;
	}
	return true;
}

CharacterAura* CharacterAura::CopyOn(CharacterInstance* owner)
{
	CharacterAura* a = dynamic_cast<CharacterAura*>(this->Copy());
	a->__Instantiate(owner);
	return a;
}
