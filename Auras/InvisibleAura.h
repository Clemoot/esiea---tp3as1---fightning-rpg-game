/**
	\file InvisibleAura.h
	\brief InvisibleAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "TargetAlterationAura.h"

/// InvisibleAura default name
#define INVISIBLE_AURA_NAME "Invisible"
/// InvisibleAura default duration
#define INVISIBLE_AURA_DURATION 1 + (rand() % 2) 

/**
	\class InvisibleAura
	\brief The owner is cannot be targetted by an ennemy
*/
class JEU_DE_COMBAT_API	InvisibleAura: public TargetAlterationAura {
public:
	/**
		\brief InvisibleAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	InvisibleAura(std::string name = INVISIBLE_AURA_NAME, uint8_t duration = INVISIBLE_AURA_DURATION);
	/**
		\brief InvisibleAura copy cosntructor
		\param other InvisibleAura to copy
	*/
	InvisibleAura(const InvisibleAura& other);
	/// InvisibleAura destructor
	~InvisibleAura();

	/// Removes the owner from the target list if the caster is an ennemy
	void Alterate(CharacterInstance* caster, std::vector<CharacterInstance*>& targets);

	/// Instantiates a new InvisibleAura on a character
	static CharacterAura* Instantiate(CharacterInstance* owner);

	/// Copy the aura
	CharacterAura* Copy();
};