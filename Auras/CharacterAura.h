/**
	\file CharacterAura.h
	\brief CharacterAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "../Utils/LibExport.h"

#include "Aura.h"
#include "../Instances/CharacterInstance.h"


/**
	\class CharacterAura
	\brief Aura owned by a character
*/
class JEU_DE_COMBAT_API CharacterAura: virtual public Aura {
public:
	/**
		\brief CharacterAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	CharacterAura(std::string name, uint8_t duration = 0);
	/**
		\brief CharacterAura copy constructor
		\param other CharacterAura to copy
	*/
	CharacterAura(const CharacterAura& other);
	/// CharacterAura destructor
	~CharacterAura();

	/// Check if the aura has an owner
	bool Instantiated() const;

	/**
		\brief Copy a CharacterAura
		This copy function creates a dynamically allocated aura
	*/
	virtual CharacterAura* Copy() = 0;
	/**
		\brief Copy a CharacterAura on another owner
		This copy function creates a dynamically allocated aura
	*/
	CharacterAura* CopyOn(CharacterInstance* owner);

protected:
	CharacterInstance* _owner;	///< Character owning the aura

	/// Add aura on a character and checks conditions
	bool __Instantiate(CharacterInstance* owner);
};