/**
	\file FadeOnAttackAura.h
	\brief FadeOnAttackAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "Aura.h"

/**
	\class FadeOnAttackAura
	\brief Aura tag
	
	If an aura is inherited from FadeOnAttackAura, the aura will stay indefinitely as long as the character do not attack
*/
class JEU_DE_COMBAT_API FadeOnAttackAura: virtual public Aura {
public:
	/**
		\brief FadeOnAttackAura constructor
		\param name Name of the aura
	*/
	FadeOnAttackAura(std::string name = "");
	/// FadeOnAttackAura destructor
	~FadeOnAttackAura();

private:
};