/**
	\file OnCastAura.cpp
	\brief OnCastAura definition
	\author Garnier Cl�ment
*/

#include "OnCastAura.h"

OnCastAura::OnCastAura(std::string name, uint8_t duration): CharacterAura(name, duration)
{}

OnCastAura::OnCastAura(const OnCastAura & other): CharacterAura(other)
{}

OnCastAura::~OnCastAura()
{}
