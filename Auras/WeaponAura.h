/**
	\file WeaponAura.h
	\brief WeaponAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "Aura.h"
#include "../Weapons/Weapon.h"

/**
	\class WeaponAura
	\brief Aura owned by a weapon
*/
class JEU_DE_COMBAT_API WeaponAura: virtual public Aura {
public:
	/**
		\brief WeaponAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	WeaponAura(std::string name, uint8_t duration = 0);
	/**
		\brief WeaponAura copy constructor
		\param other WeaponAura to copy
	*/
	WeaponAura(const WeaponAura& other);
	/// WeaponAura desctrutor
	~WeaponAura();

	/// Checks if teh aura has an owner
	bool Instantiated() const;

	/**
		\brief Copy a WeaponAura
		This copy function creates a dynamically allocated aura
	*/
	virtual WeaponAura* Copy() = 0;
	/**
		\brief Copy a WeaponAura on another owner
		This copy function creates a dynamically allocated aura
	*/
	WeaponAura* CopyOn(Weapon* owner);

protected:
	Weapon* _owner;	///< Weapon owning the aura

	/// Add aura on a character and checks conditions
	bool __Instantiate(Weapon* owner);
};