/**
	\file WeaponStatsAura.h
	\brief WeaponStatsAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "WeaponAura.h"
#include "../Stats/WeaponStats.h"

/// WeaponStatsAura default name
#define WEAPON_STATS_AURA_NAME "Stats Modification"
/// WeaponStatsAura default duration
#define WEAPON_STATS_AURA_DURATION INFINITE_DURATION

/**
	\class WeaponStatsAura
	\brief Aura which affects weapon stats
*/
class JEU_DE_COMBAT_API WeaponStatsAura: virtual public WeaponAura {
public:
	/**
		\brief WeaponStatsAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	WeaponStatsAura(std::string name = WEAPON_STATS_AURA_NAME, uint8_t duration = WEAPON_STATS_AURA_DURATION);
	/**
		\brief WeaponStatsAura constructor
		\param multiplier Stats multiplier
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	WeaponStatsAura(WeaponStats multiplier, std::string name = WEAPON_STATS_AURA_NAME, uint8_t duration = WEAPON_STATS_AURA_DURATION);
	/**
		\brief WeaponStatsAura constructor
		\param globalMultiplier Scalar multipling all stats
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	WeaponStatsAura(float globalMultiplier, std::string name = WEAPON_STATS_AURA_NAME, uint8_t duration = WEAPON_STATS_AURA_DURATION);
	/**
		\brief WeaponStatsAura constructor
		\param multiplier Stats multiplier
		\param flat Stats to add/substract
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	WeaponStatsAura(WeaponStats multiplier, WeaponStats flat, std::string name = WEAPON_STATS_AURA_NAME, uint8_t duration = WEAPON_STATS_AURA_DURATION);
	/**
		\brief WeaponStatsAura copy constructor
		\param other WeaponStatsAura to copy
	*/
	WeaponStatsAura(const WeaponStatsAura& other);
	/// WeaponStatsAura destructor
	~WeaponStatsAura();

	/// Flat stats getter
	WeaponStats FlatStats() const;
	WeaponStats Multiplier() const;		///< Stats multiplier getter

	/// Instantiates a new WeaponStatsAura on a weapon
	static WeaponAura* Instantiate(Weapon* owner);

	/// Copy the aura
	WeaponAura* Copy();

private:
	WeaponStats _flatStats;		///< Stats to add/substract
	WeaponStats _multiplier;	///< Stats multiplier
};