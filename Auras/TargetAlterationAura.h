/**
	\file TargetAlterationAura.h
	\brief TargetAlterationAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "CharacterAura.h"

/**
	\class TargetAlterationAura
	\brief Aura tag

	If an aura is inherited from TargetAlterationAura, the aura can change targets of a skill
*/

class JEU_DE_COMBAT_API TargetAlterationAura: virtual public CharacterAura {
public:
	/**
		\brief TargetAlterationAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	TargetAlterationAura(std::string name, uint8_t duration = 0);
	/**
		\brief TargetAlterationAura copy constructor
		\param other TargetAlterationAura to copy
	*/
	TargetAlterationAura(const TargetAlterationAura& other);
	/// TargetAlterationAura destructor
	~TargetAlterationAura();

	/// Alterates the targets of a skill
	virtual void Alterate(CharacterInstance* caster, std::vector<CharacterInstance*>& targets) = 0;
};