/**
	\file TauntedAura.cpp
	\brief TauntedAura definition
	\author Garnier Cl�ment
*/

#include "TauntedAura.h"

TauntedAura::TauntedAura(CharacterInstance* target, std::string name, uint8_t duration) : 
	Aura(name, duration),
	CharacterAura(name, duration),
	TargetAlterationAura(name, duration),
	_target(target)
{}

TauntedAura::TauntedAura(const TauntedAura& other) : 
	Aura(other),
	CharacterAura(other),
	TargetAlterationAura(other), 
	_target(other._target)
{}

TauntedAura::~TauntedAura()
{}

void TauntedAura::Alterate(CharacterInstance* caster, std::vector<CharacterInstance*>& targets)
{
	if (caster != _owner && _target->Dead())
		return;

	auto opponents = _owner->Opponents();
	uint8_t i;
	for (i = 0; i < opponents.size(); i++) {
		auto pos = std::find(targets.begin(), targets.end(), opponents[i]);
		if (pos != targets.end() && *pos != _target)
			targets.erase(pos);
	}
}

CharacterAura* TauntedAura::Instantiate(CharacterInstance* owner, CharacterInstance* target)
{
	TauntedAura* a = new TauntedAura(target);
	if (!a->__Instantiate(owner))
		return nullptr;
	return a;
}

CharacterAura* TauntedAura::Copy()
{
	return new TauntedAura(*this);
}
