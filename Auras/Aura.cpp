/**
	\file Aura.cpp
	\brief Aura definition
	\author Garnier Cl�ment
*/

#include "Aura.h"

Aura::Aura(std::string name, uint8_t duration):
	_name(name),
	_duration(duration)
{}

Aura::Aura(const Aura& other):
	_name(other._name),
	_duration(other._duration)
{}

Aura::~Aura() {}

std::string Aura::Name() const
{
	return _name;
}

void Aura::ReduceDuration()
{
	if (_duration == INFINITE_DURATION)
		return;
	if (_duration > 0)
		_duration--;
}

bool Aura::TimedOut() const
{
	return _duration <= 0;
}

bool Aura::__OnInstantiate()
{
	return true;
}
