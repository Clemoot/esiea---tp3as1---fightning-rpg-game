/**
	\file TauntedAura.h
	\brief TauntedAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "TargetAlterationAura.h"

/// TauntedAura default name
#define TAUNTED_AURA_NAME "Taunted"
/// TauntedAura default duration
#define TAUNTED_AURA_DURATION  1 + (rand() % 2) 

/**
	\class TauntedAura
	\brief Taunts the owner and forces the owner to attack a specific ennemy
*/
class JEU_DE_COMBAT_API TauntedAura: public TargetAlterationAura {
public:
	/**
		\brief TauntedAura constructor
		\param target Taunter / Ennemy to attack
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	TauntedAura(CharacterInstance* target, std::string name = TAUNTED_AURA_NAME, uint8_t duration = TAUNTED_AURA_DURATION);
	/**
		\brief TauntedAura copy contructor
		\param other TauntedAura to copy
	*/
	TauntedAura(const TauntedAura& other);
	/// TauntedAura destructor
	~TauntedAura();

	/// Removes all ennemies except the taunter
	void Alterate(CharacterInstance* caster, std::vector<CharacterInstance*>& targets);

	/// Instantiate a new TauntedAura on a character
	static CharacterAura* Instantiate(CharacterInstance* owner, CharacterInstance* target);

	/// Copy a Taunted Aura
	CharacterAura* Copy();

private:
	CharacterInstance* _target;		///< Taunter / Ennemy to attack
};