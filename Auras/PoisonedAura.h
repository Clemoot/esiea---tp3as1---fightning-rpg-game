/**
	\file PoisonedAura.h
	\brief PoisonedAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "OnTurnBeginAura.h"

/// PoisonedAura default name
#define POISONED_AURA_NAME "Poisoned"
/// PoisonedAura default damage
#define POISONED_AURA_DAMAGE_PORTION 1/12.f
/// PoisonedAura default duration
#define POISONED_AURA_DURATION INFINITE_DURATION

/**
	\class PoisonedAura
	\brief The owner is poisoned and suffers on the beginning of its turn
*/
class JEU_DE_COMBAT_API	PoisonedAura: public OnTurnBeginAura {
public:
	/**
		\brief PoisonedAura
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	PoisonedAura(std::string name = POISONED_AURA_NAME, uint8_t duration = POISONED_AURA_DURATION);
	/**
		\brief PoisonedAura copy constructor
		\param other PoisonedAura to copy
	*/
	PoisonedAura(const PoisonedAura& other);
	/// PoisonedAura destructor
	~PoisonedAura();

	/// Trigger on turn begginning
	void Proc();

	/// Instantiates a new PoisonedAura on a character
	static CharacterAura* Instantiate(CharacterInstance* owner);

	/// Copy the aura
	CharacterAura* Copy();

protected:
	/// Check if the character can be poisoned
	bool __OnInstantiate();
};