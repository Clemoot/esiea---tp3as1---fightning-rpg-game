/**
	\file InvisibleAura.cpp
	\brief InvisibleAura definition
	\author Garnier Cl�ment
*/

#include "InvisibleAura.h"

InvisibleAura::InvisibleAura(std::string name, uint8_t duration): 
	Aura(name, duration),
	CharacterAura(name, duration),
	TargetAlterationAura(name, duration)
{
	std::cout << "Name aura: " << _name << std::endl;
}

InvisibleAura::InvisibleAura(const InvisibleAura& other): 
	Aura(other),
	CharacterAura(other),
	TargetAlterationAura(other)
{}

InvisibleAura::~InvisibleAura()
{
}

void InvisibleAura::Alterate(CharacterInstance* caster, std::vector<CharacterInstance*>& targets)
{
	if (!caster->IsOpponent(_owner))
		return;
	auto pos = std::find(targets.begin(), targets.end(), _owner);
	if (pos != targets.end())
		targets.erase(pos);
}

CharacterAura* InvisibleAura::Instantiate(CharacterInstance* owner)
{
	InvisibleAura* a = new InvisibleAura();
	if (!a->__Instantiate(owner))
		return nullptr;
	std::cout << owner->InstantiatedCharacter().Name() << " is now invisible" << std::endl;
	return a;
}

CharacterAura* InvisibleAura::Copy()
{
	return new InvisibleAura(*this);
}
