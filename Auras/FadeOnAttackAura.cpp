/**
	\file FadeOnAttackAura.cpp
	\brief FadeOnAttackAura definition
	\author Garnier Cl�ment
*/

#include "FadeOnAttackAura.h"

FadeOnAttackAura::FadeOnAttackAura(std::string name): Aura(name)
{}

FadeOnAttackAura::~FadeOnAttackAura()
{}