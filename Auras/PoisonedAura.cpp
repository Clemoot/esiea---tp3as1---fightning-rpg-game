/**
	\file PoisonedAura.cpp
	\brief PoisonedAura definition
	\author Garnier Cl�ment
*/

#include "PoisonedAura.h"
#include "../Characters/Ranger.h"

PoisonedAura::PoisonedAura(std::string name, uint8_t duration): 
	Aura(name, duration),
	CharacterAura(name, duration),
	OnTurnBeginAura(name, duration)
{}

PoisonedAura::PoisonedAura(const PoisonedAura & other): 
	Aura(other),
	CharacterAura(other),
	OnTurnBeginAura(other)
{}

PoisonedAura::~PoisonedAura()
{}

void PoisonedAura::Proc()
{
	std::cout << _owner->InstantiatedCharacter().Name() << " poisoned !" << std::endl;
	_owner->Hit(_owner->Stats().Life() * POISONED_AURA_DAMAGE_PORTION);
}

CharacterAura* PoisonedAura::Instantiate(CharacterInstance* owner)
{
	PoisonedAura* a = new PoisonedAura();
	if (!a->__Instantiate(owner))
		return nullptr;
	return a;
}

CharacterAura* PoisonedAura::Copy()
{
	return new PoisonedAura(*this);
}

bool PoisonedAura::__OnInstantiate()
{
	if (!_owner)	return false;
	if (_owner->Is<Ranger>()) {
		std::cout << _owner->InstantiatedCharacter().Name() << "is immune to poison" << std::endl;
		return false;
	}
	return true;
}
