/**
	\file TargetAlterationAura.cpp
	\brief TargetAlterationAura definition
	\author Garnier Cl�ment
*/

#include "TargetAlterationAura.h"

TargetAlterationAura::TargetAlterationAura(std::string name, uint8_t duration): CharacterAura(name, duration)
{}

TargetAlterationAura::TargetAlterationAura(const TargetAlterationAura & other): CharacterAura(other)
{}

TargetAlterationAura::~TargetAlterationAura()
{}