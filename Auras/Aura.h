/**
	\file Aura.h
	\brief Aura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "../Utils/LibExport.h"

#include <iostream>
/// ALIAS: Aura infinite duration
#define INFINITE_DURATION UINT8_MAX

/**
	\class Aura
	\brief Basic aura declaration
*/

class JEU_DE_COMBAT_API Aura{
public:
	/**
		\brief Aura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	Aura(std::string name, uint8_t duration = 0);
	/**
		\brief Aura copy constructor
		\param other Aura to copy
	*/
	Aura(const Aura& other);

	///Aura destructor
	virtual ~Aura();

	/**
		\brief Name getter
		\return Name of the aura
	*/
	std::string Name() const;

	/// Reduces the duration of the aura (generally called at the beginning of the turn)
	void ReduceDuration();
	/**
		\brief Checks if the aura can last
		\return false if aura is timed out, true otherwise
	*/
	bool TimedOut() const;

	/**
		\brief Checks if aura can be casted in a child aura
		\param aura Aura to cast
		\return true if castable, false otherwise
	*/
	template<class T>
	static bool Is(Aura& aura) {
		try {
			dynamic_cast<T&>(aura);
			return true;
		}
		catch (const std::bad_cast&) {
			return false;
		}
	}

protected:
	std::string _name;		///< Name of the aura
	uint8_t _duration;		///< Duration of the aura

	/// Called when aura has an owner
	virtual bool __OnInstantiate();
};