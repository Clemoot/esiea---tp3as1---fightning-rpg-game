/**
	\file EnchantedAura.h
	\brief EnchantedAura declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "WeaponStatsAura.h"
#include "FadeOnAttackAura.h"

/// EnchantedAura default name
#define ENCHANTED_AURA_NAME "Enchanted"

/**
	\class EnchantedAura
	\brief The weapon is enchanted granting a damage buff
*/
class JEU_DE_COMBAT_API EnchantedAura: public WeaponStatsAura, public FadeOnAttackAura {
public:
	/**
		\brief EnchantedAura constructor
		\param name Name of the aura
		\param duration Duration of the aura
	*/
	EnchantedAura(std::string name = ENCHANTED_AURA_NAME, uint8_t duration = INFINITE_DURATION);
	/// EnchantedAura destructor
	~EnchantedAura();

	/// Instantiates a new EnchantedAura on a weapon
	static WeaponAura* Instantiate(Weapon* owner);

	/// Copy the aura
	WeaponAura* Copy();

protected:
	/// Checks if the weapon is enchantable
	bool __OnInstantiate();
};