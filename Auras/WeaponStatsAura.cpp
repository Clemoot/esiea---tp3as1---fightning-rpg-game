/**
	\file WeaponStatsAura.cpp
	\brief WeaponStatsAura definition
	\author Garnier Cl�ment
*/

#include "WeaponStatsAura.h"

WeaponStatsAura::WeaponStatsAura(std::string name, uint8_t duration) :
	Aura(name, duration),
	WeaponAura(name, duration),
	_flatStats(BaseStats::NULL_VALUE),
	_multiplier(BaseStats::IDENTITY)
{}

WeaponStatsAura::WeaponStatsAura(WeaponStats multiplier, std::string name, uint8_t duration) :
	Aura(name, duration),
	WeaponAura(name, duration),
	_flatStats(BaseStats::NULL_VALUE),
	_multiplier(multiplier)
{}

WeaponStatsAura::WeaponStatsAura(float globalMultiplier, std::string name, uint8_t duration) :
	Aura(name, duration),
	WeaponAura(name, duration),
	_flatStats(BaseStats::NULL_VALUE),
	_multiplier(globalMultiplier)
{}

WeaponStatsAura::WeaponStatsAura(WeaponStats multiplier, WeaponStats flat, std::string name, uint8_t duration) :
	Aura(name, duration),
	WeaponAura(name, duration),
	_flatStats(flat),
	_multiplier(multiplier)
{}

WeaponStatsAura::WeaponStatsAura(const WeaponStatsAura& other) :
	Aura(other), 
	WeaponAura(other), 
	_flatStats(other._flatStats),
	_multiplier(other._multiplier)
{}

WeaponStatsAura::~WeaponStatsAura()
{}

WeaponStats WeaponStatsAura::FlatStats() const
{
	return _flatStats;
}

WeaponStats WeaponStatsAura::Multiplier() const
{
	return _multiplier;
}

WeaponAura* WeaponStatsAura::Instantiate(Weapon* owner)
{
	WeaponStatsAura* a = new WeaponStatsAura();
	if (!a->__Instantiate(owner))
		return nullptr;
	return a;
}

WeaponAura* WeaponStatsAura::Copy()
{
	return new WeaponStatsAura(*this);
}