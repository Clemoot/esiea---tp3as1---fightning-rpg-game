/**
	\file EnchantedAura.cpp
	\brief EnchantedAura definition
	\author Garnier Cl�ment
*/

#include "EnchantedAura.h"

EnchantedAura::EnchantedAura(std::string name, uint8_t duration):
	Aura(name, duration),
	WeaponAura(name, duration),
	WeaponStatsAura(WeaponStats(4.f / 3.f, 1.f, BaseStats::IDENTITY), name, duration),
	FadeOnAttackAura(name)
{}

EnchantedAura::~EnchantedAura()
{
}

WeaponAura* EnchantedAura::Instantiate(Weapon* owner)
{
	EnchantedAura* a = new EnchantedAura();
	if (!a->__Instantiate(owner))
		return nullptr;
	return a;
}

WeaponAura* EnchantedAura::Copy()
{
	return new EnchantedAura(*this);
}

bool EnchantedAura::__OnInstantiate()
{
	if (!_owner)	return false;
	std::cout << _owner->Name() << " is enchanted" << std::endl;
	return true;
}
