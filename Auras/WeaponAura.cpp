/**
	\file WeaponAura.cpp
	\brief WeaponAura definition
	\author Garnier Cl�ment
*/

#include "WeaponAura.h"

WeaponAura::WeaponAura(std::string name, uint8_t duration) : Aura(name, duration), _owner(nullptr)
{
}

WeaponAura::WeaponAura(const WeaponAura& other) : Aura(other), _owner(other._owner)
{}

WeaponAura::~WeaponAura()
{
}
bool WeaponAura::Instantiated() const
{
	return _owner != nullptr;
}

bool WeaponAura::__Instantiate(Weapon* owner)
{
	_owner = owner;
	if (!__OnInstantiate()) {
		_owner = nullptr;
		return false;
	}
	return true;
}

WeaponAura* WeaponAura::CopyOn(Weapon* owner)
{
	WeaponAura* a = dynamic_cast<WeaponAura*>(this->Copy());
	a->__Instantiate(owner);
	return a;
}
