/**
	\file AimAura.cpp
	\brief AimAura definition
	\author Garnier Cl�ment
*/

#include "AimAura.h"

AimAura::AimAura(std::string name, uint8_t duration) : 
	FadeOnAttackAura(name), 
	CharacterStatsAura(BaseStats(1.f, 1.f, 1.f, 4.f / 3.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f), BaseStats::NULL_VALUE, name),
	Aura(name, duration),
	CharacterAura(name, duration)
{}

AimAura::~AimAura()
{
}

CharacterAura* AimAura::Instantiate(CharacterInstance * owner)
{
	AimAura* a = new AimAura();
	if (!a->__Instantiate(owner))
		return nullptr;
	return a;
}

CharacterAura* AimAura::Copy()
{
	return new AimAura();
}

bool AimAura::__OnInstantiate()
{
	if (!_owner)	return false;
	std::cout << _owner->InstantiatedCharacter().Name() << " is aiming" << std::endl;;
	return true;
}