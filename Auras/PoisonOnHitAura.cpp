/**
	\file PoisonOnHitAura.cpp
	\brief PoisonOnHitAura definition
	\author Garnier Cl�ment
*/

#include "PoisonOnHitAura.h"
#include "PoisonedAura.h"
#include "../Characters/Rogue.h"
#include "../Weapons/Dagger.h"
#include "../Skills/WeaponAttackSkill.h"

PoisonOnHitAura::PoisonOnHitAura(std::string name, uint8_t duration): 
	OnCastAura(name, duration),
	CharacterAura(name, duration),
	Aura(name, duration)
{}

PoisonOnHitAura::PoisonOnHitAura(const PoisonOnHitAura & other): 
	OnCastAura(other),
	CharacterAura(other),
	Aura(other)
{}

PoisonOnHitAura::~PoisonOnHitAura()
{}

void PoisonOnHitAura::OnCast(CharacterInstance & target, Skill & skill)
{
	if (!_owner)
		return;
	Weapon* weapon = _owner->EquipedWeapon();
	if (!weapon)
		return;
	if (!Weapon::Is<Dagger>(*weapon))
		return;
	if (!_owner->Is<Rogue>())
		return;
	if (!Skill::Is<WeaponAttackSkill>(skill))
		return;
	if (!_owner->IsOpponent(&target))
		return;

	float value = rand() / (float)RAND_MAX;
	if (value < _owner->Stats().PoisonRate()) {
		CharacterAura* newAura = PoisonedAura::Instantiate(&target);
		if (newAura) {
			target.AddAura(newAura);
			std::cout << target.InstantiatedCharacter().Name() << " is poisoned" << std::endl;
		}
	}
}

CharacterAura* PoisonOnHitAura::Instantiate(CharacterInstance* owner)
{
	PoisonOnHitAura* a = new PoisonOnHitAura();
	if (!a->__Instantiate(owner))
		return nullptr;
	return a;
}

CharacterAura* PoisonOnHitAura::Copy()
{
	return new PoisonOnHitAura(*this);
}
