/**
	\file Warrior.cpp
	\brief Warrior definition
	\author Garnier Cl�ment
*/

#include "Warrior.h"
#include <algorithm>
#include "../Skills/BattleCrySkill.h"
#include "../Skills/RepairSkill.h"

#include "../Stats/StatsMask.h"

#include "../Weapons/Melee.h"

Warrior::Warrior() : Character("", WARRIOR_WEAPON_DAMAGE_FUNCTION, BaseStats(WARRIOR_MASKS), WARRIOR_BASE_SKILLS) {}

Warrior::Warrior(std::string name, float life, float atk, float def, float dex, float intelligence, float spd, float dodge, float parry, Weapon* weapon):
	Character(name, WARRIOR_WEAPON_DAMAGE_FUNCTION, BaseStats(life, atk, def, dex, intelligence, spd, dodge, parry, .0f, .0f, WARRIOR_MASKS), WARRIOR_BASE_SKILLS)
{
	Equip(weapon);
}

Warrior::Warrior(std::string name, const BaseStats& s) :
	Character(name, WARRIOR_WEAPON_DAMAGE_FUNCTION, s.CopyWith(WARRIOR_MASKS), WARRIOR_BASE_SKILLS)
{}

Warrior::Warrior(const Warrior & other):
	Character(other)
{
	if (other._weapon) {
		Weapon* w = other._weapon->Copy();
		if (!Equip(w))	delete w;
	}

	std::vector<StatsMask*> masks = _stats.Masks();
	uint16_t maskValue = MASK_WITH_PARRY.Mask();
	std::for_each(masks.begin(), masks.end(), [&](StatsMask* m) {
		if (!m)	return;
		m->Mask(maskValue);
	});
}

Warrior::~Warrior() {}

float Warrior::Parry() const
{
	return _stats.Parry();
}

void Warrior::Parry(float parry)
{
	_stats.Parry(parry);
}

bool Warrior::CanEquip(Weapon* weapon)
{
	return Weapon::Is<Melee>(*weapon);
}

Character* Warrior::Copy() const
{
	return new Warrior(*this);
}
