/**
	\file DamageFunction.cpp
	\brief DamageFunction definition
	\author Garnier Cl�ment
*/

#include "DamageFunction.h"
#include "../Utils/Exceptions.h"

DamageFunction::DamageFunction(): 
	_functionWithoutWeapon([](BaseStats, BaseStats) { return .0f; }),
	_functionWithWeapon([](BaseStats, BaseStats, Weapon&) { return .0f; }),
	_minCoef(.0f),
	_maxCoef(.0f)
{}

DamageFunction::DamageFunction(std::function<float(BaseStats, BaseStats, Weapon&)> function, float minCoef, float maxCoef) :
	_functionWithWeapon(function),
	_functionWithoutWeapon(nullptr),
	_minCoef(minCoef),
	_maxCoef(maxCoef)
{}

DamageFunction::DamageFunction(std::function<float(BaseStats, BaseStats)> function, float minCoef, float maxCoef) :
	_functionWithWeapon(nullptr),
	_functionWithoutWeapon(function),
	_minCoef(minCoef),
	_maxCoef(maxCoef)
{}

DamageFunction::DamageFunction(const DamageFunction& other) :
	_functionWithWeapon(other._functionWithWeapon),
	_functionWithoutWeapon(other._functionWithoutWeapon),
	_minCoef(other._minCoef),
	_maxCoef(other._maxCoef)
{}

DamageFunction::~DamageFunction() {}

float DamageFunction::MinDamage(BaseStats casterStats, BaseStats opponentStats)
{
	return __Damage(casterStats, opponentStats, nullptr, _minCoef);
}

float DamageFunction::MaxDamage(BaseStats casterStats, BaseStats opponentStats)
{
	return __Damage(casterStats, opponentStats, nullptr, _maxCoef);
}

float DamageFunction::MinDamage(Weapon& weapon, BaseStats casterStats, BaseStats opponentStats)
{
	return __Damage(casterStats, opponentStats, &weapon, _minCoef);
}

float DamageFunction::MaxDamage(Weapon& weapon, BaseStats casterStats, BaseStats opponentStats)
{
	return __Damage(casterStats, opponentStats, &weapon, _maxCoef);
}

float DamageFunction::operator()(BaseStats casterStats, BaseStats opponentStats)
{
	float coef = _minCoef + static_cast<float>(rand()) / static_cast<float>(RAND_MAX)* (_maxCoef - _minCoef);
	return __Damage(casterStats, opponentStats, nullptr, coef);
}

float DamageFunction::operator()(Weapon& weapon, BaseStats casterStats, BaseStats opponentStats)
{
	float coef = _minCoef + static_cast<float>(rand()) / static_cast<float>(RAND_MAX) * (_maxCoef - _minCoef);
	return __Damage(casterStats, opponentStats, &weapon, coef);
}

float DamageFunction::__Damage(BaseStats casterStats, BaseStats opponentStats, Weapon* weapon, float coef)
{
	float value = .0f;
	if (weapon) {
		if (!_functionWithWeapon)
			throw FunctionWithWeaponNotDefinedException();
		value = _functionWithWeapon(casterStats, opponentStats, *weapon);
	} else {
		if (!_functionWithoutWeapon)
			throw FunctionWithoutWeaponNotDefinedException();
		value = _functionWithoutWeapon(casterStats, opponentStats);
	}
	return value * coef;
}