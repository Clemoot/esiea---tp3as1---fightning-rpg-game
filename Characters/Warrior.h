/**
	\file Warrior.h
	\brief Warrior declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef WARRIOR_INCLUDED
/// Warrior.h included
#define WARRIOR_INCLUDED

#include "Character.h"

/// Warrior default skills
#define WARRIOR_BASE_SKILLS { new BattleCrySkill(), new RepairSkill() }
/// Warrior default weapon damage function
#define WARRIOR_WEAPON_DAMAGE_FUNCTION DamageFunction([](BaseStats casterStats, BaseStats opponentStats, Weapon& weapon) { return 7.f * (casterStats.Attack() + weapon.Damage()) / opponentStats.Defense(); })
/// Warrior default stats masks
#define WARRIOR_MASKS { new MASK_WITH_PARRY }

/**
	\class Warrior
	\brief In-game Warrior character model
*/
class JEU_DE_COMBAT_API Warrior : public Character
{
public:
	/// Warrior default constructor
	Warrior();
	/**
		\brief Warrior constructor
		\param name Name of the character
		\param life Life of the character
		\param atk  Attack of the character
		\param def  Defense of the character
		\param dex  Dexterity of the character
		\param intelligence Intelligence of the character
		\param spd  Speed of the character
		\param dodge Dodge rate of the character
		\param parry Parry rate of the character
		\param weapon Weapon to equip
	*/
	Warrior(std::string name,
			float life = .0f,
			float atk = .0f,
			float def = .0f,
			float dex = .0f,
			float intelligence = .0f,
			float spd = .0f,
			float dodge = .0f,
			float parry = .0f,
			Weapon* weapon = nullptr);
	/**
		\brief Warrior constructor
		\param name Name of the character
		\param s Stats of the character
	*/
	Warrior(std::string name, const BaseStats& s);
	/**
		\brief Warrior copy constructor
		\param other Mage to copy
	*/
	Warrior(const Warrior& other);
	/// Warrior destructor
	~Warrior();

	float Parry() const;		///< Parry rate getter (alias)
	void Parry(float parry);		///< Parry rate setter (alias)

	bool CanEquip(Weapon* weapon);	///< Checks if Warrior can equip the weapon

	/// Copy the warrior
	Character* Copy() const;
};

#endif