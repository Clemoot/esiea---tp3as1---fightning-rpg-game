/**
	\file Rogue.h
	\brief Rogue declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef ROGUE_INCLUDED
/// Rogue.h included
#define ROGUE_INCLUDED

#include "Character.h"

/// Rogue passive auras
#define ROGUE_PASSIVES { new PoisonOnHitAura() }
/// Rogue default skills
#define ROGUE_BASE_SKILLS { new VanishSkill() }
/// Rogue default weapon damage function
#define ROGUE_WEAPON_DAMAGE_FUNCTION DamageFunction([](BaseStats casterStats, BaseStats opponentStats, Weapon& weapon) { return 7.f * (casterStats.Dexterity() + weapon.Damage()) / opponentStats.Defense(); })
/// Rogue default stats masks
#define ROGUE_MASKS { new MASK_WITH_POISON_RATE }

/**
	\class Rogue
	\brief In-game Rogue character model
*/
class JEU_DE_COMBAT_API Rogue : public Character
{
public:
	/// Rogue default constructor
	Rogue();
	/**
	\brief Rogue constructor
	\param name Name of the character
	\param life Life of the character
	\param atk  Attack of the character
	\param def  Defense of the character
	\param dex  Dexterity of the character
	\param intelligence Intelligence of the character
	\param spd  Speed of the character
	\param dodge Dodge rate of the character
	\param poisonRate Poison rate of the character
	\param weapon Weapon to equip
*/
	Rogue(std::string name,
		float life = .0f,
		float atk = .0f,
		float def = .0f,
		float dex = .0f,
		float intelligence = .0f,
		float spd = .0f,
		float dodge = .0f,
		float poisonRate = .0f,
		Weapon* weapon = nullptr);
	/**
		\brief Rogue constructor
		\param name Name of the character
		\param s Stats of the character
	*/
	Rogue(std::string name, const BaseStats& s);
	/**
		\brief Rogue copy constructor
		\param other Mage to copy
	*/
	Rogue(const Rogue& other);
	/// Rogue destructor
	~Rogue();

	float PoisonRate() const;	///< Poison rate getter (alias)
	void PoisonRate(float poisonRate);	///< Poison rate setter (alias)

	bool CanEquip(Weapon* weapon);	///< Checks if Rogue can equip the weapon

	/// Copy the character
	Character* Copy() const;
};

#endif