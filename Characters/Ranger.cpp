/**
	\file Ranger.cpp
	\brief Ranger definition
	\author Garnier Cl�ment
*/

#include "Ranger.h"
#include <algorithm>
#include "../Skills/AimSkill.h"
#include "../Skills/CurePoisonSkill.h"
#include "../Weapons/Bow.h"
#include "../Weapons/Dagger.h"
#include "../Stats/StatsMask.h"

Ranger::Ranger() : Character("", RANGER_WEAPON_DAMAGE_FUNCTION, RANGER_BASE_SKILLS) {}

Ranger::Ranger(std::string name, float life, float atk, float def, float dex, float intelligence, float spd, float dodge, Weapon* weapon) :
	Character(name, RANGER_WEAPON_DAMAGE_FUNCTION, BaseStats(life, atk, def, dex, intelligence, spd, dodge), RANGER_BASE_SKILLS)
{
	Equip(weapon);
}

Ranger::Ranger(std::string name, const BaseStats& s) :
	Character(name, RANGER_WEAPON_DAMAGE_FUNCTION, s, RANGER_BASE_SKILLS)
{}

Ranger::Ranger(const Ranger & other):
	Character(other)
{
	if (other._weapon) {
		Weapon* w = other._weapon->Copy();
		if (!Equip(w))	delete w;
	}

	std::vector<StatsMask*> masks = _stats.Masks();
	uint16_t maskValue = StatsMask::C_COMMON;
	std::for_each(masks.begin(), masks.end(), [&](StatsMask* m) {
		if (!m)	return;
		m->Mask(maskValue);
	});
}

Ranger::~Ranger() {}

bool Ranger::CanEquip(Weapon* weapon)
{
	return Weapon::Is<Dagger>(*weapon) || Weapon::Is<Bow>(*weapon);
}

Character* Ranger::Copy() const
{
	return new Ranger(*this);
}
