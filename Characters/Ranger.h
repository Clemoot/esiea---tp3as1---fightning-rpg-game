/**
	\file Ranger.h
	\brief Ranger declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef RANGER_INCLUDED
/// Ranger.h included
#define RANGER_INCLUDED

#include "Character.h"

/// Maximal number of usages for CurePoisonSkill
#define CURE_POISON_MAX_USAGE 3

/// Ranger default skills
#define RANGER_BASE_SKILLS { new AimSkill(), new CurePoisonSkill(CURE_POISON_MAX_USAGE) }
/// Ranger default weapon damage function
#define RANGER_WEAPON_DAMAGE_FUNCTION DamageFunction([](BaseStats casterStats, BaseStats opponentStats, Weapon& weapon) { return 9.f * (casterStats.Dexterity() + weapon.Damage()) / opponentStats.Defense(); })

/**
	\class Ranger
	\brief In-game Ranger character model
*/
class JEU_DE_COMBAT_API Ranger : public Character
{
public:
	/// Ranger default constructor
	Ranger();
	/**
		\brief Ranger constructor
		\param name Name of the character
		\param life Life of the character
		\param atk  Attack of the character
		\param def  Defense of the character
		\param dex  Dexterity of the character
		\param intelligence Intelligence of the character
		\param spd  Speed of the character
		\param dodge Dodge rate of the character
		\param weapon Weapon to equip
	*/
	Ranger(std::string name,
		float life = .0f,
		float atk = .0f,
		float def = .0f,
		float dex = .0f,
		float intelligence = .0f,
		float spd = .0f,
		float dodge = .0f,
		Weapon* weapon = nullptr);
	/**
		\brief Ranger constructor
		\param name Name of the character
		\param s Stats of the character
	*/
	Ranger(std::string name, const BaseStats& s);
	/**
		\brief Ranger copy constructor
		\param other Ranger to copy
	*/
	Ranger(const Ranger& other);
	/// Ranger destructor
	~Ranger();

	bool CanEquip(Weapon* weapon);	///< Checks if Ranger can equip the weapon

	/// Copy the character
	Character* Copy() const;
};

#endif