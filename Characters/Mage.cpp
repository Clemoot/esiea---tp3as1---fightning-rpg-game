/**
	\file Mage.cpp
	\brief Mage definition
	\author Garnier Cl�ment
*/

#include "Mage.h"
#include <algorithm>
#include "../Skills/EnchantSkill.h"
#include "../Skills/HealSkill.h"
#include "../Skills/RegenManaSkill.h"
#include "../Weapons/Sword.h"
#include "../Weapons/Staff.h"

#include "../Stats/StatsMask.h"

Mage::Mage() : Character("", MAGE_WEAPON_DAMAGE_FUNCTION, BaseStats(MAGE_MASKS), MAGE_BASE_SKILLS)
{}

Mage::Mage(std::string name) : Character(name, MAGE_WEAPON_DAMAGE_FUNCTION, BaseStats(MAGE_MASKS), MAGE_BASE_SKILLS)
{}

Mage::Mage(std::string name, float life, float atk, float def, float dex, float intelligence, float spd, float dodge, Weapon* weapon) :
	Character(name, MAGE_WEAPON_DAMAGE_FUNCTION, BaseStats(life, atk, def, dex, intelligence, spd, dodge, .0f, .0f, .0f, MAGE_MASKS), MAGE_BASE_SKILLS)
{
	Equip(weapon);
}

Mage::Mage(std::string name, const BaseStats& stats) :
	Character(name, MAGE_WEAPON_DAMAGE_FUNCTION, stats.CopyWith(MAGE_MASKS), MAGE_BASE_SKILLS)
{}

Mage::Mage(const Mage& other) :
	Character(other)
{
	if (other._weapon) {
		Weapon* w = other._weapon->Copy();
		if (!Equip(w))	delete w;
	}

	std::vector<StatsMask*> masks = _stats.Masks();
	uint16_t maskValue = MASK_WITH_MANA.Mask();
	std::for_each(masks.begin(), masks.end(), [&](StatsMask* m) {
		if (!m)	return;
		m->Mask(maskValue);
	});
}

Mage::~Mage()
{}

float Mage::Mana() const
{
	return _stats.Mana();
}

bool Mage::CanEquip(Weapon* weapon)
{
	return !Weapon::Is<Sword>(*weapon) || Weapon::Is<Staff>(*weapon);
}

Character* Mage::Copy() const
{
	return new Mage(*this);
}
