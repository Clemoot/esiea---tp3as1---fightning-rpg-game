/**
	\file Character.h
	\brief Character declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef CHARACTER_INCLUDED
/// Character.h included
#define CHARACTER_INCLUDED

#include "../Utils/LibExport.h"

#include <iostream>

#include "../Rules.h"
#include "DamageFunction.h"
#include "../Weapons/Weapon.h"

class Skill;	// Forward declaration to avoid circular dependencies (-> Skill -> CharacterInstance -> Character)
class CharacterAura;		// Forward declaration to avoid circular dependencies (-> CharacterAura -> CharacterInstance -> Character)

/// Character default skills
#define CHARACTER_DEFAULT_BASE_SKILLS { new HandAttackSkill(), new WeaponAttackSkill() }
/// Character default hand damage function
#define CHARACTER_HAND_DAMAGE_FUNCTION DamageFunction([](BaseStats casterStats, BaseStats opponentStats) { return 16.f * casterStats.Attack() / opponentStats.Defense(); }, .95f, 1.05f)
/// Character default stats masks
#define CHARACTER_DEFAULT_MASKS { new AlwaysPositiveMask, new RatesMask }

/**
	\class Character
	\brief In-game character model
*/
class JEU_DE_COMBAT_API Character
{
public:
	/**
		\brief Character constructor
		\param name Name of the character
		\param damageFunction Function determining damage dealt with a weapon
		\param life Life of the character
		\param atk  Attack of the character
		\param def  Defense of the character
		\param dex  Dexterity of the character
		\param intelligence Intelligence of the character
		\param spd  Speed of the character
		\param dodge Dodge rate of the character
	*/
	Character(std::string name = "", 
				DamageFunction damageFunction = NULL_WEAPON_DAMAGE,
				float life = .0f,
				float atk = .0f,
				float def = .0f,
				float dex = .0f,
				float intelligence = .0f,
				float spd = .0f,
				float dodge = .0f);
	/**
		 \brief Character constructor
		 \param name Name of the character
		 \param damageFunction Function determining damage dealt with a weapon
		 \param skills Castable skills by the character
		 \param passives Passive auras of the character
	*/
	Character(std::string name, DamageFunction damageFunction, std::vector<Skill*> skills, std::vector<CharacterAura*> passives = std::vector<CharacterAura*>());
	/**
		\brief Character constructor
		\param name Name of the character
		\param damageFunction Function determining damage dealt with a weapon
		\param s Base stats of the character
	*/
	Character(std::string name, DamageFunction damageFunction, const BaseStats& s);
	/**
		\brief Character constructor
		\param name Name of the character
		\param damageFunction Function determining damage dealt with a weapon
		\param s Base stats of the character
		\param skills Castable skills by the character
		\param passives Passive auras of the character
	*/
	Character(std::string name, DamageFunction damageFunction, const BaseStats& s, std::vector<Skill*> skills, std::vector<CharacterAura*> passives = std::vector<CharacterAura*>());
	/**
		\brief Character copy constructor
		\param other Character to copy
	*/
	Character(const Character& other);
	/// Character destructor
	virtual ~Character();

	std::string Name() const;		///< Name getter
	BaseStats Stats() const;		///< Stats getter
	float Life() const;				///< Life getter (alias)
	float Attack() const;			///< Attack getter	(alias)
	float Defense() const;			///< Defense getter (alias)
	float Dexterity() const;		///< Dexterity getter (alias)
	float Intelligence() const;		///< Intelligence getter (alias)
	float Speed() const;			///< Speed getter (alias)
	float Dodge() const;			///< Dodge rate getter (alias)
	std::vector<Skill*> Skills() const;		///< Castable skills getter
	std::vector<CharacterAura*> Passives() const;		///< Passive auras getter
	Weapon* EquipedWeapon() const;	///< Equiped weapon getter

	void Name(std::string name);			///< Name setter
	void Life(float life);					///< Life setter (alias)
	void Attack(float atk);					///< Attack setter (alias)
	void Defense(float def);				///< Defense setter (alias)
	void Dexterity(float dex);				///< Dexterity setter (alias)
	void Intelligence(float intelligence);	///< Intelligence setter (alias)
	void Speed(float spd);					///< Speed setter (alias)
	void Dodge(float dodge);				///< Dodge rate setter (alias)
	void Stats(BaseStats stats);			///< Stats setter

	DamageFunction HandDamageFunction() const;		///< Damage function with hands getter
	DamageFunction WeaponDamageFunction() const;	///< Damage function with weapon getter

	/// Copy the character
	virtual Character* Copy() const = 0;

	/// Checks if the character is castable in a child character
	template<class T>
	static bool Is(Character& c) {
		try {
			dynamic_cast<T&>(c);
			return true;
		} catch (const std::bad_cast&) {
			return false;
		}
	}
	/// Checks if the weapon is equipable
	virtual bool CanEquip(Weapon* weapon) = 0;
	/// Equips a weapon
	bool Equip(Weapon* weapon);

	/**
		\brief Character equality
		\param other Right value
		\return true if equal, false otherwise
	*/
	bool operator==(const Character& other) const;

protected:
	std::string _name;		///< Name of the character
	BaseStats _stats;		///< Character base stats
	Weapon* _weapon;		///< Equiped weapon
	std::vector<Skill*> _skills;	///< Castable skills
	std::vector<CharacterAura*> _passives;	///< Passive auras
	DamageFunction _weaponDamageFunction;	///< Function determining damage dealt with weapon
	DamageFunction _handDamageFunction;		///< Function determining damage dealt with hands
};

/// Alias: A team is a 3-character party
typedef Character* Team[CHARACTERS_PER_TEAM];

#endif