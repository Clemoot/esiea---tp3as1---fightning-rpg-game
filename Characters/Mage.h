/**
	\file Mage.h
	\brief Mage declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef MAGE_INCLUDED
/// Mage.h included
#define MAGE_INCLUDED

#include "Character.h"

/// Mage default skills
#define MAGE_BASE_SKILLS { new EnchantSkill(), new HealSkill(), new RegenManaSkill() }
/// Mage default weapon damage function
#define MAGE_WEAPON_DAMAGE_FUNCTION DamageFunction([](BaseStats casterStats, BaseStats opponentStats, Weapon& weapon) { return 5.f * (casterStats.Intelligence() + weapon.Damage()) / opponentStats.Defense(); })
/// Mage default stats masks
#define MAGE_MASKS { new MASK_WITH_MANA }

/**
	\class Mage
	\brief In-game Mage character model
*/
class JEU_DE_COMBAT_API Mage : public Character
{
public:
	/// Mage default constructor
	Mage();
	/**
		\brief Mage constructor
		\param name Name of the character
	*/
	Mage(std::string name);
	/**
		\brief Mage constructor
		\param name Name of the character
		\param life Life of the character
		\param atk  Attack of the character
		\param def  Defense of the character
		\param dex  Dexterity of the character
		\param intelligence Intelligence of the character
		\param spd  Speed of the character
		\param dodge Dodge rate of the character
		\param weapon Weapon to equip
	*/
	Mage(std::string name,
		float life,
		float atk = .0f,
		float def = .0f,
		float dex = .0f,
		float intelligence = .0f,
		float spd = .0f,
		float dodge = .0f,
		Weapon* weapon = nullptr);
	/**
		\brief Mage constructor
		\param name Name of the character
		\param stats Stats of the character
	*/
	Mage(std::string name, const BaseStats& stats);
	/**
		\brief Mage copy constructor
		\param other Mage to copy
	*/
	Mage(const Mage& other);
	/// Mage destructor
	~Mage();

	float Mana() const;	///< Mana getter (alias)

	bool CanEquip(Weapon* weapon);	///< Checks if Mage can equip the weapon

	/// Copy the character
	Character* Copy() const;
};

#endif