/**
	\file Rogue.cpp
	\brief Rogue definition
	\author Garnier Cl�ment
*/

#include "Rogue.h"
#include <algorithm>
#include "../Skills/VanishSkill.h"
#include "../Auras/PoisonOnHitAura.h"
#include "../Stats/StatsMask.h"
#include "../Weapons/Dagger.h"
#include "../Weapons/Bow.h"

#include "../Stats/StatsMask.h"

Rogue::Rogue() : Character("", ROGUE_WEAPON_DAMAGE_FUNCTION, BaseStats(ROGUE_MASKS), ROGUE_BASE_SKILLS, ROGUE_PASSIVES) {}

Rogue::Rogue(std::string name, float life, float atk, float def, float dex, float intelligence, float spd, float dodge, float poisonRate, Weapon* weapon) :
	Character(name, ROGUE_WEAPON_DAMAGE_FUNCTION, BaseStats(life, atk, def, dex, intelligence, spd, dodge, .0f, poisonRate, .0f, ROGUE_MASKS), ROGUE_BASE_SKILLS)
{
	CharacterAura* a = new PoisonOnHitAura();
	_passives.push_back(a);
	Equip(weapon);
}

Rogue::Rogue(std::string name, const BaseStats& s):
	Character(name, ROGUE_WEAPON_DAMAGE_FUNCTION, s.CopyWith(ROGUE_MASKS), ROGUE_BASE_SKILLS, ROGUE_PASSIVES)
{}

Rogue::Rogue(const Rogue& other):
	Character(other)
{
	if (other._weapon) {
		Weapon* w = other._weapon->Copy();
		if (!Equip(w))	delete w;
	}

	std::vector<StatsMask*> masks = _stats.Masks();
	uint16_t maskValue = MASK_WITH_POISON_RATE.Mask();
	std::for_each(masks.begin(), masks.end(), [&](StatsMask* m) {
		if (!m)	return;
		m->Mask(maskValue);
	});
}


Rogue::~Rogue() {}

float Rogue::PoisonRate() const
{
	return _stats.PoisonRate();
}

void Rogue::PoisonRate(float poisonRate)
{
	_stats.PoisonRate(poisonRate);
}

bool Rogue::CanEquip(Weapon* weapon)
{
	return Weapon::Is<Dagger>(*weapon) || Weapon::Is<Bow>(*weapon);
}

Character* Rogue::Copy() const
{
	return new Rogue(*this);
}
