/**
	\file Character.cpp
	\brief Character definition
	\author Garnier Cl�ment
*/

#include "Character.h"

#include <algorithm>

#include "../Skills/HandAttackSkill.h"
#include "../Skills/WeaponAttackSkill.h"

#include "../Stats/SpecificMasks.h"

Character::Character(std::string name, DamageFunction damageFunction, float life, float atk, float def, float dex, float intelligence, float spd, float dodge) :
	_name(name),
	_skills(CHARACTER_DEFAULT_BASE_SKILLS),
	_stats(life, atk, def, dex, intelligence, spd, dodge),
	_weapon(nullptr),
	_handDamageFunction(CHARACTER_HAND_DAMAGE_FUNCTION),
	_weaponDamageFunction(damageFunction)
{
	_stats.AddMasks(CHARACTER_DEFAULT_MASKS);
}

Character::Character(std::string name, DamageFunction damageFunction, std::vector<Skill*> skills, std::vector<CharacterAura*> passives):
	_name(name),
	_skills(CHARACTER_DEFAULT_BASE_SKILLS),
	_passives(passives),
	_stats(BaseStats::IDENTITY),
	_handDamageFunction(CHARACTER_HAND_DAMAGE_FUNCTION),
	_weaponDamageFunction(damageFunction),
	_weapon(nullptr)
{
	_skills.insert(_skills.end(), skills.begin(), skills.end());
	_stats.AddMasks(CHARACTER_DEFAULT_MASKS);
}

Character::Character(std::string name, DamageFunction damageFunction, const BaseStats& s):
	_name(name),
	_stats(s),
	_weapon(nullptr),
	_handDamageFunction(CHARACTER_HAND_DAMAGE_FUNCTION),
	_weaponDamageFunction(damageFunction)
{
	_stats.AddMasks(CHARACTER_DEFAULT_MASKS);
}

Character::Character(std::string name, DamageFunction damageFunction, const BaseStats & s, std::vector<Skill*> skills, std::vector<CharacterAura*> passives):
	_name(name),
	_stats(s),
	_skills(CHARACTER_DEFAULT_BASE_SKILLS),
	_passives(passives),
	_handDamageFunction(CHARACTER_HAND_DAMAGE_FUNCTION),
	_weaponDamageFunction(damageFunction),
	_weapon(nullptr)
{
	_skills.insert(_skills.end(), skills.begin(), skills.end());
	_stats.AddMasks(CHARACTER_DEFAULT_MASKS);
}

Character::Character(const Character& other) :
	_name(other._name),
	_stats(other._stats),
	_skills(other._skills),
	_passives(other._passives),
	_weaponDamageFunction(other._weaponDamageFunction),
	_handDamageFunction(other._handDamageFunction),
	_weapon(nullptr)
{
	if (!_stats.HasMask<AlwaysPositiveMask>())	_stats.AddMask(new AlwaysPositiveMask);
	if (!_stats.HasMask<RatesMask>())	_stats.AddMask(new RatesMask);
}

Character::~Character() {}

std::string Character::Name() const
{
	return _name;
}

BaseStats Character::Stats() const
{
	return _stats;
}

float Character::Life() const
{
	return _stats.Life();
}

float Character::Attack() const
{
	return _stats.Attack();
}

float Character::Defense() const
{
	return _stats.Defense();
}

float Character::Dexterity() const
{
	return _stats.Dexterity();
}

float Character::Intelligence() const
{
	return _stats.Intelligence();
}

float Character::Speed() const
{
	return _stats.Speed();
}

float Character::Dodge() const
{
	return _stats.Dodge();
}

std::vector<Skill*> Character::Skills() const
{
	return _skills;
}

std::vector<CharacterAura*> Character::Passives() const
{
	return _passives;
}

Weapon* Character::EquipedWeapon() const
{
	return _weapon;
}

void Character::Name(std::string name)
{
	_name = name;
}

void Character::Life(float life)
{
	_stats.Life(life);
}

void Character::Attack(float atk)
{
	_stats.Attack(atk);
}

void Character::Defense(float def)
{
	_stats.Defense(def);
}

void Character::Dexterity(float dex)
{
	_stats.Dexterity(dex);
}

void Character::Intelligence(float intelligence)
{
	_stats.Intelligence(intelligence);
}

void Character::Speed(float spd)
{
	_stats.Speed(spd);
}

void Character::Dodge(float dodge)
{
	_stats.Dodge(dodge);
}

void Character::Stats(BaseStats stats)
{
	_stats = stats;
}

DamageFunction Character::HandDamageFunction() const
{
	return _handDamageFunction;
}

DamageFunction Character::WeaponDamageFunction() const
{
	return _weaponDamageFunction;
}

bool Character::Equip(Weapon* weapon)
{
	if (!weapon)	return false;
	if (!CanEquip(weapon))	return false;
	_weapon = weapon;
	return true;
}

bool Character::operator==(const Character& other) const
{
	return _name == other._name && 
		_stats == other._stats;
}
