/**
	\file DamageFunction.h
	\brief DamageFunction declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef DAMAGE_FUNCTION_INCLUDED
/// DamageFunction.h included
#define DAMAGE_FUNCTION_INCLUDED

#include <functional>
#include "../Stats/BaseStats.h"
#include "../Utils/LibExport.h"

/// Default minimal damage multiplier
#define DEFAULT_MIN_COEF .85f
/// Default maximal damage multiplier
#define DEFAULT_MAX_COEF 1.f

/// Function dealing no damage
#define NULL_WEAPON_DAMAGE DamageFunction([](BaseStats, BaseStats) { return .0f; })

class Weapon;	/// Forward declaration to avoid circular dependencies

/**
	\class DamageFunction
	\brief Object handling functions for hands and weapon damage
*/
class JEU_DE_COMBAT_API DamageFunction {
public:
	/// DamageFunction default constructor
	DamageFunction();
	/**
		\brief DamageFunction constructor
		\param function Function determining weapon damage
		\param minCoef Minimal value for damage multiplier
		\param maxCoef Maximal value for damage multiplier
	*/
	DamageFunction(std::function<float(BaseStats, BaseStats, Weapon&)> function, float minCoef = DEFAULT_MIN_COEF, float maxCoef = DEFAULT_MAX_COEF);
	/**
		\brief DamageFunction constructor
		\param function Function determining hands damage
		\param minCoef Minimal value for damage multiplier
		\param maxCoef Maximal value for damage multiplier
	*/
	DamageFunction(std::function<float(BaseStats, BaseStats)> function, float minCoef = DEFAULT_MIN_COEF, float maxCoef = DEFAULT_MAX_COEF);
	/**
		\brief DamageFunction copy constructor
		\param other DamageFunction to copy
	*/
	DamageFunction(const DamageFunction& other);
	/// DamageFunction desctructor
	~DamageFunction();

	/// Simulates minimal value with hand function
	float MinDamage(BaseStats casterStats, BaseStats opponentStats);
	/// Simulates minimal value with weapon function
	float MinDamage(Weapon& weapon, BaseStats casterStats, BaseStats opponentStats);
	/// Simulates maximal value with hand function
	float MaxDamage(BaseStats casterStats, BaseStats opponentStats);
	/// Simulates maximal value with weapon function
	float MaxDamage(Weapon& weapon, BaseStats casterStats, BaseStats opponentStats);

	/// Call hand damage function with a random multiplier
	float operator()(BaseStats casterStats, BaseStats opponentStats);
	/// Call weapon damage function with a random multiplier
	float operator()(Weapon& weapon, BaseStats casterStats, BaseStats opponentStatsn);

private:
	/// Computes damage
	float __Damage(BaseStats casterStats, BaseStats opponentStats, Weapon* weapon, float coef);

	std::function<float(BaseStats, BaseStats, Weapon&)> _functionWithWeapon;	///< Function determining weapon damage
	std::function<float(BaseStats, BaseStats)> _functionWithoutWeapon;			///< Function determining hands damage
	float _minCoef, _maxCoef;	///< Minimal and Maximal value for damage multiplier
};

#endif