/**
	\file Characters.h
	\brief Includes all Character-related headers
	\author Garnier Cl�ment
*/

#pragma once

#ifndef CHARACTERS_INCLUDED
#define CHARACTERS_INCLUDED

#include "Warrior.h"
#include "Ranger.h"
#include "Mage.h"
#include "Rogue.h"

#endif