/**
	\file Sword.h
	\brief Sword declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef SWORD_WEAPON_INCLUDED
/// Sword.h included
#define SWORD_WEAPON_INCLUDED

#include "Melee.h"

/// Durability usage
#define SWORD_DURABILITY_USAGE 2 + (rand() % 4)

/**
	\class Sword
	\brief Melee weapon
*/
class JEU_DE_COMBAT_API Sword: public Melee
{
public:
	/// Sword default constructor
	Sword();
	/**
		\brief Sword constructor
		\param name Name of the weapon
	*/
	Sword(std::string name);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
	*/
	Sword(std::string name, float dmg);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param durability Durability of the weapon
	*/
	Sword(std::string name, float dmg, uint32_t durability);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param stats Stats granted to the owner
	*/
	Sword(std::string name, float dmg, BaseStats stats);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param durability Durability of the weapon
		\param stats Stats granted to the owner
	*/
	Sword(std::string name, float dmg, uint32_t durability, BaseStats stats);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
		\param durability Durability of the weapon
	*/
	Sword(std::string name, float dmg, float crit, uint32_t durability);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
		\param durability Durability of the weapon
		\param stats Stats granted to the owner
	*/
	Sword(std::string name, float dmg, float crit, uint32_t durability, BaseStats stats);
	/**
		\brief Sword copy constructor
		\param sword Sword to copy
	*/
	Sword(const Sword& sword);
	/// Sword destructor
	~Sword();

	/// Uses the sword
	float Use(CharacterInstance& instance);

	/// Copy the sword
	Weapon* Copy() const;
};

#endif