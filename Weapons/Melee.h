/**
	\file Melee.h
	\brief Melee declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef MELEE_WEAPON_INCLUDED
/// Melee.h included
#define MELEE_WEAPON_INCLUDED

#include "Weapon.h"

/// Damage multiplier if the weapon is broken
#define DAMAGE_POURCENTAGE_IF_BROKEN .1f

/**
	\class Melee
	\brief Melee weapon
*/
class JEU_DE_COMBAT_API Melee: public Weapon
{
public:
	/// Melee default constructor
	Melee();
	/**
		\brief Melee constructor
		\param name Name of the weapon
	*/
	Melee(std::string name);
	/**
		\brief Melee constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
	*/
	Melee(std::string name, float dmg);
	/**
		\brief Melee constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param durability Durability of the weapon
	*/
	Melee(std::string name, float dmg, uint32_t durability);
	/**
		\brief Melee constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param stats Stats granted to the owner
	*/
	Melee(std::string name, float dmg, BaseStats stats);
	/**
		\brief Melee constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param durability Durability of the weapon
		\param stats Stats granted to the owner
	*/
	Melee(std::string name, float dmg, uint32_t durability, BaseStats stats);
	/**
		\brief Melee constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
		\param durability Durability of the weapon
	*/
	Melee(std::string name, float dmg, float crit, uint32_t durability);
	/**
		\brief Melee constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
		\param durability Durability of the weapon
		\param stats Stats granted to the owner
	*/
	Melee(std::string name, float dmg, float crit, uint32_t durability, BaseStats stats);
	/**
		\brief Melee copy constructor
		\param melee Sword to copy
	*/
	Melee(const Melee& melee);
	/// Melee destructor
	~Melee();

	float Damage() const;			///< Damage getter
	uint32_t Durability() const;	///< Durability getter
	void Durability(uint32_t durability);	///< Durability setter


	/// Check if the weapon can be used
	bool Usable(CharacterInstance& instance) const;
	/// Checks if the weapon is broken
	bool Broken() const;

	/**
		\brief Repair the weapon
		\param durability Gained durability
		\return true if succeed, false otherwise
	*/
	bool Repair(int durability);

protected:
	uint32_t _durability;	///< Durability
};

#endif