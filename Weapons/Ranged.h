/**
	\file Ranged.h
	\brief Ranged declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef RANGED_WEAPON_INCLUDED
/// Ranged.h included
#define RANGED_WEAPON_INCLUDED

#include "Weapon.h"

/**
	\class Ranged
	\brief Ranged weapon
*/
class JEU_DE_COMBAT_API Ranged : public Weapon
{
public:
	/// Ranged default constructor
	Ranged();
	/**
		\brief Ranged constructor
		\param name Name of the weapon
	*/
	Ranged(std::string name);
	/**
		\brief Ranged constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
	*/
	Ranged(std::string name, float dmg);
	/**
		\brief Ranged constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
	*/
	Ranged(std::string name, float dmg, float crit);
	/**
		\brief Ranged constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param stats Stats granted to the owner
	*/
	Ranged(std::string name, float dmg, BaseStats stats);
	/**
		\brief Ranged constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
		\param stats Stats granted to the owner
	*/
	Ranged(std::string name, float dmg, float crit, BaseStats stats);
	/**
		\brief Ranged copy constructor
		\param ranged Ranged to copy
	*/
	Ranged(const Ranged& ranged);
	/// Ranged destructor
	~Ranged();
};

#endif