/**
	\file Weapon.h
	\brief Weapon declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef WEAPON_INCLUDED
/// Weapon.h included
#define WEAPON_INCLUDED

#include "../Utils/LibExport.h"

#include <iostream>

#include "../Stats/WeaponStats.h"

class CharacterInstance;	// Forward declaration to avoid circular dependencies
class WeaponAura;			// Forward declaration to avoid circular dependencies

/// Weapon default masks
#define WEAPON_DEFAULT_MASKS { new WeaponAlwaysPositiveMask, new RatesMask }

/**
	\class Weapon
	\brief In-game Weapon
*/
class JEU_DE_COMBAT_API Weapon
{
public:
	/// Weapon default constructor
	Weapon();
	/**
		\brief Weapon constructor
		\param name Name of the weapon
	*/
	Weapon(std::string name);
	/**
		\brief Weapon constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
	*/
	Weapon(std::string name, float dmg);
	/**
		\brief Weapon constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
	*/
	Weapon(std::string name, float dmg, float crit);
	/**
		\brief Weapon constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param stats Stats granted to owner
	*/
	Weapon(std::string name, float dmg, BaseStats stats);
	/**
		\brief Weapon constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
		\param stats Stats granted to owner
	*/
	Weapon(std::string name, float dmg, float crit, BaseStats stats);
	/**
		\brief Weapon constructor
		\param name Name of the weapon
		\param stats Stats of the weapon
	*/
	Weapon(std::string name, WeaponStats stats);
	/**
		\brief Weapon copy constructor
		\param weapon Weapon to copy
	*/
	Weapon(const Weapon& weapon);
	/// Weapon destructor
	virtual ~Weapon();

	std::string Name() const;				///< Name getter
	WeaponStats OriginalStats() const;		///< Base stats of the weapon
	float Damage() const;					///< Damage getter (alias)
	float CriticalRate() const;				///< Critical rate getter (alias)
	WeaponStats Stats() const;				///< Computed stats with auras

	void Name(std::string name);			///< Name setter
	void Stats(WeaponStats stats);			///< Base stats setter
	void Damage(float damage);				///< Damage setter (alias)
	void CriticalRate(float criticalRate);	///< Critical setter (alias)

	/// Checks if the weapon can be used
	virtual bool Usable(CharacterInstance& instance) const = 0;
	/// Uses the weapon
	virtual float Use(CharacterInstance& instance) = 0;
	/// Triggers auras
	void Used();

	/// Adds an aura to the weapon
	bool AddAura(WeaponAura* aura);
	/// Removes an aura from the weapon
	bool RemoveAura(WeaponAura* aura);
	/// Checks if the weapon already has a specific mask
	template<class T>
	bool HasAura();

	/// Copy the weapon
	virtual Weapon* Copy() const = 0;

	/// Checks if the weapon is castable in a child weapon
	template<class T>
	static bool Is(Weapon& w) {
		try {
			dynamic_cast<T&>(w);
			return true;
		} catch (const std::bad_cast) {
			return false;
		}
	}

	/**
		\brief Weapon equality
		\param other Right value
		\return true if equal, false otherwise
	*/
	bool operator==(const Weapon& other) const;

protected:
	std::string _name;			///< Name of the weapon
	WeaponStats _stats;			///< Base stats of the weapon
	WeaponStats _computedStats;		///< Computed stats of the weapon
	std::vector<WeaponAura*> _auras;	///< Auras

	/// Coputes stats with auras
	void __ComputeStats();
};

template<class T>
inline bool Weapon::HasAura()
{
	auto pos = std::find_if(_auras.begin(), _auras.end(), [](WeaponAura* a) {
		if (!a)	return;
		try {
			dynamic_cast<T&>(*a);
			return true;
		}
		catch (const std::bad_cast&) {
			return false;
		}
		});
	return pos != _auras.end();
}


#endif