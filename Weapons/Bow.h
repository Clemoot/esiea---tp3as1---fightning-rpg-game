/**
	\file Bow.h
	\brief Bow declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef BOW_WEAPON_INCLUDED
/// Bow.h included
#define BOW_WEAPON_INCLUDED

#include "Ranged.h"

/// Number of arrows after each usage
#define ARROW_USAGE 1

/**
	\class Bow
	\brief Bow weapon
*/
class JEU_DE_COMBAT_API Bow: public Ranged
{
public:
	/// Sword default constructor
	Bow();
	/**
		\brief Sword constructor
		\param name Name of the weapon
	*/
	Bow(std::string name);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
	*/
	Bow(std::string name, float dmg);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param nbArrows Number of arrows in the quiver
	*/
	Bow(std::string name, float dmg, uint32_t nbArrows);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param stats Stats granted to the owner
	*/
	Bow(std::string name, float dmg, BaseStats stats);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param nbArrows Number of arrows in the quiver
		\param stats Stats granted to the owner
	*/
	Bow(std::string name, float dmg, uint32_t nbArrows, BaseStats stats);
	/**
		\brief Sword constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param nbArrows Number of arrows in the quiver
		\param crit Critical rate of the weapon
		\param stats Stats granted to the owner
	*/
	Bow(std::string name, float dmg, uint32_t nbArrows, float crit, BaseStats stats);
	/**
		\brief Bow copy constructor
		\param other Bow to copy
	*/
	Bow(const Bow& other);
	/// Bow destructor
	~Bow();

	uint32_t NbArrows() const;			///< Number of arrows geter
	void NbArrows(uint32_t nbArrows);	///< Number of arrows setter

	/// Checks if the bow can be used
	bool Usable(CharacterInstance& instance) const;
	/// Uses the bow
	float Use(CharacterInstance& instance);

	/// Copy the bow
	Weapon* Copy() const;

private:
	uint32_t _nbArrows;	///< Number of arrows
};

#endif