/**
	\file Weapon.cpp
	\brief Weapon declaration
	\author Garnier Cl�ment
*/

#include "Weapon.h"

#include <algorithm>

#include "../Instances/CharacterInstance.h"

#include "../Auras/WeaponStatsAura.h"
#include "../Auras/FadeOnAttackAura.h"

#include "../Stats/SpecificMasks.h"

#include "../Utils/PointerDeleter.h"

Weapon::Weapon(): 
	_name(""),
	_stats(WeaponStats::NULL_VALUE) 
{
	_stats.AddMasks(WEAPON_DEFAULT_MASKS);
	__ComputeStats();
}

Weapon::Weapon(std::string name): 
	_name(name),
	_stats(WeaponStats::NULL_VALUE)
{
	_stats.AddMasks(WEAPON_DEFAULT_MASKS);
	__ComputeStats();
}

Weapon::Weapon(std::string name, float dmg): 
	_name(name),
	_stats(dmg, .0f, BaseStats::NULL_VALUE)
{
	_stats.AddMasks(WEAPON_DEFAULT_MASKS);
	__ComputeStats();
}

Weapon::Weapon(std::string name, float dmg, float crit): 
	_name(name),
	_stats(.0f, .0f, BaseStats::NULL_VALUE)
{
	_stats.AddMasks(WEAPON_DEFAULT_MASKS);
	__ComputeStats();
}

Weapon::Weapon(std::string name, float dmg, BaseStats stats): 
	_name(name),
	_stats(dmg, .0f, stats)
{
	_stats.AddMasks(WEAPON_DEFAULT_MASKS);
	__ComputeStats();
}

Weapon::Weapon(std::string name, float dmg, float crit, BaseStats stats): 
	_name(name),
	_stats(dmg, crit, stats)
{
	_stats.AddMasks(WEAPON_DEFAULT_MASKS);
	__ComputeStats();
}

Weapon::Weapon(std::string name, WeaponStats stats) :
	_name(name),
	_stats(stats)
{
	_stats.AddMasks(WEAPON_DEFAULT_MASKS);
	__ComputeStats();
}

Weapon::Weapon(const Weapon& weapon):
	_name(weapon._name),
	_stats(weapon._stats)
{
	__ComputeStats();
}

Weapon::~Weapon() {}

std::string Weapon::Name() const
{
	return _name;
}

WeaponStats Weapon::OriginalStats() const
{
	return _stats;
}

float Weapon::Damage() const
{
	return _computedStats.Damage();
}

float Weapon::CriticalRate() const
{
	return _computedStats.CriticalRate();
}

WeaponStats Weapon::Stats() const
{
	return _computedStats;
}

void Weapon::Name(std::string name)
{
	_name = name;
}

void Weapon::Stats(WeaponStats stats)
{
	_stats = stats;
	__ComputeStats();
}

void Weapon::Damage(float damage)
{
	_stats.Damage(damage);
	__ComputeStats();
}

void Weapon::CriticalRate(float criticalRate)
{
	_stats.CriticalRate(criticalRate);
	__ComputeStats();
}

void Weapon::Used()
{
	auto remove = std::remove_if(_auras.begin(), _auras.end(), PointerDeleter<WeaponAura>([](WeaponAura* a) {
		if (!a)	return true;
		return Aura::Is<FadeOnAttackAura>(*a);
		}));
	if (remove != _auras.end())
		_auras.erase(remove);
	__ComputeStats();
}

bool Weapon::AddAura(WeaponAura* aura)
{
	if (!aura)	return false;
	auto remove = std::remove_if(_auras.begin(), _auras.end(), PointerDeleter<WeaponAura>([&](WeaponAura*& a) {
		if (!a)	return true;
		return a->Name() == aura->Name();
		}));
	if (remove != _auras.end())
		_auras.erase(remove);
	_auras.push_back(aura);
	if (Aura::Is<WeaponStatsAura>(*aura)) {
		__ComputeStats();
	}
	return true;
}

bool Weapon::RemoveAura(WeaponAura* aura)
{
	bool isStatAura = (aura) ? Aura::Is<WeaponStatsAura>(*aura) : false;
	auto pos = std::remove_if(_auras.begin(), _auras.end(), PointerDeleter<WeaponAura>([&](WeaponAura*& a) {
		return aura == a;
		}));
	if (pos == _auras.end())
		return false;
	_auras.erase(pos);
	if (isStatAura)	__ComputeStats();
	return true;
}

bool Weapon::operator==(const Weapon& other) const
{
	return _name == other._name && _stats == other._stats;
}

void Weapon::__ComputeStats()
{
	WeaponStats newStats(_stats);
	std::vector<WeaponStatsAura> sAuras;
	
	std::for_each(_auras.begin(), _auras.end(), [&](WeaponAura*& a) {
		if (!a)	return;
		try {
			WeaponStatsAura& sAura = dynamic_cast<WeaponStatsAura&>(*a);
			newStats += sAura.FlatStats();
			sAuras.push_back(sAura);
		}
		catch (const std::bad_cast&) {}
	});

	std::for_each(sAuras.begin(), sAuras.end(), [&](WeaponStatsAura& a) {
		newStats *= a.Multiplier();
	});

	_computedStats = newStats;
}
