/**
	\file Staff.cpp
	\brief Staff declaration
	\author Garnier Cl�ment
*/

#include "Staff.h"
#include "../Instances/CharacterInstance.h"

Staff::Staff() : Ranged(), _manaCost(.0f) {}

Staff::Staff(std::string name) : Ranged(name), _manaCost(.0f) {}

Staff::Staff(std::string name, float dmg) : Ranged(name, dmg), _manaCost(.0f) {}

Staff::Staff(std::string name, float dmg, float manaCost) : Ranged(name, dmg), _manaCost(manaCost) {}

Staff::Staff(std::string name, float dmg, BaseStats stats): Ranged(name, dmg, stats), _manaCost(.0f) {}

Staff::Staff(std::string name, float dmg, float manaCost, BaseStats stats): Ranged(name, dmg, stats), _manaCost(manaCost) {}

Staff::Staff(std::string name, float dmg, float manaCost, float crit, BaseStats stats): Ranged(name, dmg, crit, stats), _manaCost(manaCost) {}

Staff::Staff(const Staff& other): Ranged(other), _manaCost(other._manaCost) {}

Staff::~Staff() {}

float Staff::ManaCost() const
{
	return _manaCost;
}

void Staff::ManaCost(float manaCost)
{
	_manaCost = manaCost;
}

bool Staff::Usable(CharacterInstance& instance) const
{
	return instance.Resources().Mana() >= _manaCost;
}

float Staff::Use(CharacterInstance& instance)
{
	instance.Consume(_manaCost);
	return _manaCost;
}

Weapon* Staff::Copy() const
{
	return new Staff(*this);
}
