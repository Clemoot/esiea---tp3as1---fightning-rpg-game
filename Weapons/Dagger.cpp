/**
	\file Dagger.cpp
	\brief Dagger declaration
	\author Garnier Cl�ment
*/

#include "Dagger.h"

Dagger::Dagger(): Melee() {}

Dagger::Dagger(std::string name): Melee(name) {}

Dagger::Dagger(std::string name, float dmg): Melee(name, dmg) {}

Dagger::Dagger(std::string name, float dmg, uint32_t durability): Melee(name, dmg, durability) {}

Dagger::Dagger(std::string name, float dmg, BaseStats stats): Melee(name, dmg, stats) {}

Dagger::Dagger(std::string name, float dmg, uint32_t durability, BaseStats stats): Melee(name, dmg, durability, stats) {}

Dagger::Dagger(std::string name, float dmg, float crit, uint32_t durability): Melee(name, dmg, crit, durability) {}

Dagger::Dagger(std::string name, float dmg, float crit, uint32_t durability, BaseStats stats): Melee(name, dmg, crit, durability, stats) {}

Dagger::Dagger(const Dagger& dagger): Melee(dagger) {}

Dagger::~Dagger()
{
}

float Dagger::Use(CharacterInstance& instance)
{
	uint32_t lost = DAGGER_DURABILITY_USAGE;
	_durability -= lost;
	return static_cast<float>(lost);
}

Weapon* Dagger::Copy() const
{
	return new Dagger(*this);
}
