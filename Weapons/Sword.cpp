/**
	\file Sword.cpp
	\brief Sword declaration
	\author Garnier Cl�ment
*/

#include "Sword.h"

Sword::Sword(): Melee() {}

Sword::Sword(std::string name): Melee(name) {}

Sword::Sword(std::string name, float dmg): Melee(name, dmg) {}

Sword::Sword(std::string name, float dmg, uint32_t durability): Melee(name, dmg, durability) {}

Sword::Sword(std::string name, float dmg, BaseStats stats): Melee(name, dmg, stats) {}

Sword::Sword(std::string name, float dmg, uint32_t durability, BaseStats stats): Melee(name, dmg, durability, stats) {}

Sword::Sword(std::string name, float dmg, float crit, uint32_t durability): Melee(name, dmg, crit, durability) {}

Sword::Sword(std::string name, float dmg, float crit, uint32_t durability, BaseStats stats): Melee(name, dmg, crit, durability, stats) {}

Sword::Sword(const Sword& sword): Melee(sword) {}

Sword::~Sword() {}

float Sword::Use(CharacterInstance& instance)
{
	uint32_t lost = SWORD_DURABILITY_USAGE;
	_durability -= lost;
	return static_cast<float>(lost);
}

Weapon* Sword::Copy() const
{
	return new Sword(*this);
}
