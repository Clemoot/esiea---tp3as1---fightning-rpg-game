/**
	\file Weapons.h
	\brief Includes all Weapon-related headers
	\author Garnier Cl�ment
*/

#pragma once

#include "Sword.h"
#include "Dagger.h"
#include "Bow.h"
#include "Staff.h"
