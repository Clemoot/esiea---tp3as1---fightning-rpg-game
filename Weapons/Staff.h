/**
	\file Staff.h
	\brief Staff declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef RANGER_WEAPON_INCLUDED
/// Staff.h included
#define RANGER_WEAPON_INCLUDED

#include "Ranged.h"

/**
	\class Staff
	\brief Staff weapon
*/
class JEU_DE_COMBAT_API Staff : public Ranged
{
public:
	/// Sword default constructor
	Staff();
	/**
		\brief Staff constructor
		\param name Name of the weapon
	*/
	Staff(std::string name);
	/**
		\brief Staff constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
	*/
	Staff(std::string name, float dmg);
	/**
		\brief Staff constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param manaCost Cost of an attack with the weapon
	*/
	Staff(std::string name, float dmg, float manaCost);
	/**
		\brief Staff constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param stats Stats granted to the owner
	*/
	Staff(std::string name, float dmg, BaseStats stats);
	/**
		\brief Staff constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param manaCost Cost of an attack with the weapon
		\param stats Stats granted to the owner
	*/
	Staff(std::string name, float dmg, float manaCost, BaseStats stats);
	/**
		\brief Staff constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param manaCost Cost of an attack with the weapon
		\param crit Critical rate of the weapon
		\param stats Stats granted to the owner
	*/
	Staff(std::string name, float dmg, float manaCost, float crit, BaseStats stats);
	/**
		\brief Staff copy constructor
		\param other Staff to copy
	*/
	Staff(const Staff& other);
	/// Staff destructor
	~Staff();

	float ManaCost() const;			///< Mana cost getter
	void ManaCost(float manaCost);	///< Mana cost setter

	/// Checks if the staff can be used
	bool Usable(CharacterInstance& instance) const;
	/// Uses the staff
	float Use(CharacterInstance& instance);

	/// Copy the staff
	Weapon* Copy() const;

private:
	float _manaCost;		///< Mana cost of an attack
};

#endif