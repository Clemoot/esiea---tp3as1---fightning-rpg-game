/**
	\file Bow.cpp
	\brief Bow declaration
	\author Garnier Cl�ment
*/

#include "Bow.h"

Bow::Bow() : Ranged(), _nbArrows(0) {}

Bow::Bow(std::string name) : Ranged(name), _nbArrows(0) {}

Bow::Bow(std::string name, float dmg): Ranged(name, dmg), _nbArrows(0) {}

Bow::Bow(std::string name, float dmg, uint32_t nbArrows) : Ranged(name, dmg), _nbArrows(nbArrows) {}

Bow::Bow(std::string name, float dmg, BaseStats stats): Ranged(name, dmg, stats), _nbArrows(0) {}

Bow::Bow(std::string name, float dmg, uint32_t nbArrows, BaseStats stats): Ranged(name, dmg, stats), _nbArrows(nbArrows) {}

Bow::Bow(std::string name, float dmg, uint32_t nbArrows, float crit, BaseStats stats): Ranged(name, dmg, crit, stats), _nbArrows(nbArrows) {}

Bow::Bow(const Bow& other) : Ranged(other), _nbArrows(other._nbArrows) {}

Bow::~Bow() {}

uint32_t Bow::NbArrows() const
{
	return _nbArrows;
}

void Bow::NbArrows(uint32_t nbArrows)
{
	_nbArrows = nbArrows;
}

bool Bow::Usable(CharacterInstance& instance) const
{
	return _nbArrows > 0;
}

float Bow::Use(CharacterInstance& instance)
{
	_nbArrows -= ARROW_USAGE;
	return ARROW_USAGE;
}

Weapon* Bow::Copy() const
{
	return new Bow(*this);
}
