/**
	\file Dagger.h
	\brief Dagger declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef DAGGER_WEAPON_INCLUDED
/// Dagger.h included
#define DAGGER_WEAPON_INCLUDED

#include "Melee.h"

/// Durability usage
#define DAGGER_DURABILITY_USAGE 1

/**
	\class Dagger
	\brief Dagger weapon
*/
class JEU_DE_COMBAT_API Dagger : public Melee
{
public:
	/// Sword default constructor
	Dagger();
	/**
		\brief Dagger constructor
		\param name Name of the weapon
	*/
	Dagger(std::string name);
	/**
		\brief Dagger constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
	*/
	Dagger(std::string name, float dmg);
	/**
		\brief Dagger constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param durability Durability of the weapon
	*/
	Dagger(std::string name, float dmg, uint32_t durability);
	/**
		\brief Dagger constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param stats Stats granted to the owner
	*/
	Dagger(std::string name, float dmg, BaseStats stats);
	/**
		\brief Dagger constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param durability Durability of the weapon
		\param stats Stats granted to the owner
	*/
	Dagger(std::string name, float dmg, uint32_t durability, BaseStats stats);
	/**
		\brief Dagger constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
		\param durability Durability of the weapon
	*/
	Dagger(std::string name, float dmg, float crit, uint32_t durability);
	/**
		\brief Dagger constructor
		\param name Name of the weapon
		\param dmg Damage of the weapon
		\param crit Critical rate of the weapon
		\param durability Durability of the weapon
		\param stats Stats granted to the owner
	*/
	Dagger(std::string name, float dmg, float crit, uint32_t durability, BaseStats stats);
	/**
		\brief Dagger copy constructor
		\param dagger Dagger to copy
	*/
	Dagger(const Dagger& dagger);
	/// Dagger destructor
	~Dagger();

	/// Uses the dagger
	float Use(CharacterInstance& instance);

	/// Copy the dagger
	Weapon* Copy() const;

private:

};

#endif