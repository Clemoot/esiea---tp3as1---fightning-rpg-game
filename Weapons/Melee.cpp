/**
	\file Melee.cpp
	\brief Melee declaration
	\author Garnier Cl�ment
*/

#include "Melee.h"

Melee::Melee(): Weapon(), _durability(0) {}

Melee::Melee(std::string name): Weapon(name), _durability(0) {}

Melee::Melee(std::string name, float dmg): Weapon(name, dmg), _durability(0) {}

Melee::Melee(std::string name, float dmg, uint32_t durability): Weapon(name, dmg), _durability(durability) {}

Melee::Melee(std::string name, float dmg, BaseStats stats) : Weapon(name, dmg, stats), _durability(0)	{}

Melee::Melee(std::string name, float dmg, uint32_t durability, BaseStats stats): Weapon(name, dmg, stats), _durability(durability) {}

Melee::Melee(std::string name, float dmg, float crit, uint32_t durability): Weapon(name, dmg, crit), _durability(durability) {}

Melee::Melee(std::string name, float dmg, float crit, uint32_t durability, BaseStats stats): Weapon(name, dmg, crit, stats), _durability(durability) {}

Melee::Melee(const Melee& melee): Weapon(melee), _durability(melee._durability)  {}

Melee::~Melee()
{
}

float Melee::Damage() const
{
	if (Broken())	return _computedStats.Damage() * .1f;
	return _computedStats.Damage();
}

uint32_t Melee::Durability() const
{
	return _durability;
}

void Melee::Durability(uint32_t durability)
{
	_durability = durability;
}

bool Melee::Repair(int durability)
{
	_durability += durability;
	return true;
}

bool Melee::Usable(CharacterInstance& instance) const
{
	return !Broken();
}

bool Melee::Broken() const
{
	return _durability <= 0;
}
