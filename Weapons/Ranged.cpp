/**
	\file Ranged.cpp
	\brief Ranged declaration
	\author Garnier Cl�ment
*/

#include "Ranged.h"

Ranged::Ranged(): Weapon() {}

Ranged::Ranged(std::string name): Weapon(name) {}

Ranged::Ranged(std::string name, float dmg): Weapon(name, dmg) {}

Ranged::Ranged(std::string name, float dmg, float crit): Weapon(name, dmg, crit) {}

Ranged::Ranged(std::string name, float dmg, BaseStats stats): Weapon(name, dmg, stats) {}

Ranged::Ranged(std::string name, float dmg, float crit, BaseStats stats): Weapon(name, dmg, crit, stats) {}

Ranged::Ranged(const Ranged& ranged): Weapon(ranged) {}

Ranged::~Ranged() {}