/**
	\file CharacterInstance.cpp
	\brief CharacterInstance definition
	\author Garnier Cl�ment
*/

#include "CharacterInstance.h"

#include <algorithm>

#include "FightInstance.h"

#include "../Utils/PointerDeleter.h"

#include "../Skills/Skill.h"

#include "../Auras/TargetAlterationAura.h"
#include "../Auras/OnTurnBeginAura.h"
#include "../Auras/CharacterStatsAura.h"
#include "../Auras/FadeOnAttackAura.h"

#include "../Characters/Warrior.h"
#include "../Weapons/Sword.h"

#include "../Stats/SpecificMasks.h"

CharacterInstance::CharacterInstance(Character& c) :
	_character(c),
	_stats(c.Stats()),
	_computedStats(_stats),
	_resources(c.Stats().Resources()),
	_weapon(nullptr),
	_others{ nullptr }
{
	std::vector<CharacterAura*> auras = c.Passives();
	std::for_each(auras.begin(), auras.end(), [&](CharacterAura* a) {
		if (!a)	return;
		_auras.push_back(a->CopyOn(this));
	});

	std::vector<Skill*> skills = c.Skills();
	std::for_each(skills.begin(), skills.end(), [&](Skill* s) {
		if (!s)	return;
		_skills.push_back(s->Copy());
	});

	if (c.EquipedWeapon())
		_weapon = c.EquipedWeapon()->Copy();

	__ComputeStats();
}

CharacterInstance::CharacterInstance(const CharacterInstance& other):
	_character(other._character),
	_stats(other._stats),
	_computedStats(other._computedStats),
	_resources(other._resources),
	_others{ other._others[PREVIOUS], other._others[NEXT], other._others[OPPONENT] },
	_weapon(nullptr)
{
	std::for_each(other._auras.begin(), other._auras.end(), [&](CharacterAura* a) {
		if (!a)	return;
		_auras.push_back(a->CopyOn(this));
	});

	std::for_each(other._skills.begin(), other._skills.end(), [&](Skill* s) {
		if (!s)	return;
		_skills.push_back(s->Copy());
	});

	if (other._weapon)	_weapon = other._weapon->Copy();
}

CharacterInstance::~CharacterInstance()
{
	{
		auto remove = std::remove_if(_auras.begin(), _auras.end(), PointerDeleter<CharacterAura>([&](CharacterAura*& a) {		return true;	}));
		if (remove != _auras.end())
			_auras.erase(remove);
	}

	{
		auto remove = std::remove_if(_skills.begin(), _skills.end(), PointerDeleter<Skill>([&](Skill*& a) {		return true;	}));
		if (remove != _skills.end())
			_skills.erase(remove);
	}

	if (_weapon) {
		delete _weapon;
		_weapon = nullptr;
	}
}

void CharacterInstance::Allies(CharacterInstance* previous, CharacterInstance* next)
{
	_others[PREVIOUS] = previous;
	_others[NEXT] = next;
}

void CharacterInstance::Opponent(CharacterInstance* opponent)
{
	_others[OPPONENT] = opponent;
}

CharacterInstance* CharacterInstance::LeftAlly()
{
	if (_others[PREVIOUS]->Dead())
		return nullptr;
	return _others[PREVIOUS];
}

CharacterInstance* CharacterInstance::RightAlly()
{
	if (_others[NEXT]->Dead())
		return nullptr;
	return _others[NEXT];
}

CharacterInstance* CharacterInstance::FrontOpponent()
{
	if (_others[OPPONENT]->Dead())
		return nullptr;
	return _others[OPPONENT];
}

std::vector<CharacterInstance*> CharacterInstance::Allies()
{
	std::vector<CharacterInstance*> allies;
	std::vector<CharacterInstance*> leftAllies, rightAllies;
	__AlliesOnLeft(leftAllies);
	__AlliesOnRight(rightAllies);
	allies.insert(allies.end(), leftAllies.begin(), leftAllies.end());
	allies.insert(allies.end(), rightAllies.begin(), rightAllies.end());
	return allies;
}

std::vector<CharacterInstance*> CharacterInstance::Opponents()
{
	if (!_others[OPPONENT])
		return std::vector<CharacterInstance*>();
	auto opponents = _others[OPPONENT]->Allies();
	opponents.push_back(_others[OPPONENT]);
	return opponents;
}

std::vector<CharacterInstance*> CharacterInstance::Others()
{
	auto opponents = Opponents();
	auto allies = Allies();

	std::vector<CharacterInstance*> res;
	res.insert(res.end(), opponents.begin(), opponents.end());
	res.insert(res.end(), allies.begin(), allies.end());
	return res;
}

std::vector<CharacterInstance*> CharacterInstance::All()
{
	auto res = Others();
	res.insert(res.end(), this);
	return res;
}

CharacterInstance* CharacterInstance::Self()
{
	return this;
}

bool CharacterInstance::IsAlly(CharacterInstance* character)
{
	std::vector<CharacterInstance*> allies = Allies();
	if (std::find(allies.begin(), allies.end(), character) == allies.end())
		return false;
	return true;
}

bool CharacterInstance::IsOpponent(CharacterInstance* character)
{
	std::vector<CharacterInstance*> opponents = Opponents();
	if (std::find(opponents.begin(), opponents.end(), character) == opponents.end())
		return false;
	return true;
}

Character& CharacterInstance::InstantiatedCharacter() const
{
	return _character;
}

BaseStats CharacterInstance::OriginalStats() const
{
	return _stats;
}

BaseStats CharacterInstance::Stats() const
{
	return _computedStats;
}

void CharacterInstance::Stats(BaseStats stats)
{
	_computedStats = stats;
}

ResourceStats CharacterInstance::Resources() const
{
	return _resources;
}

void CharacterInstance::Resources(ResourceStats rs)
{
	_resources = rs;
}

std::vector<CharacterAura*> CharacterInstance::Auras()
{
	return _auras;
}

Weapon* CharacterInstance::EquipedWeapon()
{
	return _weapon;
}

bool CharacterInstance::AddAura(CharacterAura* aura)
{
	if (!aura)	return false;
	auto remove = std::remove_if(_auras.begin(), _auras.end(), PointerDeleter<CharacterAura>([&](CharacterAura*& a) {
		if (!a)	return true;
		return a->Name() == aura->Name();
	}));
	if (remove != _auras.end())
		_auras.erase(remove);
	_auras.push_back(aura);
	if (Aura::Is<CharacterStatsAura>(*aura)) {
		__ComputeStats();
	}
	return true;
}

bool CharacterInstance::RemoveAura(CharacterAura* aura)
{
	bool isStatAura = (aura) ? Aura::Is<CharacterStatsAura>(*aura) : false;
	auto pos = std::remove_if(_auras.begin(), _auras.end(), PointerDeleter<CharacterAura>([&](CharacterAura*& a) {
		return aura == a;
	}));
	if (pos == _auras.end())
		return false;
	_auras.erase(pos);
	if (isStatAura)	__ComputeStats();
	return true;
}

void CharacterInstance::Regenerate(float amount)
{
	if (amount <= 0)	return;
	_resources.Mana(_resources.Mana() + amount);
	if (_resources.Mana() > _computedStats.Mana())
		_resources.Mana(_computedStats.Mana());
}

void CharacterInstance::Consume(float amount)
{
	if (amount <= 0)	return;
	_resources.Mana(_resources.Mana() - amount);
	if (_resources.Mana() < .0f)
		_resources.Mana(.0f);
}

void CharacterInstance::Heal(float amount)
{
	if (Dead())		return;
	if (amount <= 0)		return;
	_resources.Life(_resources.Life() + amount);
	if (_resources.Life() > _computedStats.Life())
		_resources.Life(_computedStats.Life());
}

void CharacterInstance::Hit(float damage)
{
	if (Dead())		return;
	if (damage <= 0)	return;

	_resources.Life(_resources.Life() - damage);
	if (_resources.Life() < 0) {
		_resources.Life(.0f);
		std::cout << _character.Name() << " is dead" << std::endl;
	}
}

bool CharacterInstance::CanPlay() const
{
	return !Dead();
}

std::vector<TurnOption> CharacterInstance::TurnOptions()
{
	std::vector<TurnOption> options;
	std::vector<CharacterInstance*> targets;
	std::for_each(_skills.begin(), _skills.end(), [&](Skill* skill) {
		if (!skill)	return;

		targets = skill->FindTargetsFrom(*this);
		auto characters = All();
		std::for_each(characters.begin(), characters.end(), [&](CharacterInstance* c) {
			if (!c)	return;

			auto auras = c->Auras();
			std::for_each(auras.begin(), auras.end(), [&](CharacterAura* a) {
				if (!a)	return;
				if (!a->Instantiated()) return;
				try {
					TargetAlterationAura& cast = dynamic_cast<TargetAlterationAura&>(*a);
					cast.Alterate(this, targets);
				}
				catch (const std::bad_cast&) {}
			});
		});
		

		options.push_back(TurnOption{ *skill, targets });
	});


	return options;
}

void CharacterInstance::BeginTurn()
{
	std::for_each(_skills.begin(), _skills.end(), [](Skill* skill) {
		if (!skill)	return;
		skill->ReduceCooldown();
	});

	auto remove = std::remove(_auras.begin(), _auras.end(), nullptr);
	if (remove != _auras.end())
		_auras.erase(remove);

	std::for_each(_auras.begin(), _auras.end(), [&](CharacterAura* a) {
		if (!a)	return;
		if (!a->Instantiated()) return;

		try {
			OnTurnBeginAura& cast = dynamic_cast<OnTurnBeginAura&>(*a);
			cast.Proc();
		}
		catch (const std::bad_cast&) {}
		
		a->ReduceDuration();
	});

	remove = std::remove_if(_auras.begin(), _auras.end(), PointerDeleter<CharacterAura>([](CharacterAura* a) {
		if (!a)	return true;
		return a->TimedOut();
	}));
	if (remove != _auras.end())
		_auras.erase(remove);
}

void CharacterInstance::Attacked()
{
	auto remove = std::remove_if(_auras.begin(), _auras.end(), PointerDeleter<CharacterAura>([](CharacterAura* a) {
		if (!a)	return true;
		return Aura::Is<FadeOnAttackAura>(*a);
	}));
	if (remove != _auras.end())
		_auras.erase(remove);
	__ComputeStats();
}

bool CharacterInstance::Dead() const
{
	return _resources.Life() <= 0;
}

void CharacterInstance::__AlliesOnLeft(std::vector<CharacterInstance*>& allies) const
{
	if (!_others[PREVIOUS])
		return;

	allies.push_back(_others[PREVIOUS]);
	_others[PREVIOUS]->__AlliesOnLeft(allies);
}

void CharacterInstance::__AlliesOnRight(std::vector<CharacterInstance*>& allies) const
{
	if (!_others[NEXT])
		return;

	allies.push_back(_others[NEXT]);
	_others[NEXT]->__AlliesOnRight(allies);
}

void CharacterInstance::__ComputeStats()
{
	BaseStats newStats = _stats;
	std::vector<CharacterStatsAura> sAuras;

	if (_weapon)
		newStats += _weapon->Stats().BonusStats();

	std::for_each(_auras.begin(), _auras.end(), [&](CharacterAura*& a) {
		if (!a)	return;
		try {
			CharacterStatsAura& sAura = dynamic_cast<CharacterStatsAura&>(*a);
			newStats += sAura.FlatStats();
			sAuras.push_back(sAura);
		} catch(const std::bad_cast&) {}
	});

	std::for_each(sAuras.begin(), sAuras.end(), [&](CharacterStatsAura& a) {
		newStats *= a.Multiplier();
	});

	auto percentageResources = _resources / _computedStats.Resources();
	_computedStats = newStats;
	_resources = _computedStats.Resources() * percentageResources;
	__CheckIntegrity();
}

void CharacterInstance::__CheckIntegrity()
{
	if (_resources.Life() > _computedStats.Life())
		_resources.Life(_computedStats.Life());
	if (_resources.Mana() > _computedStats.Mana())
		_resources.Mana(_computedStats.Mana());
}
