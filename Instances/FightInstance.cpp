/**
	\file FightInstance.cpp
	\brief FightInstance, TurnOption, TurnAction, Turn, FightState, TurnOrganizer definitions
	\author Garnier Cl�ment
*/

#include "FightInstance.h"

#include <algorithm>

#include "../Auras/OnTurnBeginAura.h"
#include "../Auras/OnCastAura.h"

#include "../Skills/DodgeableSkill.h"
#include "../Skills/ParriableSkill.h"

#include "../Utils/Exceptions.h"


/// Ckecks if an index not out of range
#define OUT_OF_BOUNDS(index) index < 0 || index >= CHARACTERS_PER_TEAM

FightInstance::FightInstance(Team t[TEAMS_PER_FIGHT], std::function<TurnAction*(CharacterInstance&, TurnOptions)> onTurnChanged): 
	_teams{ nullptr },
	_onTurnChanged(onTurnChanged),
	_turnCount(0)
{
	uint8_t i, j;
	
	for (i = 0; i < TEAMS_PER_FIGHT; i++) 
		for (j = 0; j < CHARACTERS_PER_TEAM; j++)
			if(t[i][j])
				_teams[i][j] = new CharacterInstance(*t[i][j]);
	
	CharacterInstance* previous = nullptr, * next = nullptr;
	for (i = 0; i < TEAMS_PER_FIGHT; i++) 
		for (j = 0; j < CHARACTERS_PER_TEAM; j++) {
			if (!_teams[i][j])
				continue;
			// Links allies (left and right)
			previous = (OUT_OF_BOUNDS(j - 1)) ? nullptr : _teams[i][j - 1];
			next = (OUT_OF_BOUNDS(j + 1)) ? nullptr : _teams[i][j + 1];
			_teams[i][j]->Allies(previous, next);

			// Links front opponent
			_teams[i][j]->Opponent(_teams[OPPONENTS(i)][j]);
		}

	for (i = 0; i < TEAMS_PER_FIGHT; i++)
		for (j = 0; j < CHARACTERS_PER_TEAM; j++)
			_organizer.AddCharacterInstance(_teams[i][j]);
	_organizer.Lock();
}

#undef OUT_OF_BOUNDS

FightInstance::~FightInstance()
{
	uint8_t i, j;
	for (i = 0; i < TEAMS_PER_FIGHT; i++)
		for (j = 0; j < CHARACTERS_PER_TEAM; j++)
			delete _teams[i][j];
}

Turn* FightInstance::NextTurn()
{
	Turn* turn = nullptr;	// coming turn
	if (_pendingTurn) {		// If a turn has not been played yet, ...
		turn = _pendingTurn;	// coming turn is the pending turn
	} 
	else {		// If no turn is pending, ...
		do {
			// Who plays ?
			CharacterInstance* cInstance = _organizer.Next();
			if (!cInstance)
				return nullptr;

			cInstance->BeginTurn();

			if (!cInstance->CanPlay()) {
				continue;
			}

			// What can he do ?
			TurnOptions options = cInstance->TurnOptions();

			// Next turn
			turn = new Turn{ *cInstance, options };

			// Character begins its turn
		} while (turn == nullptr);
	}

	_pendingTurn = turn;	// Wait action for this turn

	if (_onTurnChanged) {	// If a callback function is specified, ...
		// Call callback function and get the action
		TurnAction* action = _onTurnChanged(turn->character, turn->options);
		if (action) {	// If an action is retrieved, ...
			PlayTurn(*action);	// Play the turn with this action
			delete action;
		}
	} else {	// If no callback function specified, ...
		// Turn information are returned and wait to play turn
		return _pendingTurn;
	}
	return nullptr;
}

void FightInstance::PlayTurn(CharacterInstance& target, Skill& skill)
{
	if (!_pendingTurn)	// If no turn is pending, do nothing
		return;

	while (true) {
		try {
			float roll = static_cast<float>(rand()) / RAND_MAX;

			if (Skill::Is<DodgeableSkill>(skill)) {
				DodgeableSkill& dSkill = dynamic_cast<DodgeableSkill&>(skill);
				if (dSkill.IsDodged(target, roll)) {
					std::cout << target.InstantiatedCharacter().Name() << " dodges the attack" << std::endl;
					break;
				}
			}

			if (Skill::Is<ParriableSkill>(skill)) {
				ParriableSkill& dSkill = dynamic_cast<ParriableSkill&>(skill);
				if (dSkill.IsParried(target, roll)) {
					std::cout << target.InstantiatedCharacter().Name() << " parries the attack" << std::endl;
					break;
				}
			}

			skill.Cast(_pendingTurn->character, target);
			__OnCast(_pendingTurn->character, target, skill);

			if (_pendingTurn->character.IsOpponent(&target))
				_pendingTurn->character.Attacked();
		}
		catch (const Exception & e) {
			std::cerr << "Error: " << e << std::endl;
		}
		break;
	}
	// Turn has been played
	_turnCount++;
	delete _pendingTurn;
	_pendingTurn = nullptr;
}

void FightInstance::PlayTurn(const TurnAction& action)
{
	PlayTurn(action.target, action.skill);
}

bool FightInstance::Finished(uint8_t* indexWinner) const
{
	struct TeamState {
		uint8_t index;
		uint8_t alive;

		bool operator<(const TeamState& other) {
			return alive < other.alive;
		}
	};
	std::vector<TeamState> states;
	uint8_t i, j, aliveTeams = 0;

	// Counts dead characters in each team
	TeamState tmpState = { 0 };
	for (i = 0; i < TEAMS_PER_FIGHT; i++) {
		tmpState.index = i;
		for (j = 0; j < CHARACTERS_PER_TEAM; j++)
			if (_teams[i][j] && !_teams[i][j]->Dead())
				tmpState.alive++;
		states.push_back(tmpState);
		if (tmpState.alive > 0)
			aliveTeams++;
	}

	if (aliveTeams > 1) {
		if (indexWinner)	*indexWinner = UINT8_MAX;
		return false;
	}
	else if (aliveTeams == 1) {
		TeamState winnerTeam = *std::find_if(states.begin(), states.end(), [](TeamState ts) { return ts.alive > 0; });
		if (indexWinner)	*indexWinner = winnerTeam.index;
		return true;
	}
	else if (aliveTeams == 0) {
		if (indexWinner)	*indexWinner = (uint8_t)states.size();
		return true;
	}
	return true;
}

std::shared_ptr<FightState> FightInstance::GetState() const
{
	auto res = std::make_shared<FightState>();

	uint8_t i, j;
	for(i = 0; i < TEAMS_PER_FIGHT; i++)
		for (j = 0; j < CHARACTERS_PER_TEAM; j++) {
			CharacterInstance* tmp = _teams[i][j];
			if (!tmp)	continue;

			res->teams[i][j] = new FightState::CharacterState{
				*tmp,
				_organizer.GetPos(tmp)
			};
		}

	res->currentTurn = _turnCount;
	return res;
}

void FightInstance::__OnCast(CharacterInstance& cInstance, CharacterInstance& target, Skill& skill)
{
	auto auras = cInstance.Auras();
	std::for_each(auras.begin(), auras.end(), [&](CharacterAura*& a) {
		if (!a)	return;
		if (!a->Instantiated()) return;
		try {
			OnCastAura& cast = dynamic_cast<OnCastAura&>(*a);
			cast.OnCast(target, skill);
		} catch(const std::bad_cast&) {}
	});
}

FightInstance::TurnOrganizer::TurnOrganizer(): _locked(false)
{}

FightInstance::TurnOrganizer::~TurnOrganizer()
{
}

void FightInstance::TurnOrganizer::Lock()
{
	_locked = true;
	std::random_shuffle(_turns.begin(), _turns.end());
	std::sort(_turns.begin(), _turns.end(), [](WaitingTurn a, WaitingTurn b) {
		return a.turnOffset < b.turnOffset;
	});
}

void FightInstance::TurnOrganizer::Unlock()
{
	_locked = false;
}

void FightInstance::TurnOrganizer::AddCharacterInstance(CharacterInstance* instance)
{
	if (_locked)
		return;
	if (!instance)
		return;

	WaitingTurn tmpTurn;
	tmpTurn.character = instance;
	tmpTurn.turnOffset = instance->Stats().Speed();
	_turns.push_back(tmpTurn);
}

CharacterInstance* FightInstance::TurnOrganizer::Next()
{
	WaitingTurn next = { 0 };
	do {
		next = _turns.front();
		_turns.erase(_turns.begin());
	} while (!next.character->CanPlay());

	std::transform(_turns.begin(), _turns.end(), _turns.begin(), [&](WaitingTurn t) {
		t.turnOffset -= next.character->Stats().Speed();
		return t;
	});

	auto newpos = std::upper_bound(_turns.begin(), _turns.end(), next, [](WaitingTurn a, WaitingTurn b) {
		return a.turnOffset < b.turnOffset;
	});

	_turns.insert(newpos, next);

	return next.character;
}

uint8_t FightInstance::TurnOrganizer::GetPos(CharacterInstance* instance) const
{
	auto pos = std::find(_turns.begin(), _turns.end(), WaitingTurn{ instance });
	if (pos == _turns.end())	return UINT8_MAX;
	return std::distance(_turns.begin(), pos);
}

FightState::~FightState()
{
	uint8_t i, j;
	for(i = 0; i < TEAMS_PER_FIGHT; i++)
		for(j = 0; j < CHARACTERS_PER_TEAM; j++)
			if (teams[i][j]) {
				delete teams[i][j];
				teams[i][j] = nullptr;
			}
}