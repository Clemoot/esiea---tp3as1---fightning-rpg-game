/**
	\file FightInstance.h
	\brief FightInstance, TurnOption, TurnAction, Turn, FightState, TurnOrganizer declarations
	\author Garnier Cl�ment
*/

#pragma once

#ifndef FIGHT_INSTANCE_INCLUDED
/// FightInstance.h included
#define FIGHT_INSTANCE_INCLUDED

#include <deque>
#include <functional>

#include "../Skills/Skill.h"
#include "CharacterInstance.h"

#define OPPONENTS(team)	1 - team	///< Gives the index of opponent team

/**
	\struct TurnOption
	\brief Option a player can do for its turn
*/
struct JEU_DE_COMBAT_API TurnOption {
	Skill& skill;			///< Skill
	std::vector<CharacterInstance*> targets;	///< Possible targets for the skill
};
typedef std::vector<TurnOption> TurnOptions;	///< Many TurnOption

/**
	\struct TurnAction
	\brief Action of the turn
*/
struct JEU_DE_COMBAT_API TurnAction {
	CharacterInstance& target;	///< Target of the action
	Skill& skill;				///< Skill to cast
};

/**
	\struct Turn
	\brief Informations about the current turn
*/
struct JEU_DE_COMBAT_API Turn {
	CharacterInstance& character;	///< Playing instance
	TurnOptions options;			///< Turn options
};

/**
	\struct FightState
	\brief Informations about the current fight
*/
struct JEU_DE_COMBAT_API FightState {
	~FightState();

	/**
		\struct CharacterState
		\brief Informations about an instance and its position in turn queue
	*/
	struct CharacterState {
		CharacterInstance instance;	///< Instance
		uint8_t turnOffset;			///< Time before play
	}* teams[TEAMS_PER_FIGHT][CHARACTERS_PER_TEAM];	///< Teams
	uint16_t currentTurn;	///< Turn index
};

/**
	\class FightInstance
	\brief Manages a fight between 2 teams
*/
class JEU_DE_COMBAT_API FightInstance
{
public:
	/**
		\brief FightInstance constructor
		\param t Teams
		\param onTurnChanged Callback when a turn begins
	*/
	FightInstance(Team t[TEAMS_PER_FIGHT], std::function<TurnAction*(CharacterInstance&, TurnOptions)> onTurnChanged);
	/// FightInstance destructor
	~FightInstance();

	Turn* NextTurn();	///< Go to next turn

	/// Plays a turn
	void PlayTurn(CharacterInstance& target, Skill& skill);
	/// Plays a turn
	void PlayTurn(const TurnAction& action);

	/// Checks if the fight is finished
	bool Finished(uint8_t* indexWinner = nullptr) const;

	/// Retrieve the state of the fight
	std::shared_ptr<FightState> GetState() const;

private:
	ITeam _teams[TEAMS_PER_FIGHT];	///< Instantiated teams
	std::function<TurnAction*(CharacterInstance&, TurnOptions)> _onTurnChanged;	///< Callback when turn begins
	Turn* _pendingTurn;	///< Turn informations about the current turn
	uint16_t _turnCount;	///< Turn counter

	/**
		\class TurnOrganizer
		\brief Organizes and defines the queue for turns
	*/
	class TurnOrganizer {
	public:
		/// TurnOrganizer default constructor
		TurnOrganizer();
		// TurnOrganizer desctructor
		~TurnOrganizer();

		/**
			\brief Locks the turn queue
			Once called, no more instance can be added to the queue
		*/
		void Lock();
		/// Unlocks the turn queue
		void Unlock();

		/// Adds an instance to the queue
		void AddCharacterInstance(CharacterInstance* instance);

		CharacterInstance* Next();	/// Retrieve next instance to play

		/// Retrieve the position of an instance turn
		uint8_t GetPos(CharacterInstance* instance) const;

	private:
		/**
			\struct WaitingTurn
			\brief Offset before a instance can play
		*/
		struct WaitingTurn {
			CharacterInstance* character;	///< Waiting instance
			float turnOffset;				///< Offset before play

			bool operator==(const WaitingTurn& other) const {
				return character == other.character;
			}
		};
		bool _locked;	///< Is queue locked
		std::vector<WaitingTurn> _turns;	///< Turn queue
	} _organizer;

	/// Calls all OnCastAura of teh caster
	void __OnCast(CharacterInstance& cInstance, CharacterInstance& target, Skill& skill);
};

#endif