/**
	\file CharacterInstance.h
	\brief CharacterInstance declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef CHARACTER_INSTANCE_INCLUDED
/// CharacterInstance.h included
#define CHARACTER_INSTANCE_INCLUDED

#include "../Rules.h"

#include <iostream>
#include <vector>

#include "../Characters/Character.h"
#include "../Stats/BaseStats.h"

/// Previous ally index
#define PREVIOUS 0
/// Next ally index
#define NEXT 1
/// Front opponent index
#define OPPONENT 2
/// Number of instance neighbours
#define NB_OTHERS 3

struct TurnOption;	// Forward declaration to avoid circular dependencies

/**
	\class CharacterInstance
	\brief Instance of a character

	The instance of a character can do some actions and is created for fighting
*/
class JEU_DE_COMBAT_API CharacterInstance {
public:
	/**
		\brief CharacterInstance constructor
		\param c Character reference
	*/
	CharacterInstance(Character& c);
	/**
		\brief CharacterInstance copy constructor
		\param other CharacterInstance to copy
	*/
	CharacterInstance(const CharacterInstance& other);
	/// CharacterInstance destructor
	~CharacterInstance();

	/// Set the allies of the instance
	void Allies(CharacterInstance* left, CharacterInstance* right);
	/// Set the front opponent
	void Opponent(CharacterInstance* opponent);

	CharacterInstance* LeftAlly();			///< Left ally getter
	CharacterInstance* RightAlly();			///< Right ally getter
	CharacterInstance* FrontOpponent();		///< Front opponent getter
	std::vector<CharacterInstance*> Allies();	///< Allies getter
	std::vector<CharacterInstance*> Opponents();	///< Opponents getter
	std::vector<CharacterInstance*> Others();		///< Gets all instances except himself
	std::vector<CharacterInstance*> All();		///< Gets all instances
	CharacterInstance* Self();				///< Himself getter

	bool IsAlly(CharacterInstance* character);			///< Checks if an instance is an ally instance
	bool IsOpponent(CharacterInstance* character);		///< Checks if an instance is an opponent instance

	Character& InstantiatedCharacter() const;		///< Instantiated character getter

	BaseStats OriginalStats() const;		///< Base stats of the instance
	BaseStats Stats() const;				///< Computed stats of the instance (Base stats + Auras)
	void Stats(BaseStats stats);			///< Base stats setter
	ResourceStats Resources() const;		///< Current resources getter
	void Resources(ResourceStats rs);		///< Current resources setter
	std::vector<CharacterAura*> Auras();	///< Auras getter
	Weapon* EquipedWeapon();				///< Equiped weapon getter

	bool AddAura(CharacterAura* aura);		///< Adds an aura to an instance
	bool RemoveAura(CharacterAura* aura);	///< Removes an aura from an instance
	template<class T>
	bool HasAura();							///< Checks if the instance already has an specific aura 

	void Regenerate(float amount);			///< Regenerates resources
	void Consume(float amount);				///< Consumes resources
	void Heal(float amount);				///< Heal the instance
	void Hit(float damage);					///< Deal the instance

	bool CanPlay() const;					///< Checks if the instance can play
	std::vector<TurnOption> TurnOptions();	///< Generate all turn options the player can do
	void BeginTurn();						///< Triggers effects on turn beginning
	void Attacked();						///< Call once the character attacked

	bool Dead() const;						///< Checks if the instance is dead

	/// Checks if the instantiated chracter is castable in a child character
	template<class T>
	bool Is() {
		return Character::Is<T>(_character);
	}

protected:
	Character& _character;		///< Instantiated character

	BaseStats _stats;			///< Base stats
	ResourceStats _resources;	///< Current resources (life, mana)
	BaseStats _computedStats;	///< Stats with auras

	std::vector<CharacterAura*> _auras;	///< Owned auras
	std::vector<Skill*> _skills;		///< Skill set

	Weapon* _weapon;		///< Equiped weapon

	CharacterInstance* _others[NB_OTHERS];	///< Left and right allies and front opponent

private:
	/// Retrieves all allies on the left of the instance
	void __AlliesOnLeft(std::vector<CharacterInstance*>& allies) const;
	/// Retrives all allies on the right of the instance
	void __AlliesOnRight(std::vector<CharacterInstance*>& allies) const;
	/// Computes stats with owned auras
	void __ComputeStats();
	/// Checks if resource values are valid
	void __CheckIntegrity();
};

/// Aiies: A instantiated team is 3-instantiated character party
typedef CharacterInstance* ITeam[CHARACTERS_PER_TEAM];

#endif

template<class T>
inline bool CharacterInstance::HasAura()
{
	auto pos = std::find_if(_auras.begin(), _auras.end(), [](CharacterAura* a) {
		if (!a)	return;
		try {
			dynamic_cast<T&>(*a);
			return true;
		}
		catch (const std::bad_cast&) {
			return false;
		}
	});
	return pos != _auras.end();
}
