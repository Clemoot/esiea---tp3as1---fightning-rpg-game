/**
	\file StatsMask.cpp
	\brief StatsMask declaration
	\author Garnier Cl�ment
*/

#include "StatsMask.h"

StatsMask::StatsMask(uint16_t mask): _mask(mask)
{
	if (_mask > W_ALL)
		_mask = W_ALL;
}

StatsMask::StatsMask(const StatsMask& other): _mask(other._mask)
{}

StatsMask::~StatsMask()
{}

uint16_t StatsMask::Mask() const
{
	return _mask;
}

void StatsMask::Mask(uint16_t mask)
{
	_mask = mask;
}

void StatsMask::Apply(ResourceStats& stats)
{
	if (!(_mask & LIFE))	stats._life = .0f;
	if (!(_mask & MANA))	stats._mana = .0f;
}

void StatsMask::Apply(BaseStats & stats)
{
	if (!(_mask & LIFE))	stats._resources._life = .0f;
	if (!(_mask & ATTACK))	stats._attack = .0f;
	if (!(_mask & DEFENSE))	stats._defense = .0f;
	if (!(_mask & DEXTERITY))	stats._dexterity = .0f;
	if (!(_mask & INTELLIGENCE))	stats._intelligence = .0f;
	if (!(_mask & SPEED))	stats._speed = .0f;
	if (!(_mask & DODGE))	stats._dodge = .0f;
	if (!(_mask & PARRY))	stats._parry = .0f;
	if (!(_mask & POISONRATE))	stats._poisonRate = .0f;
	if (!(_mask & MANA))	stats._resources._mana = .0f;
}

void StatsMask::Apply(WeaponStats& stats)
{
	if (!(_mask & LIFE))	stats._bonusStats._resources._life = .0f;
	if (!(_mask & ATTACK))	stats._bonusStats._attack = .0f;
	if (!(_mask & DEFENSE))	stats._bonusStats._defense = .0f;
	if (!(_mask & DEXTERITY))	stats._bonusStats._dexterity = .0f;
	if (!(_mask & INTELLIGENCE))	stats._bonusStats._intelligence = .0f;
	if (!(_mask & SPEED))	stats._bonusStats._speed = .0f;
	if (!(_mask & DODGE))	stats._bonusStats._dodge = .0f;
	if (!(_mask & PARRY))	stats._bonusStats._parry = .0f;
	if (!(_mask & POISONRATE))	stats._bonusStats._poisonRate = .0f;
	if (!(_mask & MANA))	stats._bonusStats._resources._mana = .0f;
	if (!(_mask & DAMAGE))	stats._damage = .0f;
	if (!(_mask & CRITICALRATE))	stats._criticalRate = .0f;
}

StatsMask* StatsMask::Copy() const
{
	return new StatsMask(*this);
}
