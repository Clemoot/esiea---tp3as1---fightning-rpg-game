/**
	\file WeaponStats.h
	\brief WeaponStats declaration
	\author Garnier Cl�ment
*/

#pragma once

#include "BaseStats.h"

/**
	\class WeaponStats
	\brief Stats for a weapon
*/
class WeaponStats {
	friend class StatsMask;
	friend class AlwaysPositiveMask;
	friend class RatesMask;
	friend class WeaponAlwaysPositiveMask;
public:
	/**
		\brief WeaponStats with 0.0f on each member
	*/
	static const WeaponStats NULL_VALUE;
	/**
		\brief WeaponStats with 1.0f on each member
	*/
	static const WeaponStats IDENTITY;

	/**
		\brief WeaponStats constructor
		\param masks Masks to apply
	*/
	WeaponStats(std::vector<StatsMask*> masks = std::vector<StatsMask*>());
	/**
		\brief WeaponStats constructor
		\param damage Damage
		\param masks Masks to apply
	*/
	WeaponStats(float damage, std::vector<StatsMask*> masks = std::vector<StatsMask*>());
	/**
		\brief WeaponStats constructor
		\param bonusStats Stats to grant to the owner
		\param masks Masks to apply
	*/
	WeaponStats(BaseStats bonusStats, std::vector<StatsMask*> masks = std::vector<StatsMask*>());
	/**
		\brief WeaponStats constructor
		\param damage Damage
		\param criticalRate Critical rate
		\param masks Masks to apply
	*/
	WeaponStats(float damage, float criticalRate, std::vector<StatsMask*> masks = std::vector<StatsMask*>());
	/**
		\brief WeaponStats constructor
		\param damage Damage
		\param bonusStats Stats to grant to the owner
		\param masks Masks to apply
	*/
	WeaponStats(float damage, BaseStats bonusStats, std::vector<StatsMask*> masks = std::vector<StatsMask*>());
	/**
		\brief WeaponStats constructor
		\param damage Damage
		\param criticalRate Critate rate
		\param bonusStats Stats to grant to the owner
		\param masks Masks to apply
	*/
	WeaponStats(float damage, float criticalRate, BaseStats bonusStats, std::vector<StatsMask*> masks = std::vector<StatsMask*>());
	/**
		\brief WeaponStats copy constructor
		\param other WeaponStats to copy
	*/
	WeaponStats(const WeaponStats& other);
	/// WeaponStats destructor
	~WeaponStats();

	float Damage() const;			///< Damage getter
	float CriticalRate() const;		///< Critical rate getter
	BaseStats BonusStats() const;	///< Bonus stats getter

	void Damage(float damage);		///< Damage setter
	void CriticalRate(float criticalRate);	///< Critical rate setter
	void BonusStats(const BaseStats bonusStats);	///< Bonus stats setter

	/**
		\brief WeaponStats addition
		\param cs WeaponStats term
		\return WeaponStats sum
	*/
	WeaponStats operator+(const WeaponStats& cs);
	/**
		\brief WeaponStats addition
		\param cs WeaponStats term
		\return WeaponStats sum
	*/
	WeaponStats& operator+=(const WeaponStats& cs);
	/**
		\brief WeaponStats substration
		\param cs WeaponStats term
		\return WeaponStats difference
	*/
	WeaponStats operator-(const WeaponStats& cs);
	/**
		\brief WeaponStats substration
		\param cs WeaponStats term
		\return WeaponStats difference
	*/
	WeaponStats& operator-=(const WeaponStats& cs);
	/**
		\brief WeaponStats multiplication
		\param cs WeaponStats term
		\return WeaponStats product
	*/
	WeaponStats operator*(const WeaponStats& cs);
	/**
		\brief WeaponStats multiplication
		\param cs WeaponStats term
		\return WeaponStats product
	*/
	WeaponStats& operator*=(const WeaponStats& cs);
	/**
		\brief WeaponStats division
		\param cs WeaponStats divider
		\return WeaponStats quotient
	*/
	WeaponStats operator/(const WeaponStats& cs);
	/**
		\brief WeaponStats division
		\param cs WeaponStats divider
		\return WeaponStats quotient
	*/
	WeaponStats& operator/=(const WeaponStats& cs);
	/**
		\brief WeaponStats scalar multiplication
		\param s float term
		\return WeaponStats product
	*/
	WeaponStats operator*(const float& s);
	/**
		\brief WeaponStats scalar multiplication
		\param s float term
		\return WeaponStats product
	*/
	WeaponStats& operator*=(const float& s);
	/**
		\brief WeaponStats scalar division
		\param s float divider
		\return WeaponStats quotient
	*/
	WeaponStats operator/(const float& s);
	/**
		\brief WeaponStats scalar division
		\param s float divider
		\return WeaponStats quotient
	*/
	WeaponStats& operator/=(const float& s);
	/**
		\brief WeaponStats affectation
		\param cs Right value
		\return WeaponStats
	*/
	WeaponStats& operator=(const WeaponStats& cs);


	/// Adds mask to apply
	bool AddMask(StatsMask* mask);
	/// Adds many masks to apply
	void AddMasks(std::vector<StatsMask*> masks);
	/// Removes a mask from masks
	bool RemoveMask(StatsMask* mask);
	/// Masks getter
	std::vector<StatsMask*> Masks() const;

	/// Copy stats with new masks
	WeaponStats CopyWith(std::vector<StatsMask*> masks) const;

	/**
		\brief WeaponStats equality
		\param other Right value
		\return true if equal, false otherwise
	*/
	bool operator==(const WeaponStats& other) const;

private:
	float _damage;				///< Damage
	float _criticalRate;		///< Critical rate
	BaseStats _bonusStats;		///< Bonus stats
	std::vector<StatsMask*> _masks;	///< Masks

	/// Applies all masks
	void __Check();
};