/**
	\file WeaponStats.cpp
	\brief WeaponStats declaration
	\author Garnier Cl�ment
*/

#include "WeaponStats.h"
#include <algorithm>
#include "StatsMask.h"
#include "../Utils/PointerDeleter.h"

const WeaponStats WeaponStats::NULL_VALUE = WeaponStats(.0f, .0f, BaseStats::NULL_VALUE);
const WeaponStats WeaponStats::IDENTITY = WeaponStats(1.f, 1.f, BaseStats::IDENTITY);

WeaponStats::WeaponStats(std::vector<StatsMask*> masks):
	_damage(.0f),
	_criticalRate(.0f),
	_bonusStats(BaseStats::NULL_VALUE)
{
	_masks.insert(_masks.end(), masks.begin(), masks.end());
	__Check();
}

WeaponStats::WeaponStats(float damage, std::vector<StatsMask*> masks):
	_damage(damage),
	_criticalRate(.0f),
	_bonusStats(BaseStats::NULL_VALUE)
{
	_masks.insert(_masks.end(), masks.begin(), masks.end());
}

WeaponStats::WeaponStats(BaseStats bonusStats, std::vector<StatsMask*> masks):
	_damage(.0f),
	_criticalRate(.0f),
	_bonusStats(bonusStats)
{
	_masks.insert(_masks.end(), masks.begin(), masks.end());
}

WeaponStats::WeaponStats(float damage, float criticalRate, std::vector<StatsMask*> masks):
	_damage(damage),
	_criticalRate(criticalRate),
	_bonusStats(BaseStats::NULL_VALUE)
{
	_masks.insert(_masks.end(), masks.begin(), masks.end());
}

WeaponStats::WeaponStats(float damage, BaseStats bonusStats, std::vector<StatsMask*> masks):
	_damage(damage),
	_criticalRate(.0f),
	_bonusStats(bonusStats)
{
	_masks.insert(_masks.end(), masks.begin(), masks.end());
}

WeaponStats::WeaponStats(float damage, float criticalRate, BaseStats bonusStats, std::vector<StatsMask*> masks):
	_damage(damage),
	_criticalRate(criticalRate),
	_bonusStats(bonusStats)
{
	_masks.insert(_masks.end(), masks.begin(), masks.end());
}

WeaponStats::WeaponStats(const WeaponStats& other):
	_damage(other._damage),
	_criticalRate(other._criticalRate),
	_bonusStats(other._bonusStats)
{
	std::for_each(other._masks.begin(), other._masks.end(), [&](StatsMask* mask) {
		if (!mask)	return;
		_masks.push_back(mask->Copy());
	});
}

WeaponStats::~WeaponStats()
{
	auto remove = std::remove_if(_masks.begin(), _masks.end(), PointerDeleter<StatsMask>([&](StatsMask*& mask) {		return true;	}));
	if (remove != _masks.end())
		_masks.erase(remove);
}

float WeaponStats::Damage() const
{
	return _damage;
}

float WeaponStats::CriticalRate() const
{
	return _criticalRate;
}

BaseStats WeaponStats::BonusStats() const
{
	return _bonusStats;
}

void WeaponStats::Damage(float damage)
{
	_damage = damage;
}

void WeaponStats::CriticalRate(float criticalRate)
{
	_criticalRate = criticalRate;
}

void WeaponStats::BonusStats(const BaseStats bonusStats)
{
	_bonusStats = bonusStats;
}

WeaponStats WeaponStats::operator+(const WeaponStats& ws)
{
	WeaponStats copy(
		_damage + ws._damage,
		_criticalRate + ws._criticalRate,
		_bonusStats + ws._bonusStats
	);
	copy.__Check();
	return copy;
}

WeaponStats& WeaponStats::operator+=(const WeaponStats& ws)
{
	_damage += ws._damage;
	_criticalRate += ws._criticalRate;
	_bonusStats += ws._bonusStats;
	__Check();
	return *this;
}

WeaponStats WeaponStats::operator-(const WeaponStats& ws)
{
	WeaponStats copy(
		_damage - ws._damage,
		_criticalRate - ws._criticalRate,
		_bonusStats - ws._bonusStats
	);
	copy.__Check();
	return copy;
}

WeaponStats& WeaponStats::operator-=(const WeaponStats& ws)
{
	_damage -= ws._damage;
	_criticalRate -= ws._criticalRate;
	_bonusStats -= ws._bonusStats;
	__Check();
	return *this;
}

WeaponStats WeaponStats::operator*(const WeaponStats& ws)
{
	WeaponStats copy(
		_damage * ws._damage,
		_criticalRate * ws._criticalRate,
		_bonusStats * ws._bonusStats
	);
	copy.__Check();
	return copy;
}

WeaponStats& WeaponStats::operator*=(const WeaponStats& ws)
{
	_damage *= ws._damage;
	_criticalRate *= ws._criticalRate;
	_bonusStats *= ws._bonusStats;
	__Check();
	return *this;
}

WeaponStats WeaponStats::operator/(const WeaponStats& ws)
{
	WeaponStats copy(
		_damage / ws._damage,
		_criticalRate / ws._criticalRate,
		_bonusStats / ws._bonusStats
	);
	copy.__Check();
	return copy;
}

WeaponStats& WeaponStats::operator/=(const WeaponStats& ws)
{
	_damage /= ws._damage;
	_criticalRate /= ws._criticalRate;
	_bonusStats /= ws._bonusStats;
	__Check();
	return *this;
}

WeaponStats WeaponStats::operator*(const float& s)
{
	WeaponStats copy(
		_damage * s,
		_criticalRate * s,
		_bonusStats * s
	);
	copy.__Check();
	return copy;
}

WeaponStats& WeaponStats::operator*=(const float& s)
{
	_damage *= s;
	_criticalRate *= s;
	_bonusStats *= s;
	__Check();
	return *this;
}

WeaponStats WeaponStats::operator/(const float& s)
{
	WeaponStats copy(
		_damage / s,
		_criticalRate / s,
		_bonusStats / s
	);
	copy.__Check();
	return copy;
}

WeaponStats& WeaponStats::operator/=(const float& s)
{
	_damage /= s;
	_criticalRate /= s;
	_bonusStats /= s;
	__Check();
	return *this;
}

WeaponStats& WeaponStats::operator=(const WeaponStats& other)
{
	_damage = other._damage;
	_criticalRate = other._criticalRate;
	_bonusStats = other._bonusStats;
	_masks = std::vector<StatsMask*>();
	std::for_each(other._masks.begin(), other._masks.end(), [&](StatsMask* mask) {
		if (!mask)	return;
		_masks.push_back(mask->Copy());
	});
	return *this;
}

bool WeaponStats::AddMask(StatsMask* mask)
{
	if (!mask)	return false;
	//Check if mask already active
	auto pos = std::find(_masks.begin(), _masks.end(), mask);
	if (pos != _masks.end())	return true;
	_masks.push_back(mask->Copy());
	__Check();
	return true;
}

void WeaponStats::AddMasks(std::vector<StatsMask*> masks)
{
	std::for_each(masks.begin(), masks.end(), [&](StatsMask* mask) {
		if (!mask)	return;
		_masks.push_back(mask->Copy());
	});
}

bool WeaponStats::RemoveMask(StatsMask* mask)
{
	if (!mask)	return false;
	auto remove = std::remove_if(_masks.begin(), _masks.end(), PointerDeleter<StatsMask>([&](StatsMask*& m) {
		return m == mask;
	}));
	if (remove == _masks.end())
		return false;
	_masks.erase(remove);
	return true;
}

std::vector<StatsMask*> WeaponStats::Masks() const
{
	return _masks;
}

WeaponStats WeaponStats::CopyWith(std::vector<StatsMask*> masks) const
{
	WeaponStats stats(*this);
	stats._masks.insert(_masks.end(), masks.begin(), masks.end());
	return stats;
}

bool WeaponStats::operator==(const WeaponStats& other) const
{
	return _damage == other._damage &&
		_criticalRate == other._criticalRate &&
		_bonusStats == other._bonusStats;
}

void WeaponStats::__Check()
{
	std::for_each(_masks.begin(), _masks.end(), [&](StatsMask*& mask) {
		if (!mask)	return;
		mask->Apply(*this);
	});
}
