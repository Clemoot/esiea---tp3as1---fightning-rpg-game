/**
	\file SpecificMasks.h
	\brief RatesMask, AlwaysPositiveMask, WeaponAlwaysPositiveMask declarations
	\author Garnier Cl�ment
*/

#pragma once

#include "StatsMask.h"

/**
	\class RatesMask
	\brief Mask which checks rates
*/
class RatesMask : public StatsMask {
public:
	/// RatesMask default constructor
	RatesMask();
	/// RatesMask destructor
	~RatesMask();

	/// Applies mask on BaseStats
	void Apply(BaseStats& stats);
	/// Applies mask on WeaponStats
	void Apply(WeaponStats& stats);
	/// Copy mask
	StatsMask* Copy() const;
};

/**
	\class AlwaysPositiveMask
	\brief Mask which checks if all values are positive
*/
class AlwaysPositiveMask : public StatsMask {
public:
	/// AlwaysPositiveMask default constructor
	AlwaysPositiveMask();
	/// AlwaysPositiveMask destructor
	~AlwaysPositiveMask();

	/// Applies mask on BaseStats
	void Apply(BaseStats& stats);
	/// Applies mask on WeaponStats
	void Apply(WeaponStats& stats);
	/// Copy mask
	StatsMask* Copy() const;
};

/**
	\class WeaponAlwaysPositiveMask
	\brief Mask which checks if weapon values are positive
*/
class WeaponAlwaysPositiveMask : public StatsMask {
public:
	/// WeaponAlwaysPositiveMask default constructor
	WeaponAlwaysPositiveMask();
	/// WeaponAlwaysPositiveMask destructor
	~WeaponAlwaysPositiveMask();

	/// Applies mask on BaseStats
	void Apply(BaseStats& stats);
	/// Applies mask on WeaponStats
	void Apply(WeaponStats& stats);
	/// Copy mask
	StatsMask* Copy() const;

};