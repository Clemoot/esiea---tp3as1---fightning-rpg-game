/**
	\file ResourceStats.h
	\brief ResourceStats declaration
	\author Garnier Cl�ment
*/

#pragma once

#include <iostream>

/**
	\class ResourceStats
	\brief Life and mana
*/
class ResourceStats {
	friend class StatsMask;
	friend class RatesMask;
	friend class AlwaysPositiveMask;
	friend class BaseStats;
	friend class WeaponStats;

public:
	/**
		\brief ResourceStats constructor
		\param life Life
		\param mana Mana
	*/
	ResourceStats(float life = .0f, float mana = .0f);
	/**
		\brief ResourceStats copy constructor
		\param rs ResourceStats to copy
	*/
	ResourceStats(const ResourceStats& rs);
	/// ResourceStats desctructor
	~ResourceStats();

	/**
		\brief ResourceStats addition
		\param cs ResourceStats term
		\return ResourceStats sum
	*/
	ResourceStats operator+(const ResourceStats& cs);
	/**
		\brief ResourceStats addition
		\param cs ResourceStats term
		\return ResourceStats sum
	*/
	ResourceStats& operator+=(const ResourceStats& cs);
	/**
		\brief ResourceStats substration
		\param cs ResourceStats term
		\return ResourceStats difference
	*/
	ResourceStats operator-(const ResourceStats& cs);
	/**
		\brief ResourceStats substration
		\param cs ResourceStats term
		\return ResourceStats difference
	*/
	ResourceStats& operator-=(const ResourceStats& cs);
	/**
		\brief ResourceStats multiplication
		\param cs ResourceStats term
		\return ResourceStats product
	*/
	ResourceStats operator*(const ResourceStats& cs);
	/**
		\brief ResourceStats multiplication
		\param cs ResourceStats term
		\return ResourceStats product
	*/
	ResourceStats& operator*=(const ResourceStats& cs);
	/**
		\brief ResourceStats division
		\param cs ResourceStats divider
		\return ResourceStats quotient
	*/
	ResourceStats operator/(const ResourceStats& cs);
	/**
		\brief ResourceStats division
		\param cs ResourceStats divider
		\return ResourceStats quotient
	*/
	ResourceStats& operator/=(const ResourceStats& cs);
	/**
		\brief ResourceStats scalar multiplication
		\param s float term
		\return ResourceStats product
	*/
	ResourceStats operator*(const float& s);
	/**
		\brief ResourceStats scalar multiplication
		\param s float term
		\return ResourceStats product
	*/
	ResourceStats& operator*=(const float& s);
	/**
		\brief ResourceStats scalar division
		\param s float divider
		\return ResourceStats quotient
	*/
	ResourceStats operator/(const float& s);
	/**
		\brief ResourceStats scalar division
		\param s float divider
		\return ResourceStats quotient
	*/
	ResourceStats& operator/=(const float& s);


	float Life() const;		///< Life getter
	float Mana() const;		///< Mana getter

	void Life(float life);	///< Life setter
	void Mana(float mana);	///< Mana setter

	/**
		\brief ResourceStats equality
		\param other Right value
		\return true if equal, false otherwise
	*/
	bool operator==(const ResourceStats& other) const;

private:
	float _life;		///< Life
	float _mana;		///< Mana
};