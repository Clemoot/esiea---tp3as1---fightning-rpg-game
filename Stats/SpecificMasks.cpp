/**
	\file SpecificMasks.cpp
	\brief SpecificMasks declaration
	\author Garnier Cl�ment
*/

#include "SpecificMasks.h"

RatesMask::RatesMask(): StatsMask()
{}

RatesMask::~RatesMask()
{}

void RatesMask::Apply(BaseStats & stats)
{
	if (stats._parry < .0f)		stats._parry = .0f;
	if (stats._parry > 1.f)		stats._parry = 1.f;

	if (stats._poisonRate < .0f)	stats._poisonRate = .0f;
	if (stats._poisonRate > 1.f)	stats._poisonRate = 1.f;

	if (stats._dodge < .0f)		stats._dodge = .0f;
	if (stats._dodge > 1.f)		stats._dodge = 1.f;
}

void RatesMask::Apply(WeaponStats& stats)
{
	if (stats._criticalRate < .0f)	stats._criticalRate = .0f;
	if (stats._criticalRate > 1.f)	stats._criticalRate = 1.f;

	Apply(stats._bonusStats);
}

StatsMask* RatesMask::Copy() const
{
	return new RatesMask(*this);
}



AlwaysPositiveMask::AlwaysPositiveMask() : StatsMask()
{}

AlwaysPositiveMask::~AlwaysPositiveMask()
{}

void AlwaysPositiveMask::Apply(BaseStats & stats)
{
	if (stats._resources._life < .0f)	stats._resources._life = .0f;
	if (stats._resources._mana < .0f)	stats._resources._mana = .0f;
	if (stats._attack < .0f)	stats._attack = .0f;
	if (stats._defense < .0f)	stats._defense = .0f;
	if (stats._dexterity < .0f)	stats._dexterity = .0f;
	if (stats._intelligence < .0f)	stats._intelligence = .0f;
	if (stats._speed < .0f)	stats._speed = .0f;
	if (stats._dodge < .0f)	stats._dodge = .0f;
	if (stats._poisonRate < .0f)	stats._poisonRate = .0f;
	if (stats._parry < .0f)	stats._parry = .0f;
}

void AlwaysPositiveMask::Apply(WeaponStats& stats)
{
	if (stats._damage < .0f)		stats._damage = .0f;
	if (stats._criticalRate < .0f)		stats._criticalRate = .0f;
	Apply(stats._bonusStats);
}

StatsMask* AlwaysPositiveMask::Copy() const
{
	return new AlwaysPositiveMask(*this);
}

WeaponAlwaysPositiveMask::WeaponAlwaysPositiveMask()
{
}

WeaponAlwaysPositiveMask::~WeaponAlwaysPositiveMask()
{
}

void WeaponAlwaysPositiveMask::Apply(BaseStats& stats)
{
}

void WeaponAlwaysPositiveMask::Apply(WeaponStats& stats)
{
	if (stats._damage < .0f)		stats._damage = .0f;
	if (stats._criticalRate < .0f)		stats._criticalRate = .0f;
}

StatsMask* WeaponAlwaysPositiveMask::Copy() const
{
	return new WeaponAlwaysPositiveMask(*this);
}
