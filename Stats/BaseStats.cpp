/**
	\file BaseStats.cpp
	\brief BaseStats declaration
	\author Garnier Cl�ment
*/

#include "BaseStats.h"

#include <algorithm>

#include "StatsMask.h"

#include "../Utils/PointerDeleter.h"

const BaseStats BaseStats::IDENTITY = BaseStats(1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, { new MASK_WITH_ALL_STATS });
const BaseStats BaseStats::NULL_VALUE = BaseStats(.0f, .0f, .0f, .0f, .0f, .0f, .0f, .0f, .0f, .0f, { new MASK_WITH_ALL_STATS });

BaseStats::BaseStats(float life, float atk, float def, float dex, float intelligence, float spd, float dodge, float parry, float poisonRate, float bonusMana, std::vector<StatsMask*> masks) :
	_attack(atk), 
	_defense(def), 
	_dexterity(dex), 
	_intelligence(intelligence), 
	_speed(spd), 
	_dodge(dodge), 
	_parry(parry), 
	_poisonRate(poisonRate), 
	_bonusMana(bonusMana), 
	_resources(life, 10.f * intelligence + bonusMana)
{
	if (masks.empty())
		_masks = { new StatsMask() };
	else
		_masks.insert(_masks.end(), masks.begin(), masks.end());
	__Check();
}

BaseStats::BaseStats(float globalValue) :
	_attack(globalValue),
	_defense(globalValue),
	_dexterity(globalValue),
	_intelligence(globalValue),
	_speed(globalValue),
	_dodge(globalValue),
	_parry(globalValue),
	_poisonRate(globalValue),
	_bonusMana(globalValue),
	_resources(globalValue, 10.f * globalValue + globalValue)
{}

BaseStats::BaseStats(std::vector<StatsMask*> masks) :
	_attack(.0f),
	_defense(.0f),
	_dexterity(.0f),
	_intelligence(.0f),
	_speed(.0f),
	_dodge(.0f),
	_parry(.0f),
	_poisonRate(.0f),
	_bonusMana(.0f),
	_resources(.0f, .0f)
{
	_masks.insert(_masks.end(), masks.begin(), masks.end());
}

BaseStats::BaseStats(const BaseStats& cs) :
	_attack(cs._attack), 
	_defense(cs._defense),
	_dexterity(cs._dexterity),
	_intelligence(cs._intelligence),
	_speed(cs._speed),
	_dodge(cs._dodge),
	_parry(cs._parry),
	_poisonRate(cs._poisonRate),
	_bonusMana(cs._bonusMana),
	_resources(cs._resources._life, cs._resources._mana)
{
	std::for_each(cs._masks.begin(), cs._masks.end(), [&](StatsMask* mask) {
		if (!mask)	return;
		_masks.push_back(mask->Copy());
	});
}

BaseStats::~BaseStats()
{
	auto remove = std::remove_if(_masks.begin(), _masks.end(), PointerDeleter<StatsMask>([&](StatsMask*& mask) {		return true;	}));
	if (remove != _masks.end())
		_masks.erase(remove);
}

BaseStats BaseStats::operator+(const BaseStats& cs)
{
	BaseStats bs(
		_resources._life + cs._resources._life,
		_attack + cs._attack,
		_defense + cs._defense,
		_dexterity + cs._dexterity,
		_intelligence + cs._intelligence,
		_speed + cs._speed,
		_dodge + cs._dodge,
		_parry + cs._parry,
		_poisonRate + cs._poisonRate,
		_bonusMana + cs._bonusMana);
	bs.__Check();
	return bs;
}

BaseStats& BaseStats::operator+=(const BaseStats& cs)
{
	_resources._life += cs._resources._life;
	_attack += cs._attack;
	_defense += cs._defense;
	_dexterity += cs._dexterity;
	_intelligence += cs._intelligence;
	_speed += cs._speed;
	_dodge += cs._dodge;
	_parry += cs._parry;
	_poisonRate += cs._poisonRate;
	_bonusMana += cs._bonusMana;

	_resources._mana = 10.f * _intelligence + _bonusMana;
	
	__Check();

	return *this;
}

BaseStats BaseStats::operator-(const BaseStats& cs)
{
	BaseStats bs(
		_resources._life - cs._resources._life,
		_attack - cs._attack,
		_defense - cs._defense,
		_dexterity - cs._dexterity,
		_intelligence - cs._intelligence,
		_speed - cs._speed,
		_dodge - cs._dodge,
		_parry - cs._parry,
		_poisonRate - cs._poisonRate,
		_bonusMana + cs._bonusMana);
	bs.__Check();
	return bs;
}

BaseStats& BaseStats::operator-=(const BaseStats& cs)
{
	_resources._life -= cs._resources._life;
	_attack -= cs._attack;
	_defense -= cs._defense;
	_dexterity -= cs._dexterity;
	_intelligence -= cs._intelligence;
	_speed -= cs._speed;
	_dodge -= cs._dodge;
	_parry -= cs._parry;
	_poisonRate -= cs._poisonRate;
	_bonusMana -= cs._bonusMana;
	_resources._mana = 10.f * _intelligence + _bonusMana;

	__Check();

	return *this;
}

BaseStats BaseStats::operator*(const BaseStats& cs)
{
	BaseStats bs(
		_resources._life * cs._resources._life,
		_attack * cs._attack,
		_defense * cs._defense,
		_dexterity * cs._dexterity,
		_intelligence * cs._intelligence,
		_speed * cs._speed,
		_dodge * cs._dodge,
		_parry * cs._parry,
		_poisonRate * cs._poisonRate,
		_bonusMana * cs._bonusMana);
	bs.__Check();
	return bs;
}

BaseStats& BaseStats::operator*=(const BaseStats& cs)
{
	_resources._life *= cs._resources._life;
	_attack *= cs._attack;
	_defense *= cs._defense;
	_dexterity *= cs._dexterity;
	_intelligence *= cs._intelligence;
	_speed *= cs._speed;
	_dodge *= cs._dodge;
	_parry *= cs._parry;
	_poisonRate *= cs._poisonRate;
	_bonusMana *= cs._bonusMana;
	_resources._mana = 10.f * _intelligence + _bonusMana;

	__Check();

	return *this;
}

BaseStats BaseStats::operator/(const BaseStats& cs)
{
	BaseStats bs(
		_resources._life / cs._resources._life,
		_attack / cs._attack,
		_defense / cs._defense,
		_dexterity / cs._dexterity,
		_intelligence / cs._intelligence,
		_speed / cs._speed,
		_dodge / cs._dodge,
		_parry / cs._parry,
		_poisonRate / cs._poisonRate,
		_bonusMana / cs._bonusMana);
	bs.__Check();
	return bs;
}

BaseStats& BaseStats::operator/=(const BaseStats& cs)
{
	_resources._life /= cs._resources._life;
	_attack /= cs._attack;
	_defense /= cs._defense;
	_dexterity /= cs._dexterity;
	_intelligence /= cs._intelligence;
	_speed /= cs._speed;
	_dodge /= cs._dodge;
	_parry /= cs._parry;
	_poisonRate /= cs._poisonRate;
	_bonusMana /= cs._bonusMana;
	_resources._mana = 10.f * _intelligence + _bonusMana;

	__Check();

	return *this;
}

BaseStats BaseStats::operator*(const float& s)
{
	BaseStats bs(
		_resources._life * s,
		_attack * s,
		_defense * s,
		_dexterity * s,
		_intelligence * s,
		_speed * s,
		_dodge * s,
		_parry * s,
		_poisonRate * s,
		_bonusMana * s);
	bs.__Check();
	return bs;
}

BaseStats& BaseStats::operator*=(const float& s)
{
	_resources._life *= s;
	_attack *= s;
	_defense *= s;
	_dexterity *= s;
	_intelligence *= s;
	_speed *= s;
	_dodge *= s;
	_parry *= s;
	_poisonRate *= s;
	_bonusMana *= s;
	_resources._mana = 10.f * _intelligence + _bonusMana;
	
	__Check();

	return *this;
}

BaseStats BaseStats::operator/(const float& s)
{
	BaseStats bs(
		_resources._life / s,
		_attack / s,
		_defense / s,
		_dexterity / s,
		_intelligence / s,
		_speed / s,
		_dodge / s,
		_parry / s,
		_poisonRate / s,
		_bonusMana/ s);
	bs.__Check();
	return bs;
}

BaseStats& BaseStats::operator/=(const float& s)
{
	_resources._life /= s;
	_attack /= s;
	_defense /= s;
	_dexterity /= s;
	_intelligence /= s;
	_speed /= s;
	_dodge /= s;
	_parry /= s;
	_poisonRate /= s;
	_bonusMana /= s;
	_resources._mana = 10.f * _intelligence + _bonusMana;
	
	__Check();

	return *this;
}

BaseStats& BaseStats::operator=(const BaseStats& cs)
{
	_attack = cs._attack;
	_defense = cs._defense;
	_dexterity = cs._dexterity;
	_intelligence = cs._intelligence;
	_speed = cs._speed;
	_dodge = cs._dodge;
	_parry = cs._parry;
	_poisonRate = cs._poisonRate;
	_bonusMana = cs._bonusMana;
	_resources = cs._resources;
	
	_masks = std::vector<StatsMask*>();
	std::for_each(cs._masks.begin(), cs._masks.end(), [&](StatsMask* mask) {
		if (!mask)	return;
		_masks.push_back(mask->Copy());
	});
	return *this;
}

float BaseStats::Attack() const
{
	return _attack;
}

float BaseStats::Defense() const
{
	return _defense;
}

float BaseStats::Dexterity() const
{
	return _dexterity;
}

float BaseStats::Intelligence() const
{
	return _intelligence;
}

float BaseStats::Speed() const
{
	return _speed;
}

float BaseStats::Dodge() const
{
	return _dodge;
}

float BaseStats::Parry() const
{
	return _parry;
}

float BaseStats::PoisonRate() const
{
	return _poisonRate;
}

float BaseStats::BonusMana() const
{
	return _bonusMana;
}

float BaseStats::Life() const
{
	return _resources.Life();
}

float BaseStats::Mana() const
{
	return _resources._mana;
}

ResourceStats BaseStats::Resources() const
{
	return _resources;
}

void BaseStats::Attack(float attack)
{
	_attack = attack;
	__Check();
}

void BaseStats::Defense(float defense)
{
	_defense = defense;
	__Check();
}

void BaseStats::Dexterity(float dexterity)
{
	_dexterity = dexterity;
	__Check();
}

void BaseStats::Intelligence(float intelligence)
{
	_intelligence = intelligence;
	__Check();
}

void BaseStats::Speed(float speed)
{
	_speed = speed;
	__Check();
}

void BaseStats::Dodge(float dodge)
{
	_dodge = dodge;
	__Check();
}

void BaseStats::Parry(float parry)
{
	_parry = parry;
	__Check();
}

void BaseStats::Life(float life)
{
	_resources.Life(life);
	__Check();
}

void BaseStats::PoisonRate(float poisonRate)
{
	_poisonRate = poisonRate;
	__Check();
}

void BaseStats::BonusMana(float bonusMana)
{
	_bonusMana = bonusMana;
}

bool BaseStats::AddMask(StatsMask* mask)
{
	if (!mask)	return false;
	//Check if mask already active
	auto pos = std::find(_masks.begin(), _masks.end(), mask);
	if (pos != _masks.end())	return true;
	_masks.push_back(mask->Copy());
	__Check();
	return true;
}

void BaseStats::AddMasks(std::vector<StatsMask*> masks)
{
	std::for_each(masks.begin(), masks.end(), [&](StatsMask* mask) {
		if (!mask)	return;
		_masks.push_back(mask->Copy());
	});
}

bool BaseStats::RemoveMask(StatsMask* mask)
{
	if (!mask)	return false;
	auto remove = std::remove_if(_masks.begin(), _masks.end(), PointerDeleter<StatsMask>([&](StatsMask*& m) {
		return m == mask;
	}));
	if (remove == _masks.end())
		return false;
	_masks.erase(remove);
	return true;
}

std::vector<StatsMask*> BaseStats::Masks() const
{
	return _masks;
}

BaseStats BaseStats::CopyWith(std::vector<StatsMask*> masks) const
{
	BaseStats stats(*this);
	return stats;
}

bool BaseStats::operator==(const BaseStats& other) const
{
	return _resources == other._resources &&
		_attack == other._attack &&
		_defense == other._defense &&
		_dexterity == other._dexterity &&
		_intelligence == other._intelligence &&
		_speed == other._speed &&
		_dodge == other._dodge &&
		_parry == other._parry &&
		_poisonRate == other._poisonRate &&
		_bonusMana == other._bonusMana;
}

void BaseStats::__Check()
{
	std::for_each(_masks.begin(), _masks.end(), [&](StatsMask*& mask) {
		if(!mask)	return;
		mask->Apply(*this);
	});
}