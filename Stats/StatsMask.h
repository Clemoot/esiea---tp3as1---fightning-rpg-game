/**
	\file StatsMask.h
	\brief StatsMask declaration
	\author Garnier Cl�ment
*/

#pragma once

#include <iostream>

#include "BaseStats.h"
#include "WeaponStats.h"

/// Mask allowing all stats
#define MASK_WITH_ALL_STATS		StatsMask(StatsMask::W_ALL)
/// Mask allowing common stats and parry rate
#define MASK_WITH_PARRY			StatsMask(StatsMask::C_COMMON | StatsMask::PARRY)
/// Mask allowing common stats and poison rate
#define MASK_WITH_POISON_RATE	StatsMask(StatsMask::C_COMMON | StatsMask::POISONRATE)
/// Mask allowing common stats and mana
#define MASK_WITH_MANA			StatsMask(StatsMask::C_COMMON | StatsMask::MANA)

/**
	\class StatsMask
	\brief Mask to filter values
*/
class StatsMask {
public:
	/**
		\enum CStatsIndex
		\brief Bitwise values to create masks appliable on BaseStats object
	*/
	enum CStatsIndex {	// Character indices
		LIFE = 1,
		ATTACK = 2,
		DEFENSE = 4,
		DEXTERITY = 8,
		INTELLIGENCE = 16,
		SPEED = 32,
		DODGE = 64,
		PARRY = 128,
		POISONRATE = 256,
		MANA = 512,
		C_COMMON = LIFE | ATTACK | DEFENSE | DEXTERITY | INTELLIGENCE | SPEED | DODGE,
		C_ALL = C_COMMON | PARRY | POISONRATE | MANA
	};

	/**
		\enum WStatsIndex
		\brief Bitwise values to create masks appliable on BaseStats and WeaponStats object
	*/
	enum WStatsIndex {	// Weapon indices
		DAMAGE = 1024,
		CRITICALRATE = 2048,
		W_COMMON = C_COMMON | DAMAGE | CRITICALRATE,
		W_ALL = C_ALL | DAMAGE | CRITICALRATE
	};

	/**
		\brief StatsMask constructor
		\param mask Mask value
	*/
	StatsMask(uint16_t mask = W_COMMON);
	/**
		\brief StatsMask copy constructor
		\param other StatsMask to copy
	*/
	StatsMask(const StatsMask& other);
	/// StatsMask desctructor
	~StatsMask();

	uint16_t Mask() const;			///< Mask value getter
	void Mask(uint16_t mask);		///< Mask value setter

	/// Applies the mask on ResourceStats
	virtual void Apply(ResourceStats& stats);
	/// Applies the mask on BaseStats
	virtual void Apply(BaseStats& stats);
	/// Applies the mask on WeaponStats
	virtual void Apply(WeaponStats& stats);

	/// Copy the mask
	virtual StatsMask* Copy() const;

private:
	uint16_t _mask;	///< Mask value
};