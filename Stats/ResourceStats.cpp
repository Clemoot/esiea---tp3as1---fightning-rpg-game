/**
	\file ResourceStats.cpp
	\brief ResourceStats declaration
	\author Garnier Cl�ment
*/

#include "ResourceStats.h"

ResourceStats::ResourceStats(float life, float mana) :
	_life(life), _mana(mana)
{
}

ResourceStats::ResourceStats(const ResourceStats& rs) :
	_life(rs._life), _mana(rs._mana)
{
}

ResourceStats::~ResourceStats()
{}

ResourceStats ResourceStats::operator+(const ResourceStats& cs)
{
	return ResourceStats(_life + cs._life, _mana + cs._mana);
}

ResourceStats& ResourceStats::operator+=(const ResourceStats& cs)
{
	_life += cs._life;
	_mana += cs._mana;
	return *this;
}

ResourceStats ResourceStats::operator-(const ResourceStats& cs)
{
	return ResourceStats(_life - cs._life, _mana - cs._mana);
}

ResourceStats& ResourceStats::operator-=(const ResourceStats& cs)
{
	_life -= cs._life;
	_mana -= cs._mana;
	return *this;
}

ResourceStats ResourceStats::operator*(const ResourceStats& cs)
{
	return ResourceStats(_life * cs._life, _mana* cs._mana);
}

ResourceStats& ResourceStats::operator*=(const ResourceStats& cs)
{
	_life *= cs._life;
	_mana *= cs._mana;
	return *this;
}

ResourceStats ResourceStats::operator/(const ResourceStats& cs)
{
	return ResourceStats(_life / cs._life, _mana / cs._mana);
}

ResourceStats& ResourceStats::operator/=(const ResourceStats& cs)
{
	_life /= cs._life;
	_mana /= cs._mana;
	return *this;
}

ResourceStats ResourceStats::operator*(const float& s)
{
	return ResourceStats(_life * s, _mana* s);
}

ResourceStats& ResourceStats::operator*=(const float& s)
{
	_life *= s;
	_mana *= s;
	return *this;
}

ResourceStats ResourceStats::operator/(const float& s)
{
	return ResourceStats(_life / s, _mana / s);
}

ResourceStats& ResourceStats::operator/=(const float& s)
{
	_life /= s;
	_mana /= s;
	return *this;
}

float ResourceStats::Life() const
{
	return _life;
}

float ResourceStats::Mana() const
{
	return _mana;
}

void ResourceStats::Life(float life)
{
	_life = life;
}

void ResourceStats::Mana(float mana)
{
	_mana = mana;
}

bool ResourceStats::operator==(const ResourceStats& other) const
{
	return _life == other._life &&
		_mana == other._mana;
}