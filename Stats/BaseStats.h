/**
	\file BaseStats.h
	\brief BaseStats declaration
	\author Garnier Cl�ment
*/

#pragma once

#ifndef BASE_STATS_INCLUDED
#define BASE_STATS_INCLUDED

#include <vector>
#include "ResourceStats.h"

class StatsMask;	// Forward declaration to avoid circular dependencies

/**
	\class BaseStats
	\brief Stats of a character
*/
class BaseStats {
	friend StatsMask;
	friend class RatesMask;
	friend class AlwaysPositiveMask;
	friend class WeaponStats;

public:
	static const BaseStats IDENTITY;		///< BaseStats with 1.0f on each member
	static const BaseStats NULL_VALUE;		///< BaseStats with 0.0f on each member

	/**
		\brief baseStats constructor
		\param life Life
		\param atk Attack
		\param def Defense
		\param dex Dexterity
		\param intelligence Intelligence
		\param spd Speed
		\param dodge Dodge rate
		\param parry Parry rate
		\param poisonRate Poison rate
		\param bonusMana Bonus mana
		\param masks Masks on stats
	*/
	BaseStats(float life,
					float atk,
					float def,
					float dex,
					float intelligence,
					float spd,
					float dodge,
					float parry = .0f,
					float poisonRate = .0f,
					float bonusMana = .0f,
					std::vector<StatsMask*> masks = std::vector<StatsMask*>());
	/**
		\brief BaseStats constructor
		\param globalValue Scalar for all stats
	*/
	BaseStats(float globalValue);
	/**
		\brief BaseStats constructor
		\param masks Masks on stats
	*/
	BaseStats(std::vector<StatsMask*> masks);
	/**
		\brief BaseStats constructor
		\param cs BaseStats to copy
	*/
	BaseStats(const BaseStats& cs);
	/// BaseStats destructor
	virtual ~BaseStats();

	/**
		\brief BaseStats addition
		\param cs BaseStats term
		\return BaseStats sum
	*/
	BaseStats operator+(const BaseStats& cs);
	/**
		\brief BaseStats addition
		\param cs BaseStats term
		\return BaseStats sum
	*/
	BaseStats& operator+=(const BaseStats& cs);
	/**
		\brief BaseStats substration
		\param cs BaseStats term
		\return BaseStats difference
	*/
	BaseStats operator-(const BaseStats& cs);
	/**
		\brief BaseStats substration
		\param cs BaseStats term
		\return BaseStats difference
	*/
	BaseStats& operator-=(const BaseStats& cs);
	/**
		\brief BaseStats multiplication
		\param cs BaseStats term
		\return BaseStats product
	*/
	BaseStats operator*(const BaseStats& cs);
	/**
		\brief BaseStats multiplication
		\param cs BaseStats term
		\return BaseStats product
	*/
	BaseStats& operator*=(const BaseStats& cs);
	/**
		\brief BaseStats division
		\param cs BaseStats divider
		\return BaseStats quotient
	*/
	BaseStats operator/(const BaseStats& cs);
	/**
		\brief BaseStats division
		\param cs BaseStats divider
		\return BaseStats quotient
	*/
	BaseStats& operator/=(const BaseStats& cs);
	/**
		\brief BaseStats scalar multiplication
		\param s float term
		\return BaseStats product
	*/
	BaseStats operator*(const float& s);
	/**
		\brief BaseStats scalar multiplication
		\param s float term
		\return BaseStats product
	*/
	BaseStats& operator*=(const float& s);
	/**
		\brief BaseStats scalar division
		\param s float divider
		\return BaseStats quotient
	*/
	BaseStats operator/(const float& s);
	/**
		\brief BaseStats scalar division
		\param s float divider
		\return BaseStats quotient
	*/
	BaseStats& operator/=(const float& s);
	/**
		\brief BaseStats affectation
		\param cs Right value
		\return BaseStats
	*/
	BaseStats& operator=(const BaseStats& cs);

	float Attack() const;				///< Attack getter
	float Defense() const ;				///< Defense getter
	float Dexterity() const;			///< Dexterity getter
	float Intelligence() const;			///< Intelligence getter
	float Speed() const;				///< Speed getter
	float Dodge() const;				///< Dodge rate getter
	float Parry() const;				///< Parry rate getter
	float PoisonRate() const;			///< Poison rate getter
	float BonusMana() const;			///< Bonus mana getter
	float Life() const;					///< Life getter (alias)
	float Mana() const;					///< Mana getter (alias)
	ResourceStats Resources() const;	///< Resources getter

	void Attack(float attack);					///< Attack setter
	void Defense(float defense);				///< Defense setter
	void Dexterity(float dexterity);			///< Dexterity setter
	void Intelligence(float intelligence);		///< Intelligence setter
	void Speed(float speed);					///< Speed setter
	void Dodge(float dodge);					///< Dodge rate setter
	void Parry(float parry);					///< Parry rate setter
	void Life(float life);						///< Life setter
	void PoisonRate(float poisonRate);			///< Poison rate setter
	void BonusMana(float bonusMana);			///< Bonus mana setter

	/// Adds mask to stats
	bool AddMask(StatsMask* mask);
	/// Adds many masks to stats
	void AddMasks(std::vector<StatsMask*> masks);
	/// Removes a mask from masks
	bool RemoveMask(StatsMask* mask);
	std::vector<StatsMask*> Masks() const;		///< Masks getter
	
	/// Checks if stats have a specific mask
	template<class T>
	bool HasMask() const;

	/// Copy stats with new masks
	BaseStats CopyWith(std::vector<StatsMask*> masks) const;

	/**
		\brief BaseStats equality
		\param other Right value
		\return true if equal, false otherwise
	*/
	bool operator==(const BaseStats& other) const;

private:
	float _attack;				///< Attack
	float _defense;				///< Defense
	float _dexterity;			///< Dexterity
	float _intelligence;		///< Intelligence
	float _speed;				///< Speed
	float _dodge;				///< Dodge rate
	float _parry;				///< Parry rate
	float _poisonRate;			///< Poison rate
	float _bonusMana;			///< Bonus mana
	ResourceStats _resources;	///< Resources
	std::vector<StatsMask*> _masks;	///< Masks
	
	/// Computes masks
	void __Check();
};

#endif

template<class T>
inline bool BaseStats::HasMask() const
{
	auto pos = std::find_if(_masks.begin(), _masks.end(), [](StatsMask* m) {
		if (!m)	return false;
		try {
			dynamic_cast<T&>(*m);
			return true;
		}
		catch (const std::bad_cast&) {
			return false;
		}
	});
	return pos != _masks.end();
}
